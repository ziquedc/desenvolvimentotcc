-- phpMyAdmin SQL Dump
-- version 4.8.5
-- https://www.phpmyadmin.net/
--
-- Host: 127.0.0.1
-- Generation Time: 26-Ago-2019 às 02:50
-- Versão do servidor: 10.1.38-MariaDB
-- versão do PHP: 7.3.4

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `tcc`
--

-- --------------------------------------------------------

--
-- Estrutura da tabela `acertos`
--

CREATE TABLE `acertos` (
  `id_acertos` int(6) NOT NULL,
  `id_usuario` int(6) NOT NULL,
  `nivel_1` int(6) DEFAULT '0',
  `nivel_2` int(6) DEFAULT '0',
  `nivel_3` int(6) DEFAULT '0'
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Extraindo dados da tabela `acertos`
--

INSERT INTO `acertos` (`id_acertos`, `id_usuario`, `nivel_1`, `nivel_2`, `nivel_3`) VALUES
(24, 79, 10, 7, 9);

-- --------------------------------------------------------

--
-- Estrutura da tabela `arquivos`
--

CREATE TABLE `arquivos` (
  `id_arquivo` int(11) NOT NULL,
  `nome_arquivo` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `diretorio_arquivo` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Extraindo dados da tabela `arquivos`
--

INSERT INTO `arquivos` (`id_arquivo`, `nome_arquivo`, `diretorio_arquivo`) VALUES
(35, 'introducao_Linguagem_C.pdf', '../../assets/arquivos/livros/introducao_Linguagem_C.pdf'),
(36, 'operadores_aritimeticos.pdf', '../../assets/arquivos/livros/operadores_aritimeticos.pdf'),
(37, 'apostila_completa_C.pdf', '../../assets/arquivos/livros/apostila_completa_C.pdf'),
(38, 'funcoes_Scanf_Printf_C.pdf', '../../assets/arquivos/livros/funcoes_Scanf_Printf_C.pdf'),
(39, 'vetores_e_strings.pdf', '../../assets/arquivos/livros/vetores_e_strings.pdf'),
(43, 'testearquivo.pdf', '../../assets/arquivos/livros/testearquivo.pdf');

-- --------------------------------------------------------

--
-- Estrutura da tabela `resultado`
--

CREATE TABLE `resultado` (
  `valor_final` double DEFAULT NULL,
  `usabilidade` varchar(100) NOT NULL,
  `descricao` varchar(20) NOT NULL,
  `trintaum` varchar(255) NOT NULL,
  `trintadois` varchar(255) NOT NULL,
  `trintatres` varchar(255) NOT NULL,
  `trintaquatro` varchar(255) NOT NULL,
  `trintacinco` varchar(255) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Extraindo dados da tabela `resultado`
--

INSERT INTO `resultado` (`valor_final`, `usabilidade`, `descricao`, `trintaum`, `trintadois`, `trintatres`, `trintaquatro`, `trintacinco`) VALUES
(8.8888888888889, '4', '', '', '', '', '', ''),
(0.37037037037037, '0', '', '', '', '', '', ''),
(10, '4', '', '', '', '', '', ''),
(10, '4', 'MUITO ALTA', '', '', '', '', ''),
(10, '4', 'MUITO ALTA', '', '', '', '', ''),
(2.9050925925926, '1', 'BAIXA', '', '', '', '', ''),
(8.1481481481481, '4', 'MUITO ALTA', '', '', '', '', ''),
(8.1481481481481, '4', 'MUITO ALTA', 'LAYOUT DAS TELAS', '', '', '', ''),
(8.1481481481481, '4', 'MUITO ALTA', '', '', '', '', ''),
(8.1481481481481, '4', 'MUITO ALTA', 'teste operador ternÃ¡rio', '', '', '', ''),
(8.1481481481481, '4', 'MUITO ALTA', '', '', '', '', ''),
(8.1481481481481, '4', 'MUITO ALTA', 'fdsfsdfs 1', 'wqdwefergreg', 'rtntykjtrdtsfsdf hgrvd', 'grgbgrtnjumkbvv ', 'ghtersjikuytwsgdio'),
(8.1481481481481, '4', 'MUITO ALTA', '', '', '', 'zdcdsfda', '');

-- --------------------------------------------------------

--
-- Estrutura da tabela `usuarios`
--

CREATE TABLE `usuarios` (
  `id` int(11) NOT NULL,
  `nome` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `usuario` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `senha` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `perfil` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `nivel` varchar(10) COLLATE utf8_unicode_ci NOT NULL,
  `admin` int(2) DEFAULT '0',
  `imagem` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Extraindo dados da tabela `usuarios`
--

INSERT INTO `usuarios` (`id`, `nome`, `usuario`, `senha`, `perfil`, `nivel`, `admin`, `imagem`) VALUES
(69, 'Adminstrador', 'admin', '96aa20fe60afaf615f37142a1a654c1b', 'indefinido', '0', 1, ''),
(79, 'Luiz Henrique Dias da Costa Campos', 'ziquedc', 'e10adc3949ba59abbe56e057f20f883e', 'impulsivo', 'Completo', 0, '');

--
-- Indexes for dumped tables
--

--
-- Indexes for table `acertos`
--
ALTER TABLE `acertos`
  ADD PRIMARY KEY (`id_acertos`),
  ADD KEY `id_usuario` (`id_usuario`);

--
-- Indexes for table `arquivos`
--
ALTER TABLE `arquivos`
  ADD PRIMARY KEY (`id_arquivo`);

--
-- Indexes for table `usuarios`
--
ALTER TABLE `usuarios`
  ADD PRIMARY KEY (`id`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `acertos`
--
ALTER TABLE `acertos`
  MODIFY `id_acertos` int(6) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=25;

--
-- AUTO_INCREMENT for table `arquivos`
--
ALTER TABLE `arquivos`
  MODIFY `id_arquivo` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=44;

--
-- AUTO_INCREMENT for table `usuarios`
--
ALTER TABLE `usuarios`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=80;

--
-- Constraints for dumped tables
--

--
-- Limitadores para a tabela `acertos`
--
ALTER TABLE `acertos`
  ADD CONSTRAINT `acertos_ibfk_1` FOREIGN KEY (`id_usuario`) REFERENCES `usuarios` (`id`);
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
