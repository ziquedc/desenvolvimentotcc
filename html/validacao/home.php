<!DOCTYPE html>
<html lang="en">

<head>

  <meta charset="utf-8">
  <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
  <meta name="description" content="">
  <meta name="author" content="">

  <title>HEURISTICA</title>

  <!-- Bootstrap core CSS -->
  <link href="vendor/bootstrap/css/bootstrap.min.css" rel="stylesheet">

</head>

<body>

    <div class="container">
      <div class="py-5 text-center">
        <img class="d-block mx-auto mb-4" src="pesquisa.png" width="100" height="100">
        <h3>HEURISTICA PARA CÁLCULO DA USABILIDADE GERAL</h3>
             </div>

      <div class="row">
        
        <div class="col-md-8 order-md-1">
          <!-- Botão para acionar modal -->


 <form method="post" action="validar.php" name="">

          	 <h5 class="mb-3">1 - Os links para outras fontes são de grande auxílio para minha aprendizagem.</h5>

            <div class="d-block my-3">
              <div class="custom-control custom-radio">
                <input id="um-1" name="um" value="10" type="radio" class="custom-control-input" checked required>
                <label class="custom-control-label" for="um-1">Sim</label>
              </div>
              <div class="custom-control custom-radio">
                <input  id="um-2" name="um" value="7.5" type="radio" class="custom-control-input" required>
                <label class="custom-control-label" for="um-2">Parcialmente com poucas restrições </label>
              </div>
              <div class="custom-control custom-radio">
                <input  id="um-3" name="um" value="5" type="radio" class="custom-control-input" required>
                <label class="custom-control-label" for="um-3">Parcialmente </label>
              </div>
               <div class="custom-control custom-radio">
                <input  id="um-4" name="um" value="2.5" type="radio" class="custom-control-input" required>
                <label class="custom-control-label" for="um-4"> Parcialmente com muitas restrições </label>
              </div>
                <div class="custom-control custom-radio">
                <input  id="um-5" name="um" value="0" type="radio" class="custom-control-input" required>
                <label class="custom-control-label" for="um-5"> Não </label>
              </div>
            </div>
           
          
           
            <hr class="mb-4">

             <h5 class="mb-3">2 - E mais útil aprender tópicos com este material no computador do que com livros em sala de aula.</h5>

            <div class="d-block my-3">
              <div class="custom-control custom-radio">
                <input id="dois-1" name="dois" value="10" type="radio" class="custom-control-input" checked required>
                <label class="custom-control-label" for="dois-1">Sim</label>
              </div>
              <div class="custom-control custom-radio">
                <input  id="dois-2" name="dois" value="7.5" type="radio" class="custom-control-input" required>
                <label class="custom-control-label" for="dois-2">Parcialmente com poucas restrições </label>
              </div>
              <div class="custom-control custom-radio">
                <input  id="dois-3" name="dois" value="5" type="radio" class="custom-control-input" required>
                <label class="custom-control-label" for="dois-3">Parcialmente </label>
              </div>
               <div class="custom-control custom-radio">
                <input  id="dois-4" name="dois" value="2.5" type="radio" class="custom-control-input" required>
                <label class="custom-control-label" for="dois-4"> Parcialmente com muitas restrições </label>
              </div>
                <div class="custom-control custom-radio">
                <input  id="dois-5" name="dois" value="0" type="radio" class="custom-control-input" required>
                <label class="custom-control-label" for="dois-5"> Não </label>
              </div>
            </div>

            <hr class="mb-4">

             <h5 class="mb-3">3 - E rápido e fácil o aprendizado de um novo tópico ou o recapitular de um tópico anterior.</h5>

            <div class="d-block my-3">
              <div class="custom-control custom-radio">
                <input id="tres-1" name="tres" value="10" type="radio" class="custom-control-input" checked required>
                <label class="custom-control-label" for="tres-1">Sim</label>
              </div>
              <div class="custom-control custom-radio">
                <input  id="tres-2" name="tres" value="7.5" type="radio" class="custom-control-input" required>
                <label class="custom-control-label" for="tres-2">Parcialmente com poucas restrições </label>
              </div>
              <div class="custom-control custom-radio">
                <input  id="tres-3" name="tres" value="5" type="radio" class="custom-control-input" required>
                <label class="custom-control-label" for="tres-3">Parcialmente </label>
              </div>
               <div class="custom-control custom-radio">
                <input  id="tres-4" name="tres" value="2.5" type="radio" class="custom-control-input" required>
                <label class="custom-control-label" for="tres-4"> Parcialmente com muitas restrições </label>
              </div>
                <div class="custom-control custom-radio">
                <input  id="tres-5" name="tres" value="0" type="radio" class="custom-control-input" required>
                <label class="custom-control-label" for="tres-5"> Não </label>
              </div>
            </div>

            <hr class="mb-4">

             <h5 class="mb-3">4 - Quando erro na solução de uma tarefa, o programa me envia um aviso amigável</h5>

            <div class="d-block my-3">
              <div class="custom-control custom-radio">
                <input id="quatro-1" name="quatro" value="10" type="radio" class="custom-control-input" checked required>
                <label class="custom-control-label" for="quatro-1">Sim</label>
              </div>
              <div class="custom-control custom-radio">
                <input  id="quatro-2" name="quatro" value="7.5" type="radio" class="custom-control-input" required>
                <label class="custom-control-label" for="quatro-2">Parcialmente com poucas restrições </label>
              </div>
              <div class="custom-control custom-radio">
                <input  id="quatro-3" name="quatro" value="5" type="radio" class="custom-control-input" required>
                <label class="custom-control-label" for="quatro-3">Parcialmente </label>
              </div>
               <div class="custom-control custom-radio">
                <input  id="quatro-4" name="quatro" value="2.5" type="radio" class="custom-control-input" required>
                <label class="custom-control-label" for="quatro-4"> Parcialmente com muitas restrições </label>
              </div>
                <div class="custom-control custom-radio">
                <input  id="quatro-5" name="quatro" value="0" type="radio" class="custom-control-input" required>
                <label class="custom-control-label" for="quatro-5"> Não </label>
              </div>
            </div>

           <hr class="mb-4">

             <h5 class="mb-3">5 - As animações ajudam a aprender.</h5>

            <div class="d-block my-3">
              <div class="custom-control custom-radio">
                <input id="cinco-1" name="cinco" value="10" type="radio" class="custom-control-input" checked required>
                <label class="custom-control-label" for="cinco-1">Sim</label>
              </div>
              <div class="custom-control custom-radio">
                <input  id="cinco-2" name="cinco" value="7.5" type="radio" class="custom-control-input" required>
                <label class="custom-control-label" for="cinco-2">Parcialmente com poucas restrições </label>
              </div>
              <div class="custom-control custom-radio">
                <input  id="cinco-3" name="cinco" value="5" type="radio" class="custom-control-input" required>
                <label class="custom-control-label" for="cinco-3">Parcialmente </label>
              </div>
               <div class="custom-control custom-radio">
                <input  id="cinco-4" name="cinco" value="2.5" type="radio" class="custom-control-input" required>
                <label class="custom-control-label" for="cinco-4"> Parcialmente com muitas restrições </label>
              </div>
                <div class="custom-control custom-radio">
                <input  id="cinco-5" name="cinco" value="0" type="radio" class="custom-control-input" required>
                <label class="custom-control-label" for="cinco-5"> Não </label>
              </div>
            </div>

             <hr class="mb-4">

             <h5 class="mb-3">6 - Os sons ajudam a aprender.</h5>

            <div class="d-block my-3">
              <div class="custom-control custom-radio">
                <input id="seis-1" name="seis" value="10" type="radio" class="custom-control-input" checked required>
                <label class="custom-control-label" for="seis-1">Sim</label>
              </div>
              <div class="custom-control custom-radio">
                <input  id="seis-2" name="seis" value="7.5" type="radio" class="custom-control-input" required>
                <label class="custom-control-label" for="seis-2">Parcialmente com poucas restrições </label>
              </div>
              <div class="custom-control custom-radio">
                <input  id="seis-3" name="seis" value="5" type="radio" class="custom-control-input" required>
                <label class="custom-control-label" for="seis-3">Parcialmente </label>
              </div>
               <div class="custom-control custom-radio">
                <input  id="seis-4" name="seis" value="2.5" type="radio" class="custom-control-input" required>
                <label class="custom-control-label" for="seis-4"> Parcialmente com muitas restrições </label>
              </div>
                <div class="custom-control custom-radio">
                <input  id="seis-5" name="seis" value="0" type="radio" class="custom-control-input" required>
                <label class="custom-control-label" for="seis-5"> Não </label>
              </div>
            </div>

             <hr class="mb-4">

             <h5 class="mb-3">7 - Os links das páginas correspondem aos títulos das páginas às quais estão vinculadas.</h5>

            <div class="d-block my-3">
              <div class="custom-control custom-radio">
                <input id="sete-1" name="sete" value="10" type="radio" class="custom-control-input" checked required>
                <label class="custom-control-label" for="sete-1">Sim</label>
              </div>
              <div class="custom-control custom-radio">
                <input  id="sete-2" name="sete" value="7.5" type="radio" class="custom-control-input" required>
                <label class="custom-control-label" for="sete-2">Parcialmente com poucas restrições </label>
              </div>
              <div class="custom-control custom-radio">
                <input  id="sete-3" name="sete" value="5" type="radio" class="custom-control-input" required>
                <label class="custom-control-label" for="sete-3">Parcialmente </label>
              </div>
               <div class="custom-control custom-radio">
                <input  id="sete-4" name="sete" value="2.5" type="radio" class="custom-control-input" required>
                <label class="custom-control-label" for="sete-4"> Parcialmente com muitas restrições </label>
              </div>
                <div class="custom-control custom-radio">
                <input  id="sete-5" name="sete" value="0" type="radio" class="custom-control-input" required>
                <label class="custom-control-label" for="sete-5"> Não </label>
              </div>
            </div>

             <hr class="mb-4">

             <h5 class="mb-3">8 - Acho fácil entrar com informações no sistema.</h5>

            <div class="d-block my-3">
              <div class="custom-control custom-radio">
                <input id="oito-1" name="oito" value="10" type="radio" class="custom-control-input" checked required>
                <label class="custom-control-label" for="oito-1">Sim</label>
              </div>
              <div class="custom-control custom-radio">
                <input  id="oito-2" name="oito" value="7.5" type="radio" class="custom-control-input" required>
                <label class="custom-control-label" for="oito-2">Parcialmente com poucas restrições </label>
              </div>
              <div class="custom-control custom-radio">
                <input  id="oito-3" name="oito" value="5" type="radio" class="custom-control-input" required>
                <label class="custom-control-label" for="oito-3">Parcialmente </label>
              </div>
               <div class="custom-control custom-radio">
                <input  id="oito-4" name="oito" value="2.5" type="radio" class="custom-control-input" required>
                <label class="custom-control-label" for="oito-4"> Parcialmente com muitas restrições </label>
              </div>
                <div class="custom-control custom-radio">
                <input  id="oito-5" name="oito" value="0" type="radio" class="custom-control-input" required>
                <label class="custom-control-label" for="oito-5"> Não </label>
              </div>
            </div>

             <hr class="mb-4">

             <h5 class="mb-3">9 - O conteúdo mantém minha atenção.</h5>

            <div class="d-block my-3">
              <div class="custom-control custom-radio">
                <input id="nove-1" name="nove" value="10" type="radio" class="custom-control-input" checked required>
                <label class="custom-control-label" for="nove-1">Sim</label>
              </div>
              <div class="custom-control custom-radio">
                <input  id="nove-2" name="nove" value="7.5" type="radio" class="custom-control-input" required>
                <label class="custom-control-label" for="nove-2">Parcialmente com poucas restrições </label>
              </div>
              <div class="custom-control custom-radio">
                <input  id="nove-3" name="nove" value="5" type="radio" class="custom-control-input" required>
                <label class="custom-control-label" for="nove-3">Parcialmente </label>
              </div>
               <div class="custom-control custom-radio">
                <input  id="nove-4" name="nove" value="2.5" type="radio" class="custom-control-input" required>
                <label class="custom-control-label" for="nove-4"> Parcialmente com muitas restrições </label>
              </div>
                <div class="custom-control custom-radio">
                <input  id="nove-5" name="nove" value="0" type="radio" class="custom-control-input" required>
                <label class="custom-control-label" for="nove-5"> Não </label>
              </div>
            </div>

             <hr class="mb-4">

             <h5 class="mb-3">10 - O material de aprendizagem me faz querer aprender, sinto-me motivado.</h5>

            <div class="d-block my-3">
              <div class="custom-control custom-radio">
                <input id="dez-1" name="dez" value="10" type="radio" class="custom-control-input" checked required>
                <label class="custom-control-label" for="dez-1">Sim</label>
              </div>
              <div class="custom-control custom-radio">
                <input  id="dez-2" name="dez" value="7.5" type="radio" class="custom-control-input" required>
                <label class="custom-control-label" for="dez-2">Parcialmente com poucas restrições </label>
              </div>
              <div class="custom-control custom-radio">
                <input  id="dez-3" name="dez" value="5" type="radio" class="custom-control-input" required>
                <label class="custom-control-label" for="dez-3">Parcialmente </label>
              </div>
               <div class="custom-control custom-radio">
                <input  id="dez-4" name="dez" value="2.5" type="radio" class="custom-control-input" required>
                <label class="custom-control-label" for="dez-4"> Parcialmente com muitas restrições </label>
              </div>
                <div class="custom-control custom-radio">
                <input  id="dez-5" name="dez" value="0" type="radio" class="custom-control-input" required>
                <label class="custom-control-label" for="dez-5"> Não </label>
              </div>
            </div>

             <hr class="mb-4">

             <h5 class="mb-3">11 - As informações importantes são colocadas no topo da página.</h5>

            <div class="d-block my-3">
              <div class="custom-control custom-radio">
                <input id="onze-1" name="onze" value="10" type="radio" class="custom-control-input" checked required>
                <label class="custom-control-label" for="onze-1">Sim</label>
              </div>
              <div class="custom-control custom-radio">
                <input  id="onze-2" name="onze" value="7.5" type="radio" class="custom-control-input" required>
                <label class="custom-control-label" for="onze-2">Parcialmente com poucas restrições </label>
              </div>
              <div class="custom-control custom-radio">
                <input  id="onze-3" name="onze" value="5" type="radio" class="custom-control-input" required>
                <label class="custom-control-label" for="onze-3">Parcialmente </label>
              </div>
               <div class="custom-control custom-radio">
                <input  id="onze-4" name="onze" value="2.5" type="radio" class="custom-control-input" required>
                <label class="custom-control-label" for="onze-4"> Parcialmente com muitas restrições </label>
              </div>
                <div class="custom-control custom-radio">
                <input  id="onze-5" name="onze" value="0" type="radio" class="custom-control-input" required>
                <label class="custom-control-label" for="onze-5"> Não </label>
              </div>
            </div>

             <hr class="mb-4">

             <h5 class="mb-3">12 - As questões para a aprendizagem não seguem sempre o mesmo modelo para suas resoluções.</h5>

            <div class="d-block my-3">
              <div class="custom-control custom-radio">
                <input id="doze-1" name="doze" value="10" type="radio" class="custom-control-input" checked required>
                <label class="custom-control-label" for="doze-1">Sim</label>
              </div>
              <div class="custom-control custom-radio">
                <input  id="doze-2" name="doze" value="7.5" type="radio" class="custom-control-input" required>
                <label class="custom-control-label" for="doze-2">Parcialmente com poucas restrições </label>
              </div>
              <div class="custom-control custom-radio">
                <input  id="doze-3" name="doze" value="5" type="radio" class="custom-control-input" required>
                <label class="custom-control-label" for="doze-3">Parcialmente </label>
              </div>
               <div class="custom-control custom-radio">
                <input  id="doze-4" name="doze" value="2.5" type="radio" class="custom-control-input" required>
                <label class="custom-control-label" for="doze-4"> Parcialmente com muitas restrições </label>
              </div>
                <div class="custom-control custom-radio">
                <input  id="doze-5" name="doze" value="0" type="radio" class="custom-control-input" required>
                <label class="custom-control-label" for="doze-5"> Não </label>
              </div>
            </div>

             <hr class="mb-4">

             <h5 class="mb-3">13 - Esqueci de tudo o que estava acontecendo ao meu redor e de quanto tempo se passou.</h5>

            <div class="d-block my-3">
              <div class="custom-control custom-radio">
                <input id="treze-1" name="treze" value="10" type="radio" class="custom-control-input" checked required>
                <label class="custom-control-label" for="treze-1">Sim</label>
              </div>
              <div class="custom-control custom-radio">
                <input  id="treze-2" name="treze" value="7.5" type="radio" class="custom-control-input" required>
                <label class="custom-control-label" for="treze-2">Parcialmente com poucas restrições </label>
              </div>
              <div class="custom-control custom-radio">
                <input  id="treze-3" name="treze" value="5" type="radio" class="custom-control-input" required>
                <label class="custom-control-label" for="treze-3">Parcialmente </label>
              </div>
               <div class="custom-control custom-radio">
                <input  id="treze-4" name="treze" value="2.5" type="radio" class="custom-control-input" required>
                <label class="custom-control-label" for="treze-4"> Parcialmente com muitas restrições </label>
              </div>
                <div class="custom-control custom-radio">
                <input  id="treze-5" name="treze" value="0" type="radio" class="custom-control-input" required>
                <label class="custom-control-label" for="treze-5"> Não </label>
              </div>
            </div>

             <hr class="mb-4">

             <h5 class="mb-3">14 - O material de aprendizagem avalia meus desempenhos com classificações (notas) de minhas atividades.</h5>

            <div class="d-block my-3">
              <div class="custom-control custom-radio">
                <input id="quatorze-1" name="quatorze" value="10" type="radio" class="custom-control-input" checked required>
                <label class="custom-control-label" for="quatorze-1">Sim</label>
              </div>
              <div class="custom-control custom-radio">
                <input  id="quatorze-2" name="quatorze" value="7.5" type="radio" class="custom-control-input" required>
                <label class="custom-control-label" for="quatorze-2">Parcialmente com poucas restrições </label>
              </div>
              <div class="custom-control custom-radio">
                <input  id="quatorze-3" name="quatorze" value="5" type="radio" class="custom-control-input" required>
                <label class="custom-control-label" for="quatorze-3">Parcialmente </label>
              </div>
               <div class="custom-control custom-radio">
                <input  id="quatorze-4" name="quatorze" value="2.5" type="radio" class="custom-control-input" required>
                <label class="custom-control-label" for="quatorze-4"> Parcialmente com muitas restrições </label>
              </div>
                <div class="custom-control custom-radio">
                <input  id="quatorze-5" name="quatorze" value="0" type="radio" class="custom-control-input" required>
                <label class="custom-control-label" for="quatorze-5"> Não </label>
              </div>
            </div>
         <hr class="mb-4">

        

             <h5 class="mb-3">15 - Este material de aprendizagem não me deixa prosseguir para o próximo ponto ou exercício antes de ter respondido corretamente a cada questão.</h5>

            <div class="d-block my-3">
              <div class="custom-control custom-radio">
                <input id="quinze-1" name="quinze" value="10" type="radio" class="custom-control-input" checked required>
                <label class="custom-control-label" for="quinze-1">Sim</label>
              </div>
              <div class="custom-control custom-radio">
                <input  id="quinze-2" name="quinze" value="7.5" type="radio" class="custom-control-input" required>
                <label class="custom-control-label" for="quinze-2">Parcialmente com poucas restrições </label>
              </div>
              <div class="custom-control custom-radio">
                <input  id="quinze-3" name="quinze" value="5" type="radio" class="custom-control-input" required>
                <label class="custom-control-label" for="quinze-3">Parcialmente </label>
              </div>
               <div class="custom-control custom-radio">
                <input  id="quinze-4" name="quinze" value="2.5" type="radio" class="custom-control-input" required>
                <label class="custom-control-label" for="quinze-4"> Parcialmente com muitas restrições </label>
              </div>
                <div class="custom-control custom-radio">
                <input  id="quinze-5" name="quinze" value="0" type="radio" class="custom-control-input" required>
                <label class="custom-control-label" for="quinze-5"> Não </label>
              </div>
            </div>

             <hr class="mb-4">

             <h5 class="mb-3">16 - A linguagem usada é natural. Os termos, frases, conceitos são similares àqueles usados no meu dia-a-dia ou no ambiente de estudo.</h5>

            <div class="d-block my-3">
              <div class="custom-control custom-radio">
                <input id="dezesseis-1" name="dezesseis" value="10" type="radio" class="custom-control-input" checked required>
                <label class="custom-control-label" for="dezesseis-1">Sim</label>
              </div>
              <div class="custom-control custom-radio">
                <input  id="dezesseis-2" name="dezesseis" value="7.5" type="radio" class="custom-control-input" required>
                <label class="custom-control-label" for="dezesseis-2">Parcialmente com poucas restrições </label>
              </div>
              <div class="custom-control custom-radio">
                <input  id="dezesseis-3" name="dezesseis" value="5" type="radio" class="custom-control-input" required>
                <label class="custom-control-label" for="dezesseis-3">Parcialmente </label>
              </div>
               <div class="custom-control custom-radio">
                <input  id="dezesseis-4" name="dezesseis" value="2.5" type="radio" class="custom-control-input" required>
                <label class="custom-control-label" for="dezesseis-4"> Parcialmente com muitas restrições </label>
              </div>
                <div class="custom-control custom-radio">
                <input  id="dezesseis-5" name="dezesseis" value="0" type="radio" class="custom-control-input" required>
                <label class="custom-control-label" for="dezesseis-5"> Não </label>
              </div>
            </div>

             <hr class="mb-4">

             <h5 class="mb-3">17 - As imagens ajudam a aprender.</h5>

            <div class="d-block my-3">
              <div class="custom-control custom-radio">
                <input id="dezessete-1" name="dezessete" value="10" type="radio" class="custom-control-input" checked required>
                <label class="custom-control-label" for="dezessete-1">Sim</label>
              </div>
              <div class="custom-control custom-radio">
                <input  id="dezessete-2" name="dezessete" value="7.5" type="radio" class="custom-control-input" required>
                <label class="custom-control-label" for="dezessete-2">Parcialmente com poucas restrições </label>
              </div>
              <div class="custom-control custom-radio">
                <input  id="dezessete-3" name="dezessete" value="5" type="radio" class="custom-control-input" required>
                <label class="custom-control-label" for="dezessete-3">Parcialmente </label>
              </div>
               <div class="custom-control custom-radio">
                <input  id="dezessete-4" name="dezessete" value="2.5" type="radio" class="custom-control-input" required>
                <label class="custom-control-label" for="dezessete-4"> Parcialmente com muitas restrições </label>
              </div>
                <div class="custom-control custom-radio">
                <input  id="dezessete-5" name="dezessete" value="0" type="radio" class="custom-control-input" required>
                <label class="custom-control-label" for="dezessete-5"> Não </label>
              </div>
            </div>

             <hr class="mb-4">

             <h5 class="mb-3">18 - Sinto como se eu estivesse no controle da minha própria aprendizagem ao interagir com o site.</h5>

            <div class="d-block my-3">
              <div class="custom-control custom-radio">
                <input id="dezoito-1" name="dezoito" value="10" type="radio" class="custom-control-input" checked required>
                <label class="custom-control-label" for="dezoito-1">Sim</label>
              </div>
              <div class="custom-control custom-radio">
                <input  id="dezoito-2" name="dezoito" value="7.5" type="radio" class="custom-control-input" required>
                <label class="custom-control-label" for="dezoito-2">Parcialmente com poucas restrições </label>
              </div>
              <div class="custom-control custom-radio">
                <input  id="dezoito-3" name="dezoito" value="5" type="radio" class="custom-control-input" required>
                <label class="custom-control-label" for="dezoito-3">Parcialmente </label>
              </div>
               <div class="custom-control custom-radio">
                <input  id="dezoito-4" name="dezoito" value="2.5" type="radio" class="custom-control-input" required>
                <label class="custom-control-label" for="dezoito-4"> Parcialmente com muitas restrições </label>
              </div>
                <div class="custom-control custom-radio">
                <input  id="dezoito-5" name="dezoito" value="0" type="radio" class="custom-control-input" required>
                <label class="custom-control-label" for="dezoito-5"> Não </label>
              </div>
            </div>

             <hr class="mb-4">

             <h5 class="mb-3">19 - As informações são apresentadas em um formato que os torna fácil de aprende.</h5>

            <div class="d-block my-3">
              <div class="custom-control custom-radio">
                <input id="dezenove-1" name="dezenove" value="10" type="radio" class="custom-control-input" checked required>
                <label class="custom-control-label" for="dezenove-1">Sim</label>
              </div>
              <div class="custom-control custom-radio">
                <input  id="dezenove-2" name="dezenove" value="7.5" type="radio" class="custom-control-input" required>
                <label class="custom-control-label" for="dezenove-2">Parcialmente com poucas restrições </label>
              </div>
              <div class="custom-control custom-radio">
                <input  id="dezenove-3" name="dezenove" value="5" type="radio" class="custom-control-input" required>
                <label class="custom-control-label" for="dezenove-3">Parcialmente </label>
              </div>
               <div class="custom-control custom-radio">
                <input  id="dezenove-4" name="dezenove" value="2.5" type="radio" class="custom-control-input" required>
                <label class="custom-control-label" for="dezenove-4"> Parcialmente com muitas restrições </label>
              </div>
                <div class="custom-control custom-radio">
                <input  id="dezenove-5" name="dezenove" value="0" type="radio" class="custom-control-input" required>
                <label class="custom-control-label" for="dezenove-5"> Não </label>
              </div>
            </div>

             <hr class="mb-4">

             <h5 class="mb-3">20 - As cores são usadas da mesma forma em todo o site.</h5>

            <div class="d-block my-3">
              <div class="custom-control custom-radio">
                <input id="vinte-1" name="vinte" value="10" type="radio" class="custom-control-input" checked required>
                <label class="custom-control-label" for="vinte-1">Sim</label>
              </div>
              <div class="custom-control custom-radio">
                <input  id="vinte-2" name="vinte" value="7.5" type="radio" class="custom-control-input" required>
                <label class="custom-control-label" for="vinte-2">Parcialmente com poucas restrições </label>
              </div>
              <div class="custom-control custom-radio">
                <input  id="vinte-3" name="vinte" value="5" type="radio" class="custom-control-input" required>
                <label class="custom-control-label" for="vinte-3">Parcialmente </label>
              </div>
               <div class="custom-control custom-radio">
                <input  id="vinte-4" name="vinte" value="2.5" type="radio" class="custom-control-input" required>
                <label class="custom-control-label" for="vinte-4"> Parcialmente com muitas restrições </label>
              </div>
                <div class="custom-control custom-radio">
                <input  id="vinte-5" name="vinte" value="0" type="radio" class="custom-control-input" required>
                <label class="custom-control-label" for="vinte-5"> Não </label>
              </div>
            </div>

             <hr class="mb-4">

             <h5 class="mb-3">21 - O site guia os usuários experientes através de atalhos em forma de abreviações, questões especiais, macros e comandos escondidos.</h5>

            <div class="d-block my-3">
              <div class="custom-control custom-radio">
                <input id="vinteum-1" name="vinteum" value="10" type="radio" class="custom-control-input" checked required>
                <label class="custom-control-label" for="vinteum-1">Sim</label>
              </div>
              <div class="custom-control custom-radio">
                <input  id="vinteum-2" name="vinteum" value="7.5" type="radio" class="custom-control-input" required>
                <label class="custom-control-label" for="vinteum-2">Parcialmente com poucas restrições </label>
              </div>
              <div class="custom-control custom-radio">
                <input  id="vinteum-3" name="vinteum" value="5" type="radio" class="custom-control-input" required>
                <label class="custom-control-label" for="vinteum-3">Parcialmente </label>
              </div>
               <div class="custom-control custom-radio">
                <input  id="vinteum-4" name="vinteum" value="2.5" type="radio" class="custom-control-input" required>
                <label class="custom-control-label" for="vinteum-4"> Parcialmente com muitas restrições </label>
              </div>
                <div class="custom-control custom-radio">
                <input  id="vinteum-5" name="vinteum" value="0" type="radio" class="custom-control-input" required>
                <label class="custom-control-label" for="vinteum-5"> Não </label>
              </div>
            </div>

             <hr class="mb-4">

             <h5 class="mb-3">22 - As mensagens de erros indicam precisamente qual é o problema.</h5>

            <div class="d-block my-3">
              <div class="custom-control custom-radio">
                <input id="vintedois-1" name="vintedois" value="10" type="radio" class="custom-control-input" checked required>
                <label class="custom-control-label" for="vintedois-1">Sim</label>
              </div>
              <div class="custom-control custom-radio">
                <input  id="vintedois-2" name="vintedois" value="7.5" type="radio" class="custom-control-input" required>
                <label class="custom-control-label" for="vintedois-2">Parcialmente com poucas restrições </label>
              </div>
              <div class="custom-control custom-radio">
                <input  id="vintedois-3" name="vintedois" value="5" type="radio" class="custom-control-input" required>
                <label class="custom-control-label" for="vintedois-3">Parcialmente </label>
              </div>
               <div class="custom-control custom-radio">
                <input  id="vintedois-4" name="vintedois" value="2.5" type="radio" class="custom-control-input" required>
                <label class="custom-control-label" for="vintedois-4"> Parcialmente com muitas restrições </label>
              </div>
                <div class="custom-control custom-radio">
                <input  id="vintedois-5" name="vintedois" value="0" type="radio" class="custom-control-input" required>
                <label class="custom-control-label" for="vintedois-5"> Não </label>
              </div>
            </div>

             <hr class="mb-4">

             <h5 class="mb-3">23 - As mensagens de erro indicam um procedimento para a correção do erro.</h5>

            <div class="d-block my-3">
              <div class="custom-control custom-radio">
                <input id="vintetres-1" name="vintetres" value="10" type="radio" class="custom-control-input" checked required>
                <label class="custom-control-label" for="vintetres-1">Sim</label>
              </div>
              <div class="custom-control custom-radio">
                <input  id="vintetres-2" name="vintetres" value="7.5" type="radio" class="custom-control-input" required>
                <label class="custom-control-label" for="vintetres-2">Parcialmente com poucas restrições </label>
              </div>
              <div class="custom-control custom-radio">
                <input  id="vintetres-3" name="vintetres" value="5" type="radio" class="custom-control-input" required>
                <label class="custom-control-label" for="vintetres-3">Parcialmente </label>
              </div>
               <div class="custom-control custom-radio">
                <input  id="vintetres-4" name="vintetres" value="2.5" type="radio" class="custom-control-input" required>
                <label class="custom-control-label" for="vintetres-4"> Parcialmente com muitas restrições </label>
              </div>
                <div class="custom-control custom-radio">
                <input  id="vintetres-5" name="vintetres" value="0" type="radio" class="custom-control-input" required>
                <label class="custom-control-label" for="vintetres-5"> Não </label>
              </div>
            </div>

             <hr class="mb-4">

             <h5 class="mb-3">24 - Sei onde estou e quais opções seguir, isto é, o que concluí e o que ainda devo fazer.</h5>

            <div class="d-block my-3">
              <div class="custom-control custom-radio">
                <input id="vintequatro-1" name="vintequatro" value="10" type="radio" class="custom-control-input" checked required>
                <label class="custom-control-label" for="vintequatro-1">Sim</label>
              </div>
              <div class="custom-control custom-radio">
                <input  id="vintequatro-2" name="vintequatro" value="7.5" type="radio" class="custom-control-input" required>
                <label class="custom-control-label" for="vintequatro-2">Parcialmente com poucas restrições </label>
              </div>
              <div class="custom-control custom-radio">
                <input  id="vintequatro-3" name="vintequatro" value="5" type="radio" class="custom-control-input" required>
                <label class="custom-control-label" for="vintequatro-3">Parcialmente </label>
              </div>
               <div class="custom-control custom-radio">
                <input  id="vintequatro-4" name="vintequatro" value="2.5" type="radio" class="custom-control-input" required>
                <label class="custom-control-label" for="vintequatro-4"> Parcialmente com muitas restrições </label>
              </div>
                <div class="custom-control custom-radio">
                <input  id="vintequatro-5" name="vintequatro" value="0" type="radio" class="custom-control-input" required>
                <label class="custom-control-label" for="vintequatro-5"> Não </label>
              </div>
            </div>

             <hr class="mb-4">

             <h5 class="mb-3">25 - O feedback (aviso/resposta dado pelo sistema) é imediato.</h5>

            <div class="d-block my-3">
              <div class="custom-control custom-radio">
                <input id="vintecinco-1" name="vintecinco" value="10" type="radio" class="custom-control-input" checked required>
                <label class="custom-control-label" for="vintecinco-1">Sim</label>
              </div>
              <div class="custom-control custom-radio">
                <input  id="vintecinco-2" name="vintecinco" value="7.5" type="radio" class="custom-control-input" required>
                <label class="custom-control-label" for="vintecinco-2">Parcialmente com poucas restrições </label>
              </div>
              <div class="custom-control custom-radio">
                <input  id="vintecinco-3" name="vintecinco" value="5" type="radio" class="custom-control-input" required>
                <label class="custom-control-label" for="vintecinco-3">Parcialmente </label>
              </div>
               <div class="custom-control custom-radio">
                <input  id="vintecinco-4" name="vintecinco" value="2.5" type="radio" class="custom-control-input" required>
                <label class="custom-control-label" for="vintecinco-4"> Parcialmente com muitas restrições </label>
              </div>
                <div class="custom-control custom-radio">
                <input  id="vintecinco-5" name="vintecinco" value="0" type="radio" class="custom-control-input" required>
                <label class="custom-control-label" for="vintecinco-5"> Não </label>
              </div>
            </div>

             <hr class="mb-4">

             <h5 class="mb-3">26 - Não fico confuso com a forma com que os símbolos, ícones, imagens são usados.</h5>

            <div class="d-block my-3">
              <div class="custom-control custom-radio">
                <input id="vinteseis-1" name="vinteseis" value="10" type="radio" class="custom-control-input" checked required>
                <label class="custom-control-label" for="vinteseis-1">Sim</label>
              </div>
              <div class="custom-control custom-radio">
                <input  id="vinteseis-2" name="vinteseis" value="7.5" type="radio" class="custom-control-input" required>
                <label class="custom-control-label" for="vinteseis-2">Parcialmente com poucas restrições </label>
              </div>
              <div class="custom-control custom-radio">
                <input  id="vinteseis-3" name="vinteseis" value="5" type="radio" class="custom-control-input" required>
                <label class="custom-control-label" for="vinteseis-3">Parcialmente </label>
              </div>
               <div class="custom-control custom-radio">
                <input  id="vinteseis-4" name="vinteseis" value="2.5" type="radio" class="custom-control-input" required>
                <label class="custom-control-label" for="vinteseis-4"> Parcialmente com muitas restrições </label>
              </div>
                <div class="custom-control custom-radio">
                <input  id="vinteseis-5" name="vinteseis" value="0" type="radio" class="custom-control-input" required>
                <label class="custom-control-label" for="vinteseis-5"> Não </label>
              </div>
            </div>

 		<hr class="mb-4">

             <h5 class="mb-3">27 - Tenho de lembrar muitas coisas ao mesmo tempo. Gostaria de usar o papel para escrever algumas anotações.</h5>

            <div class="d-block my-3">
              <div class="custom-control custom-radio">
                <input id="vintesete-1" name="vintesete" value="10" type="radio" class="custom-control-input" checked required>
                <label class="custom-control-label" for="vintesete-1">Sim</label>
              </div>
              <div class="custom-control custom-radio">
                <input  id="vintesete-2" name="vintesete" value="7.5" type="radio" class="custom-control-input" required>
                <label class="custom-control-label" for="vintesete-2">Parcialmente com poucas restrições </label>
              </div>
              <div class="custom-control custom-radio">
                <input  id="vintesete-3" name="vintesete" value="5" type="radio" class="custom-control-input" required>
                <label class="custom-control-label" for="vintesete-3">Parcialmente </label>
              </div>
               <div class="custom-control custom-radio">
                <input  id="vintesete-4" name="vintesete" value="2.5" type="radio" class="custom-control-input" required>
                <label class="custom-control-label" for="vintesete-4"> Parcialmente com muitas restrições </label>
              </div>
                <div class="custom-control custom-radio">
                <input  id="vintesete-5" name="vintesete" value="0" type="radio" class="custom-control-input" required>
                <label class="custom-control-label" for="vintesete-5"> Não </label>
              </div>
            </div>

             <hr class="mb-4">

             <h5 class="mb-3">28 - A qualquer hora que um erro é cometido urna mensagem de erro é apresentada.</h5>

            <div class="d-block my-3">
              <div class="custom-control custom-radio">
                <input id="vinteoito-1" name="vinteoito" value="10" type="radio" class="custom-control-input" checked required>
                <label class="custom-control-label" for="vinteoito-1">Sim</label>
              </div>
              <div class="custom-control custom-radio">
                <input  id="vinteoito-2" name="vinteoito" value="7.5" type="radio" class="custom-control-input" required>
                <label class="custom-control-label" for="vinteoito-2">Parcialmente com poucas restrições </label>
              </div>
              <div class="custom-control custom-radio">
                <input  id="vinteoito-3" name="vinteoito" value="5" type="radio" class="custom-control-input" required>
                <label class="custom-control-label" for="vinteoito-3">Parcialmente </label>
              </div>
               <div class="custom-control custom-radio">
                <input  id="vinteoito-4" name="vinteoito" value="2.5" type="radio" class="custom-control-input" required>
                <label class="custom-control-label" for="vinteoito-4"> Parcialmente com muitas restrições </label>
              </div>
                <div class="custom-control custom-radio">
                <input  id="vinteoito-5" name="vinteoito" value="0" type="radio" class="custom-control-input" required>
                <label class="custom-control-label" for="vinteoito-5"> Não </label>
              </div>
            </div>

             <hr class="mb-4">

             <h5 class="mb-3">29 - Cada página apresenta todos os botões de navegação ou hiperlinks necessários, tais corno, anterior (voltar), próxima e página inicial (homepage).</h5>

            <div class="d-block my-3">
              <div class="custom-control custom-radio">
                <input id="vintenove-1" name="vintenove" value="10" type="radio" class="custom-control-input" checked required>
                <label class="custom-control-label" for="vintenove-1">Sim</label>
              </div>
              <div class="custom-control custom-radio">
                <input  id="vintenove-2" name="vintenove" value="7.5" type="radio" class="custom-control-input" required>
                <label class="custom-control-label" for="vintenove-2">Parcialmente com poucas restrições </label>
              </div>
              <div class="custom-control custom-radio">
                <input  id="vintenove-3" name="vintenove" value="5" type="radio" class="custom-control-input" required>
                <label class="custom-control-label" for="vintenove-3">Parcialmente </label>
              </div>
               <div class="custom-control custom-radio">
                <input  id="vintenove-4" name="vintenove" value="2.5" type="radio" class="custom-control-input" required>
                <label class="custom-control-label" for="vintenove-4"> Parcialmente com muitas restrições </label>
              </div>
                <div class="custom-control custom-radio">
                <input  id="vintenove-5" name="vintenove" value="0" type="radio" class="custom-control-input" required>
                <label class="custom-control-label" for="vintenove-5"> Não </label>
              </div>
            </div>

             <hr class="mb-4">

             <h5 class="mb-3">30 - Este material de aprendizagem ensina habilidades e conhecimentos que necessitarei no futuro. </h5>

            <div class="d-block my-3">
              <div class="custom-control custom-radio">
                <input id="trinta-1" name="trinta" value="10" type="radio" class="custom-control-input" checked required>
                <label class="custom-control-label" for="trinta-1">Sim</label>
              </div>
              <div class="custom-control custom-radio">
                <input  id="trinta-2" name="trinta" value="7.5" type="radio" class="custom-control-input" required>
                <label class="custom-control-label" for="trinta-2">Parcialmente com poucas restrições </label>
              </div>
              <div class="custom-control custom-radio">
                <input  id="trinta-3" name="trinta" value="5" type="radio" class="custom-control-input" required>
                <label class="custom-control-label" for="trinta-3">Parcialmente </label>
              </div>
               <div class="custom-control custom-radio">
                <input  id="trinta-4" name="trinta" value="2.5" type="radio" class="custom-control-input" required>
                <label class="custom-control-label" for="trinta-4"> Parcialmente com muitas restrições </label>
              </div>
                <div class="custom-control custom-radio">
                <input  id="trinta-5" name="trinta" value="0" type="radio" class="custom-control-input" required>
                <label class="custom-control-label" for="trinta-5"> Não </label>
              </div>
            </div>

             <hr class="mb-4">

             <h5 class="mb-3">31 - O que mais o atrai neste site? (questão aberta).</h5>

            <div class="d-block my-3">
              <div class="custom-control custom-radio">
                <textarea class="form-control rounded-0" name="trintaum" rows="10"></textarea>
              </div>
            </div>

             <hr class="mb-4">

             <h5 class="mb-3">32 - O que você mais gostou neste site? (questão aberta)..</h5>

            <div class="d-block my-3">
              <div class="custom-control custom-radio">
                <textarea class="form-control rounded-0"  name="trintadois" rows="10"></textarea>
              </div>
            </div>


             <hr class="mb-4">

             <h5 class="mb-3">33 - O que você menos gostou neste site? (questão aberta).</h5>

            <div class="d-block my-3">
              <div class="custom-control custom-radio">
                <textarea class="form-control rounded-0"  name="trintatres" rows="10"></textarea>
              </div>
            </div>

             <hr class="mb-4">

             <h5 class="mb-3">34 - Use o espaço que segue para preencher no mínimo cinco problemas que você.</h5>

            <div class="d-block my-3">
              <div class="custom-control custom-radio">
                <textarea class="form-control rounded-0"  name="trintaquatro" rows="10"></textarea>
              </div>
            </div>


             <hr class="mb-4">

             <h5 class="mb-3">35 - Encontrou como os mais problemáticos no sistema. (questão aberta).</h5>

            <div class="d-block my-3">
              <div class="custom-control custom-radio">
                <textarea class="form-control rounded-0"  name="trintacinco" rows="10"></textarea>
              </div>
            </div>

             <hr class="mb-4"> 
         <input type="submit" class="btn btn-primary" name="enviar">
         <a href="index.php" class="btn btn-primary">Voltar</a>
       
      
          </form>
        </div>
      </div>

         </div>

    <!-- Bootstrap core JavaScript
    ================================================== -->
    <!-- Placed at the end of the document so the pages load faster -->



         

 <script src="vendor/jquery/jquery.min.js"></script>
  <script src="vendor/bootstrap/js/bootstrap.bundle.min.js"></script>
  <script src="bootbox.min.js"></script>
  </body>

</html>