 

<!doctype html>
<html lang="en">
  <head>
  <title>HEURISTICA</title>

  <meta charset="utf-8">
  <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
  <meta name="description" content="">
  <meta name="author" content="">
  <!-- Bootstrap core CSS -->
  <link href="vendor/bootstrap/css/bootstrap.min.css" rel="stylesheet">
    <!-- Bootstrap core CSS -->

    </style>
    <!-- Custom styles for this template -->
    <link href="signin.css" rel="stylesheet">
  </head>
  
  <body>

    <div class="container">
      <br>

      <div class="jumbotron">
        <img class="d-block mx-auto mb-4" src="usabilidade.png" width="250" height="250">
        <h1>Heurística para Cálculo Da Usabilidade Geral</h1>
        <p class="lead">Heurísticas de usabilidade são definidas como um conjunto de regras
gerais que descrevem propriedades comuns em interfaces usáveis
derivado do conhecimento de aspectos psicológicos, computacionais
e sociológicos dos domínios do problema.
Estas regras são utilizadas tanto para direcionar o design de interface
quanto na avaliação da usabilidade</p>
        <p><a class="btn btn-lg btn-success" href="home.php" role="button">Começar!</a></p>
         <p>&copy; 2019 Ciência da Computação - Unicruz - João Breno Barasuol e Bruno Barbosa</p>
      </div>


   

    </div> <!-- /container -->
</html>
