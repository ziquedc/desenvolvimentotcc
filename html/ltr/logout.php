<?php
 	if(!isset($_SESSION)){ //Verificar se a sessão não já está aberta.
    	session_start();
  	}
  //se a sessão nao for criada no login, será redirecionado de volta para o form de login e matará a sessao atual
  if(isset ($_SESSION['usuario']) == true){
    unset($_SESSION['usuario']); //destroi a sessao do usuário
    unset($_SESSION['perfil']); //destroi a sessao do usuário
    unset($_SESSION['nivel']); //destroi a sessao do usuário
    unset($_SESSION['login']); //destroi a sessao do usuário
    unset($_SESSION['admin']); //destroi a sessao do usuário
    header('location:index.php');
  } 
  
	



?>