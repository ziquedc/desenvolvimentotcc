<?php
if(!isset($_SESSION)){ //Verificar se a sessão não já está aberta.
    session_start();
  }
  //se a sessão nao for criada no login, será redirecionado de volto para o form de login
  if(!isset ($_SESSION['usuario']) == true){
    unset($_SESSION['usuario']);
    header('location:index.php');
  } 
  //se identificar uma sessão abre a página
 ?>
<?php include('cabecalho.php') ?>



        <div class="page-wrapper">
            <!-- ============================================================== -->
            <!-- Bread crumb and right sidebar toggle -->
            <!-- ============================================================== -->
            <div class="page-breadcrumb">
                <div class="row">
                    <div class="col-12 d-flex no-block align-items-center">
                        <h4 class="page-title">Linguagem C</h4>
                        <div class="ml-auto text-right">
                            <nav aria-label="breadcrumb">
                                <ol class="breadcrumb">
                                    <li class="breadcrumb-item"><a href="#">Início</a></li>
                                    <li class="breadcrumb-item active" aria-current="page">Nível 1</li>
                                </ol>
                            </nav>
                        </div>
                    </div>
                </div>
            </div>
            <!-- ============================================================== -->
            <!-- End Bread crumb and right sidebar toggle -->
            <!-- ============================================================== -->
            <!-- ============================================================== -->
            <!-- Container fluid  -->
            <!-- ============================================================== -->
            <div class="container-fluid">
                <!-- ============================================================== -->
                <!-- Start Page Content -->
                <!-- ============================================================== -->
                <div class="row">
                    <div class="col-md-6">
                        <div class="card">
                            <div class="card-body">
                                <h4 class="card-title m-b-0">Árvores Binárias</h4>
                            </div>
                            <div class="comment-widgets scrollable">
                                <!-- ARVORES -->
                                <div class="d-flex flex-row comment-row m-t-0">
                                   
                                    <div class="comment-text w-100">
                                        <h5 class="font-medium"><u><b>O Que é uma árvore?</b></u></h5>  
                                        <span class="m-b-15 d-block">
                                        Uma árvore binária é uma estrutura de dados caracterizada por: Ou não tem elemento algum. Ou tem um elemento distinto, 
                                        denominado raiz, 
                                        com dois ponteiros para duas estruturas diferentes, denominadas sub-árvore esquerda e sub-árvore direita.
                                        </span>

                                        <h5 class="font-medium"><u><b>Utilização</b></u></h5>  
                                        <span class="m-b-15 d-block">
                                            É utilizada em casos onde os dados ou objetos possuem relações "hierárquicas" entre si, onde aparece um nodo destaque
                                            (raiz). Os demais nodos estarão organizados de acordo com seus níveis, onde cada nodo terá nodos ascendentes e descendentes.
                                        </span>


                                        
                                    </div>
                                </div>
                                
                                <div class="d-flex flex-row comment-row m-t-0">
                                    <div class="comment-text w-100">
                                        <h5 class="font-medium"><u><b>Terminologia</u></b></h5>
                                        <img class="img-fluid" alt="Responsive image" src="../../assets/images/conteudos/grau-ex.png" width="auto" height="222px">
                                        <span class="m-b-15 d-block">
                                        <br/>
                                        <b>Grau de um nó:</b> número de sub-árvore de um nó;<br/>
                                        Exemplo: <br/>
                                            <ul>
                                                <li> grau do nó A é 3;</li>
                                                <li> grau do nó F é 1;</li>
                                                <li> grau do nó J é 2;</li>
                                                <li> grau do nó M é 0;</li>
                                            </ul>  
                                        <br/>
                                        <b>Folha:</b> é um nó de grau 0, também chamado nó terminal.<br/>
                                        Exemplo: <br/>
                                            <ul>
                                                <li> são folhas os nós E, K, G, H, I, L, M;</li>
                                            </ul>

                                        <br/>
                                        <b>Nível do nó:</b> é o número de ligações entre o nó e a raiz, ou o
                                            comprimento do caminho que o nó à raiz. O nível da raiz é,
                                            obviamente, 0.<br/>
                                        Exemplo: <br/>
                                            <ul>
                                                <li> nível do nó A é 0;</li>
                                                <li> nível do nó B é 1;</li>
                                                <li> nível do nó I é 2;</li>
                                                <li> nível do nó L é 3;</li>
                                            </ul>

                                            <b>Altura da árvore:</b> mais alto nível da árvore acrescido de 1.<br/>
                                        Exemplo: <br/>
                                            <ul>
                                                <li>a altura é 3 +1 = 4</li>
                                            </ul>
                                            <img class="img-fluid" alt="Responsive image"  src="../../assets/images/conteudos/terminologia.png" height="80%"><br/>
                                            <img class="img-fluid" alt="Responsive image"  src="../../assets/images/conteudos/terminologia-floresta.png" height="80%">

                                        <div class="comment-footer">
                                        </div>
                                    </div>
                                </div>
                                <!-- END ARVORES -->

                               <!-- ÁRVORES BINÁRIAS-->
                                <div class="d-flex flex-row comment-row m-t-0">
                                    <div class="comment-text w-100">
                                        <h5 class="font-medium"><u><b>Árvores Binárias</u></b></h5>
                                        <br/>
                                        <img class="img-fluid" alt="Responsive image"  src="../../assets/images/conteudos/arvore-binaria.png" height="80%">
                                        <img class="img-fluid" alt="Responsive image"  src="../../assets/images/conteudos/arvore-binaria-ex.png" height="80%">
                                        <img class="img-fluid" alt="Responsive image"  src="../../assets/images/conteudos/arvore-binaria-3.png" height="80%">
                                        <img class="img-fluid" alt="Responsive image"  src="../../assets/images/conteudos/arvore-binaria-4.png" height="80%">
                                        <img class="img-fluid" alt="Responsive image"  src="../../assets/images/conteudos/arvore-binaria-5.png" height="80%">
                                        

                                       
                                        <div class="comment-footer">
                                            
                                        </div>
                                    </div>
                                </div>
                                
                                <div class="card" style="margin-top: 15px">
                                    <div class="comment-widgets scrollable">
                                        <!-- VARIÁVEIS -->
                                        <div class="d-flex flex-row comment-row m-t-0">
                                        
                                            <div class="comment-text w-100">
                                                
                                                <div class="embed-responsive embed-responsive-4by3    ">
                                                        <iframe class="embed-responsive-item" src="../../assets/arquivos/conteudos/arvores.pdf" allowfullscreen width="500" height="500"></iframe>
                                                        <div style="margin-bottom:5px"> <strong> <a  title="Árvores" target="_blank"></a> </strong> Fonte: <strong></strong> </div>
                                                </div>
                                            </div>
                                        </div>
                                        
                                        
                                    </div>
                                </div>
                                <!-- END ÁRVORES BINÁRIAS-->
                            </div>
                        </div>
                        
                    </div>
                    <!--FIM COLUNA ESQUERDA-->

                    <!--INICIO COLUNA DIREITA-->
                    <div class="col-md-6">
                     <!--PILHAS -->
                        <div class="card">
                            <div class="card-body">               
                                <div class="d-flex flex-row comment-row m-t-0">
                                </div>
                                <h4 class="card-title m-b-0">Pilhas</h4>
                                <span class="m-b-15 d-block"><br/>
                                Uma pilha é um conjunto ordenado de itens, no qual novos itens podem ser inseridos e a partir do qual podem ser eliminados itens de uma extremidade, chamada topo da pilha. 

                                Também é chamada de lista linear, onde todas as inserções e eliminações são feitas em apenas uma das extremidades, chamada topo.
                                </span>
                                <img class="img-fluid" alt="Responsive image" src="../../assets/images/conteudos/pilha1.png" width="auto" height="222px">
                                <div class="embed-responsive embed-responsive-1by1">
                                        <iframe class="em   bed-responsive-item" src="../../assets/arquivos/conteudos/tutorialpilhas.pdf" allowfullscreen width="500" height="500"></iframe>
                                        <div style="margin-bottom:5px"> <strong> <a  title="Teste de mesa" target="_blank"></a> </strong> Fonte: <strong></strong> </div>
                                </div>
                            </div>
                            
                        </div>
                        <!--END PILHAS --> 

                        <!--FILAS -->
                        <div class="card">
                            <div class="card-body">
                                <h4 class="card-title m-b-0">Filas</h4>
                                <br>
                                <span>
                                    <b>O QUE É?</b> <br/>
                                    Estruturas de dados que se comportam
                                como filas tradicionais. A finalidade é
                                registrar a ordem de chegada de
                                componentes. (FORBELLONE;
                                EBERSPACHER, 2005)
                               

                                    <img class="img-fluid" alt="Responsive image" src="../../assets/images/conteudos/fila1.png" width="auto" height="222px">
                                </span>
                            </div>
                            
                            <div class="comment-widgets scrollable">
                                
                                <div class="d-flex flex-row comment-row m-t-0">
                                        
                                    <div class="embed-responsive embed-responsive-4by3">
                                        <iframe class="em   bed-responsive-item" src="../../assets/arquivos/conteudos/filasppt.pdf" allowfullscreen width="500" height="500"></iframe>
                                        <div style="margin-bottom:5px"> <strong> <a  title="Teste de mesa" target="_blank"></a> </strong> Fonte: <strong></strong> </div>
                                    </div>


                                </div>
                            </div>
                            
                            
                            
                        </div>
                        <!--END FILAS -->
                    </div>
                </div>
                
                <!-- ============================================================== -->
                <!-- End PAge Content -->
                <!-- ============================================================== -->
                <!-- ============================================================== -->
                <!-- Right sidebar -->
                <!-- ============================================================== -->
                <!-- .right-sidebar -->
                <!-- ============================================================== -->
                <!-- End Right sidebar -->
                <!-- ============================================================== -->
            </div>
            <!-- ============================================================== -->
            <!-- End Container fluid  -->
            <!-- ============================================================== -->
            <!-- ============================================================== -->
            <!-- footer -->
            <!-- ============================================================== -->
          
            <!-- ============================================================== -->
            <!-- End footer -->
            <!-- ============================================================== -->
        </div>  


        



<?php include('rodape.php') ?>