<?php 
include_once('conexao.php');
$nome = $_POST['nome'];
$login = $_POST['usuario'];
$senha = md5( $_POST['senha'] );
$nivel = "1";
$imagem = "";
//select para verificar se ja está cadastrado o usuário informado
$conn = getConnection();
$stm = $conn->prepare("SELECT * FROM usuarios WHERE usuario = ? ");
$stm->bindParam(1,$login);
$stm->execute();
$linhaAfetadas = $stm->rowCount();

//array de retorono
$retorno = array();    


if($linhaAfetadas > 0){ //se já possuir cadastro

    $retorno["sucesso"] = false;
    $retorno["mensagem"] = "Usuário já cadastrado!!";

}


else{ //se não estiver cadastrado
    // insert
    $stm2 = $conn->prepare("INSERT INTO usuarios (nome, usuario ,senha, nivel, imagem) VALUES(?,?,?,?,?)");
    $stm2->bindParam(1,$nome);
    $stm2->bindParam(2,$login);
    $stm2->bindParam(3,$senha);
    $stm2->bindParam(4,$nivel);
    $stm2->bindParam(5,$imagem);

  

    if($stm2->execute()){
    $id = $conn->lastInsertId(); //pega o id da ultima inserção realizada

        //retorno para o ajax
        $retorno["sucesso"] = true;
        $retorno["mensagem"] = "Usuário Cadastrado com Sucesso!";
        $retorno["id"] = $id;

    }
    else{//se falha na inserção
        $retorno["sucesso"] = false;
        $retorno["mensagem"] = "Falha ao cadastrar usuário!";
    }    

}//fim else




echo json_encode($retorno);

?>