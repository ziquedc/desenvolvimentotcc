<!DOCTYPE html>
<html dir="ltr">

<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <!-- Tell the browser to be responsive to screen width -->
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="description" content="">
    <meta name="author" content="">
    <!-- Favicon icon -->
    <link rel="icon" type="image/png" sizes="16x16" href="../../assets/images/icon.png">
    <title>G-Dev</title>
    <!-- Custom CSS -->
    <link href="../../dist/css/style.min.css" rel="stylesheet">
    
</head>

<body>
    <div class="main-wrapper">
        <!-- ============================================================== -->
        <!-- Preloader - style you can find in spinners.css -->
        <!-- ============================================================== -->
        <div class="preloader">
            <div class="lds-ripple">
                <div class="lds-pos"></div>
                <div class="lds-pos"></div>
            </div>
        </div>
        <!-- ============================================================== -->
        <!-- Preloader - style you can find in spinners.css -->
        <!-- ============================================================== -->
        <!-- ============================================================== -->
        <!-- Login box.scss -->
        <!-- ============================================================== -->
        <div class="auth-wrapper d-flex no-block justify-content-center align-items-center bg-dark">
            <div class="auth-box bg-dark border-top border-secondary">
                <div id="loginform">
                    <div class="text-center p-t-20 p-b-20">
                        <!--LOGO-->
                        <span class="db"><img src="../../assets/images/logo-completo.png" alt="logo" /></span>
                    </div>
                    <div class="text-center" style="margin-bottom: 28px;">
                        <p class="h3" style="color: #c6c07d; font-size: 23px; margin-bottom: 26px"><b>Guia do Desenvolvedor</b></p>
                        <span class="text-white">Objeto de Aprendizagem voltado ao ensino de programação integrando Técnicas de Navegação Adaptativa, baseadas no Estilo Cognitivo de cada usuário.</span>
                    </div>

                    <!--BOTÕES-->
                    <button id="btnAvancar" class="btn btn-info" type="button" style="margin-left: 22%"><i class="ti-arrow-circle-right m-r-5"></i>Avançar para Cadastro</button>
                    
                    <div class="text-center" style="margin-top: 100px">
                        <p class="h5"><b>Créditos:</b> Luiz Henrique Campos</p>
                    </div>
                </div>

                
            </div>
        </div>
        

    </div>

    <!-- ============================================================== -->
    <!-- All Required js -->
    <!-- ============================================================== -->
    <script src="../../assets/libs/jquery/dist/jquery.min.js"></script>
    <!-- Bootstrap tether Core JavaScript -->
    <script src="../../assets/libs/popper.js/dist/umd/popper.min.js"></script>
    <script src="../../assets/libs/bootstrap/dist/js/bootstrap.min.js"></script>
    <!-- ============================================================== -->
    <!-- This page plugin js -->
    <!-- ============================================================== -->

    <script>
        $('[data-toggle="tooltip"]').tooltip();
        $(".preloader").fadeOut();

        //BOTÃO AVANÇAR PARA CADASTRO E LOGIN
        $('#btnAvancar').on("click", function(){
            window.location.href="login.php";
        });
    </script>

    <!-- Bootstrap tether Core JavaScript -->
    <script src="../../assets/libs/popper.js/dist/umd/popper.min.js"></script>
    <script src="../../assets/libs/bootstrap/dist/js/bootstrap.min.js"></script>
</body>
</html>