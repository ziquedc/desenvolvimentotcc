<?php  
if(!isset($_SESSION)){ //Verificar se a sessão não já está aberta.
    session_start();
}
//se a sessão nao for criada no login, será redirecionado de volto para o form de login
if(!isset ($_SESSION['usuario']) == true){
    unset($_SESSION['usuario']); //destroi a sessao do usuário
    unset($_SESSION['perfil']); //destroi a sessao do usuário
    unset($_SESSION['nivel']); //destroi a sessao do usuário
    unset($_SESSION['login']); //destroi a sessao do usuário
    unset($_SESSION['admin']); //destroi a sessao do usuário
    header('location:index.php');
} 
//se identificar uma sessão abre a página
//SELECT PARA OBTER OS DADOS SO USUÁRIO
$login = $_SESSION['login'];
include_once('conexao.php');
$conn = getConnection();
$stm = $conn->prepare("SELECT * FROM usuarios WHERE usuario = ? ");
$stm->bindParam(1,$login);
$stm->execute();
$dados = $stm->fetch(PDO::FETCH_ASSOC);
$imagem = $dados['imagem'];
$id_user = $dados['id'];
//verifica se a imagem no banco de dados esta vazia, se estiver vazia seta uma imagem padrão
if(empty($imagem) ){ //se nao tem imagem no bd
    $imagem = "../../assets/images/users/1.jpg";
}

?>

 <?php include('cabecalho.php'); ?> 

    <div class="page-wrapper">
    <iframe width="100%" height="700" src="https://app.powerbi.com/view?r=eyJrIjoiZGExY2U5MGItODg1Yy00NDE3LTlmMmItYTBhZjczMWNjODc1IiwidCI6ImE1MTgwMjk1LWIyZmItNGJmNi1hMjU5LWNjZmY2YjAxYjYyZCJ9" frameborder="0" allowFullScreen="true"></iframe>
    </div>
    
    


 <?php include('rodape.php'); ?>   