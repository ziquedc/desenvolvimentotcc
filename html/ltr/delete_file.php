<?php
include_once('conexao.php');

$id = $_POST['id'];

$conn = getConnection();
$stm = $conn->prepare('SELECT * FROM arquivos WHERE id_arquivo = ?');
$stm->bindParam(1, $id);
$stm->execute();
$dados = $stm->fetch(PDO::FETCH_ASSOC);
$arquivo = $dados['diretorio_arquivo'];

$stm2 = $conn->prepare('DELETE FROM arquivos WHERE id_arquivo = ?');
$stm2->bindParam(1,$id);
unlink($arquivo);
//array de retorno
$retorno = array();

if($stm2->execute()){
	$retorno['sucesso'] = true;
    $retorno['mensagem'] = "Arquivo excluído com sucesso";
}else{
	$retorno['sucesso'] = false;
    $retorno['mensagem'] = "Falha ao excluir";
}

echo json_encode($retorno);




?>