<?php
//VALIDAÇÃO DA SESSION ABERTA OU NAO
if(!isset($_SESSION)){ //Verificar se a sessão não já está aberta.
    session_start();
}
//se a sessão nao for criada no login, será redirecionado de volto para o form de login
if(!isset ($_SESSION['usuario']) == true){
    unset($_SESSION['usuario']);
    header('location:index.php');
}

##########################################################################################

include_once('conexao.php');
$nome = $_POST['nome'];
$login = $_POST['usuario'];
$perfil = $_POST['perfil'];
$nivel = $_POST['nivel'];
$admin = $_POST['admin'];
$id = $_POST['id'];

//update na tabela salvando o usuário
$conn = getConnection();
$stm = $conn->prepare("UPDATE usuarios SET nome = ?, usuario = ?, perfil = ?, nivel = ?, admin= ? WHERE id = ? ");
$stm->bindParam(1,$nome);
$stm->bindParam(2,$login);
$stm->bindParam(3,$perfil);
$stm->bindParam(4,$nivel);
$stm->bindParam(5,$admin);
$stm->bindParam(6,$id);


if($stm->execute()){
    //echo "<script>alert('Cadastrado com sucesso');</script>"; 
    //echo "<script>window.location = 'cpanel_banner.php';</script>";
    $retorno['sucesso'] = true;
    $retorno['mensagem'] = "Usuário alterado com sucesso";
}else{
    $retorno['sucesso'] = false;
    $retorno['mensagem'] = "Erro ao alterar dados";
}

echo json_encode($retorno);

?>