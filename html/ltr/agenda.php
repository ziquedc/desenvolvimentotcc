<?php include('cabecalho.php') ?>

<?php include('conexao.php') ?>

<?php 
    $conn = getConnection();
    $stm = $conn->prepare("SELECT * FROM agenda_teste");
    $stm->execute();
    $i=0;


?>
   
        <div class="page-wrapper">
        
            <!-- ============================================================== -->
            <!-- Bread crumb and right sidebar toggle -->
            <!-- ============================================================== -->
            <div class="page-breadcrumb">
                <div class="row">
                    <div class="col-12 d-flex no-block align-items-center">
                        <h4 class="page-title">Agendas</h4>
                        <div class="ml-auto text-right">
                            <nav aria-label="breadcrumb">
                                <ol class="breadcrumb">
                                    <li class="breadcrumb-item"><a href="#">Início</a></li>
                                    <li class="breadcrumb-item active" aria-current="page">Agendas</li>
                                </ol>
                            </nav>
                        </div>
                    </div>
                </div>
            </div>
            <!-- ============================================================== -->
            <!-- End Bread crumb and right sidebar toggle -->
            <!-- ============================================================== -->
            <!-- ============================================================== -->
            <!-- Container fluid  -->
            <!-- ============================================================== -->
            <div class="container-fluid">
                <!-- ============================================================== -->
                <!-- Start Page Content -->
                <!-- ============================================================== -->
                <div class="row">
                    <div class="col-md-12">
                        <div class="card">
                            <div class="">
                                <div class="row">                  
                                    <div class="col-lg-12 col-sm-12 col-md-12 col-xs-12">
                                        <div class="card-body b-l calender-sidebar">
                                            <div id="calendario"></div>
                                            
                                        </div>
                                    </div>

                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>

            <!--MODAL QUE RECEBE O EVENTO LISTANDO OS DADOS E PODENDO REALIZAR A ALTERAÇÃO-->
            <div class="modal fade" id="modalListar" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
                <div class="modal-dialog" role="document">
                    <div class="modal-content">
                    <div class="modal-header">
                        <h5 class="modal-title" id="titleEvent"></h5>
                        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                        </button>
                    </div>
                    <div class="modal-body">
                        <div id="descriptionEvent"></div>
                        <div id="inicio"></div>
                        <div id="fim"></div>
                    </div>
                    <div class="modal-footer">
                        <button type="button" class="btn btn-success">Alterar</button>
                        <a href="excluir_evento.php?cod="> <button type="button" class="btn btn-danger" >Apagar</button></a>
                        <button type="button" class="btn btn-default" data-dismiss="modal">Cancelar</button>
                    </div>
                    </div>
                </div>
            </div>

             <!--MODAL PARA ADICIONAR UM NOVO EVENTO-->
             <div class="modal fade" id="modalAdicionar" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
                <div class="modal-dialog" role="document">
                    <div class="modal-content">
                    <div class="modal-header">
                        <h5 class="modal-title" id="titleEvent">Adicionar Evento</h5>
                        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                        </button>
                    </div>
                    <div class="modal-body">
                        <form action="salvar_evento.php" method="POST">
                        <div class="form-group">
                                <label for="exampleInputPassword1">Data</label>
                                <input type="text" class="form-control" id="data_add"  name="data" disabled required>
                            </div>
                            <div class="form-group">
                                <label for="exampleInputEmail1">Título</label>
                                <input type="text" class="form-control" name="titulo" required>
                            </div>
                            <div class="form-group">
                                <label for="exampleInputPassword1">Descricao</label>
                                <textarea class="form-control" rows="3" id="descricao"  name="descricao" required></textarea>
                            </div>
                            <div class="form-group">
                                <label for="exampleInputPassword1">Início</label>
                                <input type="time" class="form-control" id="data"  name="horaInicio" required>
                            </div>
                            <div class="form-group">
                                <label for="exampleInputPassword1">Até</label>
                                <input type="time" class="form-control" id="data"  name="horaFim" required >
                            </div>
                        
                            </div>
                            <div class="modal-footer">
                                <button type="submit" class="btn btn-success" >Salvar</button>
                                <button type="button" class="btn btn-default" data-dismiss="modal">Cancelar</button>
                            </div>
                    </form>
                    </div>
                </div>
            </div>


          <script>
                    //função que chama e mostra o fullcalendar
                    $('#calendario').fullCalendar({ 

                        themeSystem: 'bootstrap4', //tema
                        header:{ //titulos e botoes do cabeçalho do calendario
                            left: 'today,prev,next',
                            center: 'title',
                            right: 'month,basicWeek,agendaDay'
                        },
                        
                        //eventos listados do bando de dados
                        events:[
                            <?php while($linha = $stm->fetch(PDO::FETCH_ASSOC)){ ?>
                                {
                                    id: '<?php echo $linha['id'] ?>',
                                    title: '<?php echo $linha['title'] ?>',
                                    description: '<?php echo $linha['descriptionn'] ?>',
                                    start: '<?php echo $linha['inicio']?>',
                                    end: '<?php echo $linha['fim']?>',
                                    color: '<?php echo $linha['color']?>',
                                },
                            <?php $i++; }?>
                        ],
                        
                        //torna o dia clicável
                        selectable: true, 

                         //evento de clique em um dia (por dia)
                         dayClick:function(date,jsEvent,view){ 
                           // alert('Valor selecionado: ' + date.format());
                            //alert('Visão atual: ' + view.name);
                            //$(this).css('background-color', 'red');

                            $('#data_add').val(date.format());
                            $('#modalAdicionar').modal('show'); //chama e mostra o modal abaixo
                        },
                                
                        //clique em eventos  
                        eventClick:function(calEvent,jsEvent,view){ //função de clique em eventos
                            $('#titleEvent').html(calEvent.title);
                            $('#descriptionEvent').html('Procedimentos: '+ calEvent.description.bold());
                            $('#inicio').html('Início: '+calEvent.start.format('HH:mm').bold());
                            $('#fim').html('Até: '+calEvent.end.format('HH:mm').bold());

                            $('#modalListar').modal();
                        }

                    });//fim full calendar      
          </script>
            



















           
        </div>
        <!-- ============================================================== -->
        <!-- End Page wrapper  -->
        <!-- ============================================================== -->
    </div>


     


<?php include('rodape.php') ?>