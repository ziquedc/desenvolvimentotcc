<?php
if(!isset($_SESSION)){ //Verificar se a sessão não já está aberta.
    session_start();
}
//se a sessão nao for criada no login, será redirecionado de volto para o form de login
if(!isset ($_SESSION['usuario']) == true){
    unset($_SESSION['usuario']); //destroi a sessao do usuário
    unset($_SESSION['perfil']); //destroi a sessao do usuário
    unset($_SESSION['nivel']); //destroi a sessao do usuário
    unset($_SESSION['login']); //destroi a sessao do usuário
    unset($_SESSION['admin']); //destroi a sessao do usuário
    header('location:index.php');
} 
$id = $_POST['id'];
//PEGAR OS DADOS VINDOS DO QUESTIONÁRIO
$questao1 = $_POST['1']; //option2
$questao2 = $_POST['2']; //option4
$questao3 = $_POST['3']; //option2
$questao4 = $_POST['4']; //option1
$questao5 = $_POST['5']; //option4
$questao6 = $_POST['6']; //option1
$questao7 = $_POST['7']; //option3
$questao8 = $_POST['8']; //option2
$questao9 = $_POST['9']; //option1
$questao10 = $_POST['10']; //option2

//VERIFICAR ACERTOS
$acertos = 0;

if($questao1 == 'option2'){
    $acertos = $acertos + 1;
}

if($questao2 == 'option4'){$acertos = $acertos + 1;}
if($questao3 == 'option2'){$acertos = $acertos + 1;}
if($questao4 == 'option1'){$acertos = $acertos + 1;}
if($questao5 == 'option4'){$acertos = $acertos + 1;}
if($questao6 == 'option1'){$acertos = $acertos + 1;}
if($questao7 == 'option3'){$acertos = $acertos + 1;}
if($questao8 == 'option2'){$acertos = $acertos + 1;}
if($questao9 == 'option1'){$acertos = $acertos + 1;}
if($questao10 == 'option2'){$acertos = $acertos + 1;}

//array de retorno
$retorno = array();
//verifica o numero de acertos

//update na tabela de acertos com os acertos
include("conexao.php");

//conexao e insert
$conn = getConnection();
$stm = $conn->prepare(
    "INSERT INTO 
    acertos (id_usuario, nivel_1) 
    VALUES (?, ?) "
    );
$stm->bindParam(1,$id);
$stm->bindParam(2,$acertos);


if($acertos >= 7){
   if( $stm->execute()){
        $retorno['aprovado'] = true;
        $retorno['mensagem'] = 'Parabéns!! Você foi aprovado para o próximo nível. Total de acertos: '.$acertos;
        //update no nível atual do usuário
        $proximoNivel = 2;
        $update = $conn->prepare("UPDATE usuarios set nivel = ? WHERE id = ? ");
        $update->bindParam(1,$proximoNivel);
        $update->bindParam(2,$id);
        $update->execute();
        //atualiza o nivel na sessão atual
        $_SESSION['nivel'] = $proximoNivel;
   }
   else{
    $retorno['aprovado'] = true;
    $retorno['mensagem'] = 'ERRO AO REALIZAR INSERT. ID usuario: '.$id;
   }
    
}
else{
    $retorno['aprovado'] = false;
    $retorno['mensagem'] = 'Reprovado!! Você não atingiu o mínimo de acertos para avançar de nível. Total de acertos: '.$acertos;
}
    


echo json_encode($retorno);


?>