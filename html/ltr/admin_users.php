<?php
if(!isset($_SESSION)){ //Verificar se a sessão não já está aberta.
    session_start();
  }
  //se a sessão nao for criada no login, será redirecionado de volto para o form de login
  if(!isset ($_SESSION['usuario']) == true){
    unset($_SESSION['usuario']);
    header('location:index.php');
  } 
  //se identificar uma sessão abre a página
 ?>
<?php include('cabecalho.php') ?>


        <div class="page-wrapper">
            <!-- ============================================================== -->
            <!-- Bread crumb and right sidebar toggle -->
            <!-- ============================================================== -->
            <div class="page-breadcrumb">
                <div class="row">
                    <div class="col-12 d-flex no-block align-items-center">
                        <h4 class="page-title">Gerenciar Usuários</h4>
                        <div class="ml-auto text-right">
                            <nav aria-label="breadcrumb">
                                
                            </nav>
                        </div>
                    </div>
                </div>
            </div>
            <!-- ============================================================== -->
            <!-- End Bread crumb and right sidebar toggle -->
            <!-- ============================================================== -->
            <!-- ============================================================== -->
            <!-- Container fluid  -->
            <!-- ============================================================== -->
            <div class="container-fluid">
                <!-- ============================================================== -->
                <!-- Start Page Content -->
                <!-- ============================================================== -->
                <div class="row">
                    <div class="col-12">

                        <div class="card">
                            <div class="card-body">
                                <div class="table-responsive">
                                    <table id="#table_user" class="table table-striped table-bordered">
                                        <thead>
                                            <tr>
                                                <th><b>FOTO </b></th>
                                                <th><b>NOME </b></th>
                                                <th><b>USUÁRIO </b></th>
                                                <th><b>PERFIL </b></th>
                                                <th><b>NÍVEL </b></th>
                                                <th><b>ADMIN </b></th>
                                                <th><b>AÇÕES </b></th>
                                            </tr>
                                        </thead>
                                        <tbody id="tabela">
                                            

                                        </tbody>
                                       
                                    </table>
                                </div>

                            </div>
                        </div>
                    </div>
                </div>
            <!--MODAL EDITAR USUÁRIO-->
                <div class="modal fade none-border" id="modalUser" data-backdrop="static" data-keyboard="false">
                    <div class="modal-dialog">
                        <div class="modal-content">
                            <div class="modal-header bg-warning" style="color: #ffffff">
                                <h4 class="modal-title"><strong><i class="ti-pencil-alt"></i> Edição Usuário</strong></h4>
                                <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                            </div>
                            <div class="modal-body">
                                <form id="formModal" enctype="multipart/form-data">
                                    <div class="row">
                                        <div class="col-md-12">
                                            <label class="control-label"><b>Nome</b></label>
                                            <input class="form-control form-white"  type="text" name="nome" id="nome" required />
                                        </div>
                                    </div>
                                    <div class="row">
                                        <div class="col-md-6">
                                            <label class="control-label mt-3"><b>Usuário</b></label>
                                            <input class="form-control form-white"  type="text" name="usuario" id="usuario" required />
                                        </div>
                                        <div class="col-md-6">
                                            <label class="control-label mt-3"><b>Perfil</b></label>
                                            
                                            <select class="custom-select"  id='perfil' name='perfil'>
                                                <option value="convergente">Convergente</option>
                                                <option value="divergente">Divergente</option>
                                                <option value="holista">Holista</option>
                                                <option value="impulsivo">Impulsivo</option>
                                                <option value="reflexivo">Reflexivo</option>
                                                <option value="serialista">Serialista</option>
                                                <option value="indefinido">Indefinido</option>
                                            </select>
                                        </div>
                                    </div>
                                    <div class="row">
                                        <div class="col-md-6">
                                            <label class="control-label mt-3"><b>Nível</b></label>
                                            <select class="custom-select"  id='nivel' name='nivel'>
                                                <option value="1">1</option>
                                                <option value="2">2</option>
                                                <option value="3">3</option>
                                            </select>
                                            <input class="form-control form-white"  type="text" name="id" id="id" hidden />
                                        </div>
                                        <div class="col-md-6">
                                            <label class="control-label mt-3"><b>Administrador</b></label>
                                            <select class="custom-select" id='admin' name='admin'>
                                                <option value="1">Sim</option>
                                                <option value="0">Não</option>
                                            </select>
                                        </div>
                                    </div>
                                
                                    </div>
                                    <div class="modal-footer">
                                        <button type="submit" class="btn btn-danger waves-effect waves-light save-category" >Salvar</button>
                                        <button type="button" class="btn btn-secondary waves-effect" data-dismiss="modal">Cancelar</button>
                            </form>
                            </div>
                        </div>
                    </div>
                </div>
                <!-- END MODAL -->
                <!--MODAL ALTERAR SENHA-->
                 <!-- MODAL ALTERAÇÃO DE SENHA -->
                 <div class="modal fade" id="modalSenhaAdmin" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true" data-backdrop="static" data-keyboard="false">
                  <div class="modal-dialog" role="document"> 
                    <div class="modal-content">
                      <div class="modal-header bg-primary">
                        <h5 class="modal-title text-white" id="exampleModalLabel"><i class="fa fa-key" aria-hidden="true"></i><b>Alterar Senha</b></h5>
                        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                          <span aria-hidden="true">&times;</span>
                        </button>
                      </div>
                      <div class="modal-body">
                        <form id="formSenhaAdmin">
                            <div class="form-group">
                            <input type="text" class="form-control" name="idUser" id="senhaAtual" hidden>
                          </div>
                          <div class="row">
                            <div class="col">
                                <label for="text" class="col-form-label"><b>Nova Senha:</b></label>
                              <input type="password" class="form-control" name="senha1Admin" id="senha1Admin" required>
                            </div>
                            <div class="col">
                                <label for="text" class="col-form-label"><b>Confirmar Senha:</b></label>
                              <input type="password" class="form-control" id="senha2Admin" required>
                            </div>
                          </div><br>
                          <div id='msgRetornoSenha' class="alert alert-danger" role="alert" style='display:none '></div>
                      </div>
                      
                      <div class="modal-footer">
                        <p id="retornoSenha" style="margin-right: 130px; display: none"><b></b></p>
                        <button type="submit" class="btn btn-success">Alterar</button>
                        <button type="button" class="btn btn-secondary" data-dismiss="modal">Cancelar</button>
                        </form>
                      </div>
                    </div>
                  </div>
                </div>

                <!-- ============================================================== -->
                <!-- End PAge Content -->
                <!-- ============================================================== -->
                <!-- ============================================================== -->
                <!-- Right sidebar -->
                <!-- ============================================================== -->
                <!-- .right-sidebar -->
                <!-- ============================================================== -->
                <!-- End Right sidebar -->
                <!-- ============================================================== -->
            </div>
            <!-- ============================================================== -->
            <!-- End Container fluid  -->
            <!-- ============================================================== -->

  <!-- ============================================================== -->
    <!-- All Required js -->
    <!-- ============================================================== -->
    <script src="../../assets/libs/jquery/dist/jquery.min.js"></script>
    <!-- Bootstrap tether Core JavaScript -->
    <script src="../../assets/libs/popper.js/dist/umd/popper.min.js"></script>
    <script src="../../assets/libs/bootstrap/dist/js/bootstrap.min.js"></script>
    <!-- ============================================================== -->
    <!-- This page plugin js -->
    <!-- ============================================================== -->


<script>
    $(document).ready(function(){
        popularTabela();
    });

    //função para popular a tabela 
    function popularTabela(){
        $('#tabela').empty(); //limpa a tabela
        $.ajax({
            type:'POST',
            dataType:'json',
            url:'users_json.php'

            }).done(function(data){
            for(var i =0; data.length>i; i++){
                //preenche a tabela
                $('#tabela').append("<tr><td> <img class='rounded-circle' src='../../assets/images/users/1.jpg' width='30' height'30'></td><td> "+data[i].nome+" </td><td> "+data[i].usuario+" </td><td> "+data[i].perfil+" </td><td> "+data[i].nivel+" </td><td> "+data[i].admin+"</td>   </td><td> <a id='excluir' href='' class='text-success' alt='"+data[i].id+"''><button class='btn badge-danger text-white'><i class='fa fa-trash fa-1x'></button></i></a> <a id='editar' href='' class='text-success' alt='"+data[i].id+"''><button class='btn badge-warning text-white'><i class='ti ti-pencil-alt'></button></i></a> <a id='editarSenha' href='' class='text-success' alt='"+data[i].id+"''><button class='btn badge-primary text-white'><i class='ti ti-key'></button></i></a> </td></tr>");
            }

            //evento de clique no botao de excluir
            $('#tabela tr td #excluir').click(function(e){
                e.preventDefault();
                var id = $(this).attr("alt");
                var linha = $(this).parent().parent();
                exluirRegistro(id,linha);
                toastr.success('Usuário excluído com sucesso!');
            });//fim excluir

            //evento de clique no botao de editar
            $('#tabela tr td #editar').click(function(e){
                e.preventDefault();
                var id = $(this).attr("alt");
                $.ajax({
                    type:'POST',
                    data:"id="+id,
                    url:'user_id_json.php'
                }).done(function(data){ 
                    $('#nome').val(data[0].nome);
                    $('#usuario').val(data[0].usuario);
                    $('#perfil').val(data[0].perfil);
                    $('#nivel').val(data[0].nivel);
                    $('#id').val(data[0].id);
                    $('#admin').val(data[0].admin);
                    $('#modalUser').modal();
                    //submit modal salvar
                    $('#formModal').submit(function(e){
                        e.preventDefault();
                        var form = $(this).serialize();
                        salvarEdicaoUsuario(form);
                    })
                }).fail(function(){ 
                    alert('Erro interno. Contate o Administrador!');
                })//fim ajax

                //evento de clique no botao alterar senha
                $(' #editarSenha').click(function(e){
                    e.preventDefault();
                    var id = $(this).attr("alt");
                    $('#senhaAtual').val(id);
                    $('#modalSenhaAdmin').modal();
                    alterarSenha();
                });//fim excluir




            });//fim editar



        }).fail(function(){
        alert('erro interno. Contate o Administrador.')
        });
    }//fim função



    // ###########################################################
    //FUNÇÕES 

    //FUNÇÃO DE EXCLUIR REGISTRO
    function exluirRegistro(cod, elemento){
        $.ajax({
            type:'POST',
            data:"id="+cod,
            url:'delete_user.php',
            asyn: true
            }).done(function(data){
               /* var sucesso = $.parseJSON(data)['sucesso'];
                var mensagem = $.parseJSON(data)['mensagem'];

                if(sucesso){
                    elemento.fadeOut();
                }else{
                    alert(mensagem);
                } */
                elemento.fadeOut();

        }).fail(function(){
        alert('Erro interno. Contate o administrador');
        })
    }//fim função

    //FUNÇÃO SALVAR4 EDIÇÃO USUÁRIO
    function salvarEdicaoUsuario(dados){
        $.ajax({
            type:'POST',
            data:dados,
            url:'save_user_edition_admin.php'
        }).done(function(data){
            popularTabela();
            $('#modalUser').modal('hide');
            toastr.success('Alteração Realizada com sucesso');
            

        }).fail(function(){
            alert('Erro interno. Contate o Administrador');
        })
    }; //fim função

    //FUNÇÃO ALTERAR SENHA formSenhaAdmin
    function alterarSenha(){
        $('#formSenhaAdmin').submit(function(e){
            e.preventDefault();
            var senha1 = $('#senha1Admin').val();
            var senha2 = $('#senha2Admin').val();
            var formDados = $(this).serialize();
            if(senha1 == senha2){//senhas iguais
                $.ajax({
                    type:'POST',
                    data:formDados,
                    url:'alterar_senha_admin.php'
                 }).done(function(data){
                    console.log(data);
                    $('#modalSenhaAdmin').modal('hide');
                    toastr.success('Alteração Realizada com sucesso');
                }).fail(function(){
                    alert('Erro interno. Contate o Administrador!');
                })//fim ajax

            }else{//senhas diferem
                $('#msgRetornoSenha').css('display','inline');
                $('#msgRetornoSenha').addClass('alert-danger');
                $('#msgRetornoSenha').html('As Senhas diferem!');
            }
        })


        
    }//fim função

</script>











        

        <footer class="footer text-center">
            Projetado e Desenvolvido por: <b>Luiz Henrique Campos</b>. Todos os direitos reservados.
    </footer>
        <!-- ============================================================== -->
        <!-- End footer -->
        <!-- ============================================================== -->
    </div> 
  
   <!-- Bootstrap tether Core JavaScript -->
   <script src="../../assets/libs/popper.js/dist/umd/popper.min.js"></script>
    <script src="../../assets/libs/bootstrap/dist/js/bootstrap.min.js"></script>
    <!-- slimscrollbar scrollbar JavaScript -->
    <script src="../../assets/libs/perfect-scrollbar/dist/perfect-scrollbar.jquery.min.js"></script>
    <script src="../../assets/extra-libs/sparkline/sparkline.js"></script>
    <!--Wave Effects -->
    <script src="../../dist/js/waves.js"></script>
    <!--Menu sidebar -->
    <script src="../../dist/js/sidebarmenu.js"></script>
    <!--Custom JavaScript -->
    <script src="../../dist/js/custom.min.js"></script>
    <!-- this page js -->
    <script src="../../assets/libs/toastr/build/toastr.min.js"></script>
   
    
</body>
</html>