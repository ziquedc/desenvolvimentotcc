<?php
if(!isset($_SESSION)){ //Verificar se a sessão não já está aberta.
    session_start();
  }
  //se a sessão nao for criada no login, será redirecionado de volto para o form de login
  if(!isset ($_SESSION['usuario']) == true){
    unset($_SESSION['usuario']);
    header('location:index.php');
  } 
  //se identificar uma sessão abre a página
 ?>
<?php include('cabecalho.php') ?>


        <div class="page-wrapper">
            <!-- ============================================================== -->
            <!-- Bread crumb and right sidebar toggle -->
            <!-- ============================================================== -->
            <div class="page-breadcrumb">
                <div class="row">
                    <div class="col-12 d-flex no-block align-items-center">
                        <h4 class="page-title">Upload de Arquivos</h4>
                        <div class="ml-auto text-right">
                            <nav aria-label="breadcrumb">
                                
                            </nav>
                        </div>
                    </div>
                </div>
            </div>
            <!-- ============================================================== -->
            <!-- End Bread crumb and right sidebar toggle -->
            <!-- ============================================================== -->
            <!-- ============================================================== -->
            <!-- Container fluid  -->
            <!-- ============================================================== -->
            <div class="container-fluid">
                <!-- ============================================================== -->
                <!-- Start Page Content -->
                <!-- ============================================================== -->
                <div class="row">
                    <div class="col-12">

                        <div class="card">
                            <div class="card-body">
                                <div class="table-responsive">
                                <button id='btnAddFile' type="button" class="btn btn-success my-1"><i class="fa fa-plus mr-2" aria-hidden="true"></i>Adicionar Arquivo</button>
                                    <table id="#table_files" class="table table-striped table-bordered">
                                        <thead>
                                            <tr>
                                                <th><b>NOME ARQUIVO </b></th>
                                                <th><b>DIRETORIO </b></th>
                                                <th><b>AÇÕES </b></th>
                                            </tr>
                                        </thead>
                                        <tbody id="tabela_files">
                                            

                                        </tbody>
                                       
                                    </table>
                                </div>

                            </div>
                        </div>
                    </div>
                </div>
            <!--MODAL ADICIONAR ARQUIVO-->
                <div class="modal fade none-border" id="modalAddFile" data-backdrop="static" data-keyboard="false">
                    <div class="modal-dialog">
                        <div class="modal-content">
                            <div class="modal-header bg-success" style="color: #ffffff">
                                <h4 class="modal-title"><strong><i class="fa fa-upload" aria-hidden="true"></i> Upload de Arquivo</strong></h4>
                                <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                            </div>
                            <div class="modal-body">
                                <form id="formAddFile" enctype="multipart/form-data">
                                    <div class="row">
                                        <div class="col-md-12">
                                            <label class="control-label"><b>Arquivo</b></label> <small>(somente formato PDF. Máximo 2MB)</small>
                                            <input class="form-control form-white"  type="file" name="arquivo" id="arquivo" required />
                                        </div>
                                    </div>
                                    <div class="row">
                                        <div class="col-md-12 mt-3">
                                            <label class="control-label"><b>Nome do arquivo</b></label> <small>(não use espaços)</small>
                                            <input class="form-control form-white"  type="text" name="nome" id="nome" required />
                                        </div>
                                        
                                    </div>
                                    </div>
                                    
                                    <div class="modal-footer">
                                    <div><img id='loading' src='../../assets/images/loading2.gif' width="33px" style="display:none"></div>
                                        <button type="submit" class="btn btn-danger waves-effect waves-light save-category" >Salvar</button>
                                        <button type="button" class="btn btn-secondary waves-effect" data-dismiss="modal">Cancelar</button>
                            </form>
                            </div>
                        </div>
                    </div>
                </div>
                <!-- END MODAL -->

                <!--MODAL ALTERAR SENHA-->
                 <div class="modal fade" id="modalSenhaAdmin" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true" data-backdrop="static" data-keyboard="false">
                  <div class="modal-dialog" role="document"> 
                    <div class="modal-content">
                      <div class="modal-header bg-primary">
                        <h5 class="modal-title text-white" id="exampleModalLabel"><i class="fa fa-key" aria-hidden="true"></i><b>Alterar Senha</b></h5>
                        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                          <span aria-hidden="true">&times;</span>
                        </button>
                      </div>
                      <div class="modal-body">
                        <form id="formSenhaAdmin">
                            <div class="form-group">
                            <input type="text" class="form-control" name="idUser" id="senhaAtual" hidden>
                          </div>
                          <div class="row">
                            <div class="col">
                                <label for="text" class="col-form-label"><b>Nova Senha:</b></label>
                              <input type="password" class="form-control" name="senha1Admin" id="senha1Admin" required>
                            </div>
                            <div class="col">
                                <label for="text" class="col-form-label"><b>Confirmar Senha:</b></label>
                              <input type="password" class="form-control" id="senha2Admin" required>
                            </div>
                          </div><br>
                          <div id='msgRetornoSenha' class="alert alert-danger" role="alert" style='display:none '></div>
                      </div>
                      
                      <div class="modal-footer">
                        <p id="retornoSenha" style="margin-right: 130px; display: none"><b></b></p>
                        <button type="submit" class="btn btn-success">Alterar</button>
                        <button type="button" class="btn btn-secondary" data-dismiss="modal">Cancelar</button>
                        </form>
                      </div>
                    </div>
                  </div>
                </div>

                <!-- ============================================================== -->
                <!-- End PAge Content -->
                <!-- ============================================================== -->
                <!-- ============================================================== -->
                <!-- Right sidebar -->
                <!-- ============================================================== -->
                <!-- .right-sidebar -->
                <!-- ============================================================== -->
                <!-- End Right sidebar -->
                <!-- ============================================================== -->
            </div>
            <!-- ============================================================== -->
            <!-- End Container fluid  -->
            <!-- ============================================================== -->

  <!-- ============================================================== -->
    <!-- All Required js -->
    <!-- ============================================================== -->
    <script src="../../assets/libs/jquery/dist/jquery.min.js"></script>
    <!-- Bootstrap tether Core JavaScript -->
    <script src="../../assets/libs/popper.js/dist/umd/popper.min.js"></script>
    <script src="../../assets/libs/bootstrap/dist/js/bootstrap.min.js"></script>
    <!-- ============================================================== -->
    <!-- This page plugin js -->
    <!-- ============================================================== -->


<script>
    //ABRIR MODAL ADD ARQUIVO
    $('#btnAddFile').on('click', function(){
        $('#modalAddFile').modal();
    });
    //SUBMIT FORMULARIO MODAL ADD ARQUIVO
    $('#formAddFile').submit(function(e){
        e.preventDefault();
        $('#loading').css('display','block');
        var form = $(this)[0]; 
        var formData = new FormData(form);
        //salva o arquivo via metodo ajax
        $.ajax({
            type:'POST',
            data:formData,
            url:'save_file.php',
            processData: false,  
            contentType: false
        }).done(function(data){
            var sucesso = $.parseJSON(data)['sucesso'];
            var mensagem = $.parseJSON(data)['mensagem'];
            if(sucesso){
                $('#modalAddFile').modal('toggle');
                toastr.success(mensagem);
                popularTabela();
            }else{
                $('#modalAddFile').modal('toggle');
                toastr.error(mensagem ,'Falha!');
            }
        }).fail(function(){
            alert('Erro interno. Contate o Administrador.');
        });
    })//fim submit


    $(document).ready(function(){
        popularTabela();
    });

    //função para popular a tabela 
    function popularTabela(){
        $('#tabela_files').empty(); //limpa a tabela
        $.ajax({
            type:'POST',
            dataType:'json',
            url:'files_json.php'

            }).done(function(data){
            for(var i =0; data.length>i; i++){
                //preenche a tabela
                $('#tabela_files').append("<tr><td> "+data[i].nome_arquivo+" </td><td> "+data[i].diretorio_arquivo+" </td><td> <a id='excluir' href='' class='text-success' alt='"+data[i].id_arquivo+"''><button class='btn badge-danger text-white'><i class='fa fa-trash fa-1x'></button></i></a> </td></tr>");
            }

            //evento de clique no botao de excluir
            $('#tabela_files tr td #excluir').click(function(e){
                e.preventDefault();
                var id = $(this).attr("alt");
                var linha = $(this).parent().parent();
                exluirRegistro(id,linha);
                toastr.success('Usuário excluído com sucesso!');
            });//fim excluir

           



        }).fail(function(){
        alert('erro interno. Contate o Administrador.')
        });
    }//fim função



    // ###########################################################
    //FUNÇÕES 

    //FUNÇÃO DE EXCLUIR REGISTRO
    function exluirRegistro(cod, elemento){
        $.ajax({
            type:'POST',
            data:"id="+cod,
            url:'delete_file.php',
            asyn: true
            }).done(function(data){
               /* var sucesso = $.parseJSON(data)['sucesso'];
                var mensagem = $.parseJSON(data)['mensagem'];

                if(sucesso){
                    elemento.fadeOut();
                }else{
                    alert(mensagem);
                } */
                console.log(data);
                elemento.fadeOut();

        }).fail(function(){
        alert('Erro interno. Contate o administrador');
        })
    }//fim função

   


</script>











        

        <footer class="footer text-center">
            Projetado e Desenvolvido por: <b>Luiz Henrique Campos</b>. Todos os direitos reservados.
    </footer>
        <!-- ============================================================== -->
        <!-- End footer -->
        <!-- ============================================================== -->
    </div> 
  
   <!-- Bootstrap tether Core JavaScript -->
   <script src="../../assets/libs/popper.js/dist/umd/popper.min.js"></script>
    <script src="../../assets/libs/bootstrap/dist/js/bootstrap.min.js"></script>
    <!-- slimscrollbar scrollbar JavaScript -->
    <script src="../../assets/libs/perfect-scrollbar/dist/perfect-scrollbar.jquery.min.js"></script>
    <script src="../../assets/extra-libs/sparkline/sparkline.js"></script>
    <!--Wave Effects -->
    <script src="../../dist/js/waves.js"></script>
    <!--Menu sidebar -->
    <script src="../../dist/js/sidebarmenu.js"></script>
    <!--Custom JavaScript -->
    <script src="../../dist/js/custom.min.js"></script>
    <!-- this page js -->
    <script src="../../assets/libs/toastr/build/toastr.min.js"></script>
   
    
</body>
</html>