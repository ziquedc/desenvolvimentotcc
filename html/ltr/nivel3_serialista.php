<?php
if(!isset($_SESSION)){ //Verificar se a sessão não já está aberta.
    session_start();
  }
  //se a sessão nao for criada no login, será redirecionado de volto para o form de login
  if(!isset ($_SESSION['usuario']) == true){
    unset($_SESSION['usuario']);
    header('location:index.php');
  } 
  //se identificar uma sessão abre a página
 ?>
<?php include('cabecalho.php') ?>



        <div class="page-wrapper">
            <!-- ============================================================== -->
            <!-- Bread crumb and right sidebar toggle -->
            <!-- ============================================================== -->
            <div class="page-breadcrumb">
                <div class="row">
                    <div class="col-12 d-flex no-block align-items-center">
                        <h4 class="page-title">Linguagem C</h4>
                        <div class="ml-auto text-right">
                            <nav aria-label="breadcrumb">
                                <ol class="breadcrumb">
                                    <li class="breadcrumb-item"><a href="home.php">Início</a></li>
                                    <li class="breadcrumb-item active" aria-current="page">Nível 3</li>
                                </ol>
                            </nav>
                        </div>
                    </div>
                </div>
            </div>
            <!-- ============================================================== -->
            <!-- End Bread crumb and right sidebar toggle -->
            <!-- ============================================================== -->
            <!-- ============================================================== -->
            <!-- Container fluid  -->
            <!-- ============================================================== -->
            <div class="container-fluid">
                <!-- ============================================================== -->
                <!-- Start Page Content -->
                <!-- ============================================================== -->
                <div class="row">
                    <div class="col-md-6">
                        <div class="card">
                            <div class="card-body">
                                <h4 class="card-title m-b-0">Compressão de Dados</h4>
                            </div>
                            <div class="comment-widgets scrollable">
                                <!-- ARVORES -->
                                <div class="d-flex flex-row comment-row m-t-0">
                                   
                                    <div class="comment-text w-100">
                                        <h5 class="font-medium"><u><b>O Que é ?</b></u></h5>  
                                        <span class="m-b-15 d-block">
                                            A compressão de dados ou, em inglês, <i> data
                                            compression </i>, consiste na utilização de um
                                            conjunto de métodos com o intuito da
                                            redução do espaço armazenado em unidades
                                            de memória secundária ou mesmo primária
                                            de um sistema computacional
                                        </span>
                                         <h5 class="font-medium"><u><b>Aplicação</b></u></h5>  
                                        <span class="m-b-15 d-block">
                                            <ul>
                                                <li>Internet - redução do tamanho dos arquivos;</li>
                                                <li>
                                                        Diminuir a quantidade de tráfego, aumentando a velocidade
                                                    de navegação, realização de <i>downloads</i> de arquivos e visualização
                                                    de vídeos;
                                                </li>
                                                <li>
                                                        Offline - Arquivos compactados são preferíveis quando há interesse
                                                    de armazenamento de maior número de dados possível, no menos espaço
                                                    de memória secundária disponível em dispositivos de armazenamento;
                                                </li>
                                            </ul>
                                        </span><br>
                                        <img class="img-fluid" alt="Responsive image" src="../../assets/images/conteudos/compressao1.png" width="auto" height="222px">
                                        <img class="img-fluid" alt="Responsive image" src="../../assets/images/conteudos/compressao2.png" width="auto" height="222px">
                                        <img class="img-fluid" alt="Responsive image" src="../../assets/images/conteudos/compressao3.png" width="auto" height="222px">
                                        <img class="img-fluid" alt="Responsive image" src="../../assets/images/conteudos/compressao4.png" width="auto" height="222px">
                                        <img class="img-fluid" alt="Responsive image" src="../../assets/images/conteudos/compressao5.png" width="auto" height="222px">
                                        <img class="img-fluid" alt="Responsive image" src="../../assets/images/conteudos/compressao6.png" width="auto" height="222px">
                                        <img class="img-fluid" alt="Responsive image" src="../../assets/images/conteudos/compressao7.png" width="auto" height="222px">
                                        <img class="img-fluid" alt="Responsive image" src="../../assets/images/conteudos/compressao8.png" width="auto" height="222px">
                                        <img class="img-fluid" alt="Responsive image" src="../../assets/images/conteudos/compressao9.png" width="auto" height="222px">
                                        <img class="img-fluid" alt="Responsive image" src="../../assets/images/conteudos/compressao10.png" width="auto" height="222px">
                                        <img class="img-fluid" alt="Responsive image" src="../../assets/images/conteudos/huffman1.png" width="auto" height="222px">
                                        <img class="img-fluid" alt="Responsive image" src="../../assets/images/conteudos/huffman3.png" width="auto" height="222px">
                                        <img class="img-fluid" alt="Responsive image" src="../../assets/images/conteudos/huffman2.jpg" width="auto" height="222px">
                                       
                                    </div>
                                </div>
                                
                                
                                <!-- END ARVORES -->

                               
                               
                            </div>
                        </div>
                        
                    </div>
                    <!--FIM COLUNA ESQUERDA-->

                    <!--INICIO COLUNA DIREITA-->
                    <div class="col-md-6">
                     <!--Grafos -->
                        <div class="card">
                            <div class="card-body">               
                                <div class="d-flex flex-row comment-row m-t-0">
                                </div>
                                <h4 class="card-title m-b-0">Grafos</h4>
                                <span class="m-b-15 d-block"><br/>
                                    <h5 class="font-medium"><u><b>O Que é ?</b></u></h5>
                                    Em uma definição mais formal, Tenenbaum, Langsan e Augenstein
                                    (1995) expressam que um grafo consiste num
                                    conjunto de nós (ou vértices) e num conjunto de arcos (ou arestas).
                                    Cada arco num grafo é especificado por um par de nós .
                                </span>
                                <img class="img-fluid" alt="Responsive image" src="../../assets/images/conteudos/grafos.png" width="auto" height="222px">
                                <img class="img-fluid" alt="Responsive image" src="../../assets/images/conteudos/grafos2.png" width="auto" height="222px">

                                <span class="m-b-15 d-block">
                                <i class="fa fa-exclamation-triangle text-success" aria-hidden="true"></i>   EXEMPLOS DE UTILIZAÇÕES: <br/>
                                    <ul>
                                        <li>Mapas </li>
                                        <li>Redes (computadores, telefonia, etc) </li>
                                        <li>Conexões em Circuitos Integrados </li>
                                        <li>Diagrama de Fluxo de Dados </li>
                                        <li>Redes Neurais </li>
                                        <li>Química Orgânica (desenho de moléculas) </li>
                                    </ul> 

                                    <img class="img-fluid" alt="Responsive image" src="../../assets/images/conteudos/grafos3.png" width="auto" height="222px">
                                    <br/>
                                    <img class=" img-fluid rounded mx-auto d-block" alt="Responsive image" src="../../assets/images/conteudos/grafos4.png" width="auto" height="222px"><br/>
                                    <img class=" img-fluid rounded mx-auto d-block" alt="Responsive image" src="../../assets/images/conteudos/grafos5.png" width="auto" height="222px"><br/>
                                    
                                    <img class=" img-fluid rounded mx-auto d-block" alt="Responsive image" src="../../assets/images/conteudos/grafos6.png" width="auto" height="222px"><br/>
                                    <br/>
                                    <img class=" img-fluid rounded mx-auto d-block" alt="Responsive image" src="../../assets/images/conteudos/grafos7.png" width="auto" height="222px"><br/>
                                    <img class=" img-fluid rounded mx-auto d-block" alt="Responsive image" src="../../assets/images/conteudos/grafos8.png" width="auto" height="222px"><br/>
                                    <img class=" img-fluid rounded mx-auto d-block" alt="Responsive image" src="../../assets/images/conteudos/grafos9.png" width="auto" height="222px"><br/>
                                    <img class=" img-fluid rounded mx-auto d-block" alt="Responsive image" src="../../assets/images/conteudos/grafos10.png" width="auto" height="222px"><br/>
                                    <img class=" img-fluid rounded mx-auto d-block" alt="Responsive image" src="../../assets/images/conteudos/grafos11.png" width="auto" height="222px"><br/>
                                    <img class=" img-fluid rounded mx-auto d-block" alt="Responsive image" src="../../assets/images/conteudos/grafos12.png" width="auto" height="222px"><br/>
                                    <img class=" img-fluid rounded mx-auto d-block" alt="Responsive image" src="../../assets/images/conteudos/grafos13.png" width="auto" height="222px"><br/>
                                    <img class=" img-fluid rounded mx-auto d-block" alt="Responsive image" src="../../assets/images/conteudos/grafos14.png" width="auto" height="222px"><br/>
                                    <img class=" img-fluid rounded mx-auto d-block" alt="Responsive image" src="../../assets/images/conteudos/grafos15.png" width="auto" height="222px"><br/>
                                    
                                </span>
                            </div>
                            
                        </div>
                        <!--END GRAFOS --> 

                        
                    </div>
                </div>
                
                <!-- ============================================================== -->
                <!-- End PAge Content -->
                <!-- ============================================================== -->
                <!-- ============================================================== -->
                <!-- Right sidebar -->
                <!-- ============================================================== -->
                <!-- .right-sidebar -->
                <!-- ============================================================== -->
                <!-- End Right sidebar -->
                <!-- ============================================================== -->
            </div>
            <!-- ============================================================== -->
            <!-- End Container fluid  -->
            <!-- ============================================================== -->
            <!-- ============================================================== -->
            <!-- footer -->
            <!-- ============================================================== -->
           
            <!-- ============================================================== -->
            <!-- End footer -->
            <!-- ============================================================== -->
        </div>  


        



<?php include('rodape.php') ?>