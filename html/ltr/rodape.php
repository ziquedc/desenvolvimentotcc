    <footer class="footer text-center">
            Projetado e Desenvolvido por: <b>Luiz Henrique Campos</b>. Todos os direitos reservados.
    </footer>
        <!-- ============================================================== -->
        <!-- End footer -->
        <!-- ============================================================== -->
    </div> 
  
  <script src="../../assets/libs/jquery/dist/jquery.min.js"></script>
    <script src="../../dist/js/jquery.ui.touch-punch-improved.js"></script>
    <script src="../../dist/js/jquery-ui.min.js"></script>
    <!-- Bootstrap tether Core JavaScript -->
    <script src="../../assets/libs/popper.js/dist/umd/popper.min.js"></script>
    <script src="../../assets/libs/bootstrap/dist/js/bootstrap.min.js"></script>
    <!-- slimscrollbar scrollbar JavaScript -->
    <script src="../../assets/libs/perfect-scrollbar/dist/perfect-scrollbar.jquery.min.js"></script>
    <script src="../../assets/extra-libs/sparkline/sparkline.js"></script>
    <!--Wave Effects -->
    <script src="../../dist/js/waves.js"></script>
    <!--Menu sidebar -->
    <script src="../../dist/js/sidebarmenu.js"></script>
    <!--Custom JavaScript -->
    <script src="../../dist/js/custom.min.js"></script>
    <!-- this page js -->
   
    <script src="../../assets/extra-libs/multicheck/datatable-checkbox-init.js"></script>
    <script src="../../assets/extra-libs/multicheck/jquery.multicheck.js"></script>
    <script src="../../assets/extra-libs/DataTables/datatables.min.js"></script>
    <script src="../../assets/libs/jquery-steps/build/jquery.steps.min.js"></script>
    <script src="../../assets/libs/jquery-validation/dist/jquery.validate.min.js"></script>
   
    <script>
        /****************************************
         *       Basic Table                   *
         ****************************************/
        $('#table_user').DataTable({
            "language": {
                    "url": "//cdn.datatables.net/plug-ins/1.10.19/i18n/Portuguese-Brasil.json"
                   
                  }
        });


         // formulário nível 1
            var form = $("#form1");
            form.validate({
                errorPlacement: function errorPlacement(error, element) { element.before(error); },
                rules: {
                    confirm: {
                        equalTo: "#password"
                    }
                }
                
            });
            form.children("div").steps({
                headerTag: "h3",
                bodyTag: "section",
                transitionEffect: "slideLeft",
                onStepChanging: function(event, currentIndex, newIndex) {
                    form.validate().settings.ignore = ":disabled,:hidden";
                    return form.valid();
                },
                onFinishing: function(event, currentIndex) {
                    form.validate().settings.ignore = ":disabled";
                    return form.valid();
                },
                onFinished: function(event, currentIndex) {
                    //faz o submit do formulário via ajax
                    $.ajax({
                        type:'POST',
                        data: form.serialize(),
                        url: 'valida_prova1.php'
                    }).done(function(data){
                        //fecha o modal da prova
                        $('#modalProvas1').modal('toggle');
                        //abre o modal de carregamento e fecha após 7 segundos
                        $('#modalGif').modal();
                        setTimeout(function(){ $('#modalGif').modal('hide'); }, 15000);
                        //abre o modal de retorno dos dados
                        var mensagem = $.parseJSON(data)["mensagem"];
                        $('#msgRetorno').html(mensagem);
                        setTimeout(function(){ $('#modalRetorno').modal(); }, 15000);
                    }).fail(function(){
                        alert('Erro interno. Contate o Administrador.');
                    });
                    
                }
            });


            // formulário nível 2
            var form2 = $("#form2");
            form2.validate({
                errorPlacement: function errorPlacement(error, element) { element.before(error); },
                rules: {
                    confirm: {
                        equalTo: "#password"
                    }
                }
                
            });
            form2.children("div").steps({
                headerTag: "h3",
                bodyTag: "section",
                transitionEffect: "slideLeft",
                onStepChanging: function(event, currentIndex, newIndex) {
                    form2.validate().settings.ignore = ":disabled,:hidden";
                    return form2.valid();
                },
                onFinishing: function(event, currentIndex) {
                    form2.validate().settings.ignore = ":disabled";
                    return form2.valid();
                },
                onFinished: function(event, currentIndex) {
                    //faz o submit do formulário via ajax
                    $.ajax({
                        type:'POST',
                        data: form2.serialize(),
                        url: 'valida_prova2.php'
                    }).done(function(data){
                        //fecha o modal da prova
                        $('#modalProvas2').modal('toggle');
                        //abre o modal de carregamento e fecha após 7 segundos
                        $('#modalGif').modal();
                        setTimeout(function(){ $('#modalGif').modal('hide'); }, 15000);
                        //abre o modal de retorno dos dados
                        var mensagem = $.parseJSON(data)["mensagem"];
                        $('#msgRetorno').html(mensagem);
                        setTimeout(function(){ $('#modalRetorno').modal(); }, 15000);
                    }).fail(function(){
                        alert('Erro interno. Contate o Administrador.');
                    });
                    
                }
            });

            // formulário nível 3
            var form3 = $("#form3");
            form3.validate({
                errorPlacement: function errorPlacement(error, element) { element.before(error); },
                rules: {
                    confirm: {
                        equalTo: "#password"
                    }
                }
                
            });
            form3.children("div").steps({
                headerTag: "h3",
                bodyTag: "section",
                transitionEffect: "slideLeft",
                onStepChanging: function(event, currentIndex, newIndex) {
                    form3.validate().settings.ignore = ":disabled,:hidden";
                    return form3.valid();
                },
                onFinishing: function(event, currentIndex) {
                    form3.validate().settings.ignore = ":disabled";
                    return form3.valid();
                },
                onFinished: function(event, currentIndex) {
                    //faz o submit do formulário via ajax
                    $.ajax({
                        type:'POST',
                        data: form3.serialize(),
                        url: 'valida_prova3.php'
                    }).done(function(data){
                        //fecha o modal da prova
                        $('#modalProvas3').modal('toggle');
                        //abre o modal de carregamento e fecha após 7 segundos
                        $('#modalGif').modal();
                        setTimeout(function(){ $('#modalGif').modal('hide'); }, 15000);
                        //abre o modal de retorno dos dados
                        var mensagem = $.parseJSON(data)["mensagem"];
                        $('#msgRetorno').html(mensagem);
                        setTimeout(function(){ $('#modalRetorno').modal(); }, 15000);
                    }).fail(function(){
                        alert('Erro interno. Contate o Administrador.');
                    });
                    
                }
            });

    </script>
</body>
</html>