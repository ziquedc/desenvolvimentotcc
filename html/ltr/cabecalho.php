<?php
if(!isset($_SESSION)){ //Verificar se a sessão não já está aberta.
    session_start();
  }
  //se a sessão nao for criada no login, será redirecionado de volto para o form de login
  if(!isset ($_SESSION['usuario']) == true){
    unset($_SESSION['usuario']);
    
    header('location:index.php');
  } 
  //se identificar uma sessão abre a página
 ?>
<?php
//SELECT NA TABELA USUARIO PARA APRESENTAR NO MODAL DE EDIÇÃO DE PERFIL

include_once('conexao.php');
$conn = getConnection();
$stm = $conn->prepare("SELECT * FROM usuarios WHERE usuario = ? ");
$stm->bindParam(1,$_SESSION['login']);
$stm->execute();
$dados = $stm->fetch(PDO::FETCH_ASSOC);
$imagem = $dados['imagem'];
$id_user = $dados['id'];
//verifica se a imagem no banco de dados esta vazia, se estiver vazia seta uma imagem padrão
if(empty($imagem) ){ //se nao tem imagem no bd
    $imagem = "../../assets/images/users/1.jpg";
}

$admin = $_SESSION['admin'];

?>

<!DOCTYPE html>
<html dir="ltr" lang="pt-br">

<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <!-- Tell the browser to be responsive to screen width -->
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="description" content="">
    <meta name="author" content="">
    <!-- Favicon icon -->
    <link rel="icon" type="image/png" sizes="16x16" href="../../assets/images/icon.png">
    <title>G-Dev</title>
   
    <!--AGENDA FULLCALENDAR-->
    <script src="../../dist/js/jquery.min.js"></script>
    <script src="../../dist/js/moment.min.js"></script>
    <link rel="stylesheet" type="text/css" href="../../dist/css/fullcalendar.min.css">
    <script src="../../dist/js/fullcalendar.min.js"></script>
    <script src="../../dist/js/pt-br.js"></script>
     <!-- Custom CSS -->
    <link href="../../dist/css/style.min.css" rel="stylesheet">
    <link href="../../assets/libs/toastr/build/toastr.min.css" rel="stylesheet">
     <link rel="stylesheet" type="text/css" href="../../assets/extra-libs/multicheck/multicheck.css">
    <link href="../../assets/libs/datatables.net-bs4/css/dataTables.bootstrap4.css" rel="stylesheet">
     <!-- Custom CSS -->
     <link href="../../assets/libs/jquery-steps/jquery.steps.css" rel="stylesheet">
    <link href="../../assets/libs/jquery-steps/steps.css" rel="stylesheet">
    <link href="../../dist/css/style.min.css" rel="stylesheet">
    <!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
    <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
    <!--[if lt IE 9]>
    <script src="https://oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js"></script>
    <script src="https://oss.maxcdn.com/libs/respond.js/1.4.2/respond.min.js"></script>
<![endif]-->
   
</head>
<body>

 <!-- ============================================================== -->
    <!-- Preloader - style you can find in spinners.css -->
    <!-- ============================================================== -->
    <div class="preloader">
        <div class="lds-ripple">
            <div class="lds-pos"></div>
            <div class="lds-pos"></div>
            
        </div>
    </div>
    <!-- ============================================================== -->
    <!-- Main wrapper - style you can find in pages.scss -->
    <!-- ============================================================== -->
    <div id="main-wrapper">
        <!-- ============================================================== -->
        <!-- Topbar header - style you can find in pages.scss -->
        <!-- ============================================================== -->
        <header class="topbar" data-navbarbg="skin5">
            <nav class="navbar top-navbar navbar-expand-md navbar-dark">
                <div class="navbar-header" data-logobg="skin5">
                    <!-- This is for the sidebar toggle which is visible on mobile only -->
                    <a class="nav-toggler waves-effect waves-light d-block d-md-none" href="javascript:void(0)"><i class="ti-menu ti-close"></i></a>
                    <!-- ============================================================== -->
                    <!-- LOGO PRINCIPAL -->
                    <!-- ============================================================== -->
                    <a class="navbar-brand" href="home.php">
                        <!-- Logo icon -->
                        <b class="logo-icon p-l-10">
                            <!--You can put here icon as well // <i class="wi wi-sunset"></i> //-->
                            <!-- Dark Logo icon -->
                            <img src="../../assets/images/logo-icon.png" alt="homepage" class="light-logo" />
                           
                        </b>
                        <!--End Logo icon -->
                         <!-- Logo text -->
                        <span class="logo-text">
                             <!-- dark Logo text -->
                             <img src="../../assets/images/logo-text.png" alt="homepage" class="light-logo" />
                            
                        </span>
                        <!-- Logo icon -->
                        <!-- <b class="logo-icon"> -->
                            <!--You can put here icon as well // <i class="wi wi-sunset"></i> //-->
                            <!-- Dark Logo icon -->
                            <!-- <img src="../../assets/images/logo-text.png" alt="homepage" class="light-logo" /> -->
                            
                        <!-- </b> -->
                        <!--End Logo icon -->
                    </a>
                    <!-- ============================================================== -->
                    <!-- End Logo -->
                    <!-- ============================================================== -->
                    <!-- ============================================================== -->
                    <!-- Toggle which is visible on mobile only -->
                    <!-- ============================================================== -->
                    <a class="topbartoggler d-block d-md-none waves-effect waves-light" href="javascript:void(0)" data-toggle="collapse" data-target="#navbarSupportedContent" aria-controls="navbarSupportedContent" aria-expanded="false" aria-label="Toggle navigation"><i class="ti-more"></i></a>
                </div>
                <!-- ============================================================== -->
                <!-- End Logo -->
                <!-- ============================================================== -->
                <div class="navbar-collapse collapse" id="navbarSupportedContent" data-navbarbg="skin5">
                    <!-- ============================================================== -->
                    <!-- toggle and nav items -->
                    <!-- ============================================================== -->
                    <ul class="navbar-nav float-left mr-auto">
                        <li class="nav-item d-none d-md-block"><a class="nav-link sidebartoggler waves-effect waves-light" href="javascript:void(0)" data-sidebartype="mini-sidebar"><i class="mdi mdi-menu font-24"></i></a></li>
                         
                        <!-- ============================================================== -->
                        <!-- Search -->
                        <!-- ============================================================== -->
                       
                    </ul>
                    <!-- ============================================================== -->
                    <!-- Right side toggle and nav items -->
                    <!-- ============================================================== -->
                    <ul class="navbar-nav float-right">
                        <!-- ============================================================== -->
                        <!-- Comment -->
                        <!-- ============================================================== -->
                        
                        
                        <!-- ============================================================== -->
                        <!-- ICONE DO PERFIL E SUB-MENUS -->
                        <!-- ============================================================== -->
                        <li class="nav-item dropdown">
                            <a class="nav-link dropdown-toggle text-muted waves-effect waves-dark pro-pic" href="" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false"><img src="<?php echo $imagem; ?>" alt="user" class="rounded-circle" width="31"></a>
                            <div class="dropdown-menu dropdown-menu-right user-dd animated">
                                <div class="dropdown-divider"></div>
                                <a class="dropdown-item" href="javascript:void(0)" style="padding: 0px"><i class="ti-user m-r-5 m-l-5"></i> <b><?php  echo $_SESSION['usuario']; ?></b> </a>
                                <hr><div class="dropdown-divider"></div>
                                <div class="dropdown-divider"></div>
                                <a id="btnPerfil" class="dropdown-item ml-2" href="javascript:void(0)" style="padding: 0px" ><i class="ti-settings m-r-5 m-l-5"></i> Meu Perfil</a>
                                <div class="dropdown-divider"></div>
                                <a id="btnSenha" class="dropdown-item ml-2" href="javascript:void(0)" style="padding: 0px" ><i class="ti-lock m-r-5 m-l-5"></i> Alterar Senha</a>
                                <div class="dropdown-divider"></div>
                                <?php if($admin == 1) { ?>
                                    <a class="dropdown-item ml-2" href="admin_users.php" style="padding: 0px" ><i class="fa fa-cogs m-r-5 m-l-5"></i> Gerenciar Usuários </a>
                                    <div class="dropdown-divider"></div>
                                    <a class="dropdown-item ml-2" href="admin_files.php" style="padding: 0px" ><i class="fa fa-upload m-r-5 m-l-5"></i> Upload de Arquivos </a>
                                    <div class="dropdown-divider"></div>
                                <?php } ?>
                                <a class="dropdown-item ml-2" href="logout.php"style="padding: 0px"><i class="fa fa-power-off m-r-5 m-l-5" ></i> Sair</a>
                                <div class="dropdown-divider"></div>
                              
                            </div>
                        </li>
                       
                    </ul>
                </div>
            </nav>
        </header>
        <!-- ============================================================== -->
        <!-- End Topbar header -->
        <!-- ============================================================== -->
        <!-- ============================================================== -->
        <!-- MENU LATERAL ESQUERDA - style you can find in sidebar.scss  -->
        <!-- ============================================================== -->
        <aside class="left-sidebar" data-sidebarbg="skin5">
            <!-- Sidebar scroll-->
            <div class="scroll-sidebar">

                <style>
                    a.noclick{
                        pointer-events: none;
                    }
                </style>


                <!-- MENU BARRA LATERAL ESQUERDA-->
                <nav class="sidebar-nav">
                    <ul id="sidebarnav" class="p-t-30">
                        <li class="sidebar-item"> <a class="sidebar-link waves-effect waves-dark sidebar-link" href="home.php" aria-expanded="false"><i class="mdi mdi-home"></i><span class="hide-menu">Início</span></a></li>
                        <!--NÍVEL 1-->
                        <?php if($_SESSION['nivel'] == 1) { // nível 1?>

                            <?php  if($_SESSION['perfil'] === 'serialista') { ?>
                             <li class="sidebar-item" > <a class="sidebar-link waves-effect waves-dark sidebar-link" href="nivel1_serialista.php" aria-expanded="false"><i class="fa fa-book"></i><span class="hide-menu" >Nível 1</span></a></li>
                            <?php } ?>

                            <?php  if($_SESSION['perfil'] === 'reflexivo') { ?> 
                                <li class="sidebar-item" > <a class="sidebar-link waves-effect waves-dark sidebar-link" href="nivel1_reflexivo.php" aria-expanded="false"><i class="fa fa-book"></i><span class="hide-menu" >Nível 1</span></a></li>
                            <?php } ?>

                            <?php  if($_SESSION['perfil'] === 'divergente') { ?>
                                <li class="sidebar-item" > <a class="sidebar-link waves-effect waves-dark sidebar-link" href="nivel1_divergente.php" aria-expanded="false"><i class="fa fa-book"></i><span class="hide-menu" >Nível 1</span></a></li>
                            <?php } ?>

                            <?php  if($_SESSION['perfil'] === 'holista') { ?> 
                                <li class="sidebar-item" > <a class="sidebar-link waves-effect waves-dark sidebar-link" href="nivel1_holista.php" aria-expanded="false"><i class="fa fa-book"></i><span class="hide-menu" >Nível 1</span></a></li>
                            <?php } ?>

                            <?php  if($_SESSION['perfil'] === 'impulsivo') { ?> 
                                <li class="sidebar-item" > <a class="sidebar-link waves-effect waves-dark sidebar-link" href="nivel1_impulsivo.php" aria-expanded="false"><i class="fa fa-book"></i><span class="hide-menu" >Nível 1</span></a></li>
                            <?php } ?>

                            <?php  if($_SESSION['perfil'] === 'convergente') { ?>
                                <li class="sidebar-item" > <a class="sidebar-link waves-effect waves-dark sidebar-link" href="nivel1_convergente.php" aria-expanded="false"><i class="fa fa-book"></i><span class="hide-menu" >Nível 1</span></a></li>
                            <?php } ?>
                            
                            


                        <?php } ?>
                        <!-- END NÍVEL 1-->

                        <!--NÍVEL 2-->
                        <?php if($_SESSION['nivel'] == 2) { // nível 2?>
                            
                            <?php  if($_SESSION['perfil'] === 'impulsivo') { ?> 
                                <li class="sidebar-item" > <a class="sidebar-link waves-effect waves-dark sidebar-link" href="nivel1_impulsivo.php" aria-expanded="false"><i class="fa fa-book"></i><span class="hide-menu" >Nível 1</span></a></li>
                                <li class="sidebar-item"> <a class="sidebar-link waves-effect waves-dark sidebar-link" href="nivel2_impulsivo.php" aria-expanded="false"><i class="fa fa-book"></i><span class="hide-menu">Nível 2</span></a></li>
                            <?php } ?>

                            <?php  if($_SESSION['perfil'] === 'serialista') { ?> 
                                <li class="sidebar-item" > <a class="sidebar-link waves-effect waves-dark sidebar-link" href="nivel1_serialista.php" aria-expanded="false"><i class="fa fa-book"></i><span class="hide-menu" >Nível 1</span></a></li>
                                <li class="sidebar-item"> <a class="sidebar-link waves-effect waves-dark sidebar-link" href="nivel2_serialista.php" aria-expanded="false"><i class="fa fa-book"></i><span class="hide-menu">Nível 2</span></a></li>
                            <?php } ?>

                            <?php  if($_SESSION['perfil'] === 'divergente') { ?> 
                                <li class="sidebar-item" > <a class="sidebar-link waves-effect waves-dark sidebar-link" href="nivel1_divergente.php" aria-expanded="false"><i class="fa fa-book"></i><span class="hide-menu" >Nível 1</span></a></li>
                                <li class="sidebar-item"> <a class="sidebar-link waves-effect waves-dark sidebar-link" href="nivel2_divergente.php" aria-expanded="false"><i class="fa fa-book"></i><span class="hide-menu">Nível 2</span></a></li>
                            <?php } ?>

                            <?php  if($_SESSION['perfil'] === 'convergente') { ?> 
                                <li class="sidebar-item" > <a class="sidebar-link waves-effect waves-dark sidebar-link" href="nivel1_convergente.php" aria-expanded="false"><i class="fa fa-book"></i><span class="hide-menu" >Nível 1</span></a></li>
                                <li class="sidebar-item"> <a class="sidebar-link waves-effect waves-dark sidebar-link" href="nivel2_convergente.php" aria-expanded="false"><i class="fa fa-book"></i><span class="hide-menu">Nível 2</span></a></li>
                            <?php } ?>

                            <?php  if($_SESSION['perfil'] === 'holista') { ?> 
                                <li class="sidebar-item" > <a class="sidebar-link waves-effect waves-dark sidebar-link" href="nivel1_holista.php" aria-expanded="false"><i class="fa fa-book"></i><span class="hide-menu" >Nível 1</span></a></li>
                                <li class="sidebar-item"> <a class="sidebar-link waves-effect waves-dark sidebar-link" href="nivel2_holista.php" aria-expanded="false"><i class="fa fa-book"></i><span class="hide-menu">Nível 2</span></a></li>    
                            <?php } ?>

                            <?php  if($_SESSION['perfil'] === 'reflexivo') { ?> 
                                <li class="sidebar-item" > <a class="sidebar-link waves-effect waves-dark sidebar-link" href="nivel1_reflexivo.php" aria-expanded="false"><i class="fa fa-book"></i><span class="hide-menu" >Nível 1</span></a></li>
                                <li class="sidebar-item"> <a class="sidebar-link waves-effect waves-dark sidebar-link" href="nivel2_reflexivo.php" aria-expanded="false"><i class="fa fa-book"></i><span class="hide-menu">Nível 2</span></a></li>    
                            <?php } ?>

                        <?php } ?>
                        <!-- END NÍVEL 2-->

                        <!--NÍVEL 3-->
                        <?php if($_SESSION['nivel'] == 3) { // nível 3?>
                            <?php  if($_SESSION['perfil'] === 'impulsivo') { ?> 
                                <li class="sidebar-item" > <a class="sidebar-link waves-effect waves-dark sidebar-link" href="nivel1_impulsivo.php" aria-expanded="false"><i class="fa fa-book"></i><span class="hide-menu" >Nível 1</span></a></li>
                                <li class="sidebar-item"> <a class="sidebar-link waves-effect waves-dark sidebar-link" href="nivel2_impulsivo.php" aria-expanded="false"><i class="fa fa-book"></i><span class="hide-menu">Nível 2</span></a></li>
                                <li class="sidebar-item"> <a class="sidebar-link waves-effect waves-dark sidebar-link" href="nivel3_impulsivo.php" aria-expanded="false"><i class="fa fa-book"></i><span class="hide-menu">Nível 3</span></a></li>
                            <?php } ?>

                            <?php  if($_SESSION['perfil'] === 'serialista') { ?> 
                                <li class="sidebar-item" > <a class="sidebar-link waves-effect waves-dark sidebar-link" href="nivel1_serialista.php" aria-expanded="false"><i class="fa fa-book"></i><span class="hide-menu" >Nível 1</span></a></li>
                                <li class="sidebar-item"> <a class="sidebar-link waves-effect waves-dark sidebar-link" href="nivel2_serialista.php" aria-expanded="false"><i class="fa fa-book"></i><span class="hide-menu">Nível 2</span></a></li>
                                
                                <li class="sidebar-item"> <a class="sidebar-link waves-effect waves-dark sidebar-link" href="nivel3_serialista.php" aria-expanded="false"><i class="fa fa-book"></i><span class="hide-menu">Nível 3</span></a></li>      
                            <?php } ?>

                            <?php  if($_SESSION['perfil'] === 'divergente') { ?> 
                                <li class="sidebar-item" > <a class="sidebar-link waves-effect waves-dark sidebar-link" href="nivel1_divergente.php" aria-expanded="false"><i class="fa fa-book"></i><span class="hide-menu" >Nível 1</span></a></li>
                                <li class="sidebar-item"> <a class="sidebar-link waves-effect waves-dark sidebar-link" href="nivel2_divergente.php" aria-expanded="false"><i class="fa fa-book"></i><span class="hide-menu">Nível 2</span></a></li>
                                <li class="sidebar-item"> <a class="sidebar-link waves-effect waves-dark sidebar-link" href="nivel3_divergente.php" aria-expanded="false"><i class="fa fa-book"></i><span class="hide-menu">Nível 3</span></a></li>
                                <?php } ?>

                            <?php  if($_SESSION['perfil'] === 'convergente') { ?> 
                                <li class="sidebar-item" > <a class="sidebar-link waves-effect waves-dark sidebar-link" href="nivel1_convergente.php" aria-expanded="false"><i class="fa fa-book"></i><span class="hide-menu" >Nível 1</span></a></li>
                                <li class="sidebar-item"> <a class="sidebar-link waves-effect waves-dark sidebar-link" href="nivel2_convergente.php" aria-expanded="false"><i class="fa fa-book"></i><span class="hide-menu">Nível 2</span></a></li>
                                
                                <li class="sidebar-item"> <a class="sidebar-link waves-effect waves-dark sidebar-link" href="nivel3_convergente.php" aria-expanded="false"><i class="fa fa-book"></i><span class="hide-menu">Nível 3</span></a></li>
                                <?php } ?>

                            <?php  if($_SESSION['perfil'] === 'holista') { ?> 
                                <li class="sidebar-item" > <a class="sidebar-link waves-effect waves-dark sidebar-link" href="nivel1_holista.php" aria-expanded="false"><i class="fa fa-book"></i><span class="hide-menu" >Nível 1</span></a></li>
                                <li class="sidebar-item"> <a class="sidebar-link waves-effect waves-dark sidebar-link" href="nivel2_holista.php" aria-expanded="false"><i class="fa fa-book"></i><span class="hide-menu">Nível 2</span></a></li>    
                                <li class="sidebar-item"> <a class="sidebar-link waves-effect waves-dark sidebar-link" href="nivel3_holista.php" aria-expanded="false"><i class="fa fa-book"></i><span class="hide-menu">Nível 3</span></a></li>
                            <?php } ?>

                            <?php  if($_SESSION['perfil'] === 'reflexivo') { ?> 
                                <li class="sidebar-item" > <a class="sidebar-link waves-effect waves-dark sidebar-link" href="nivel1_reflexivo.php" aria-expanded="false"><i class="fa fa-book"></i><span class="hide-menu" >Nível 1</span></a></li>
                                <li class="sidebar-item"> <a class="sidebar-link waves-effect waves-dark sidebar-link" href="nivel2_reflexivo.php" aria-expanded="false"><i class="fa fa-book"></i><span class="hide-menu">Nível 2</span></a></li>    
                                
                                <li class="sidebar-item"> <a class="sidebar-link waves-effect waves-dark sidebar-link" href="nivel3_reflexivo.php" aria-expanded="false"><i class="fa fa-book"></i><span class="hide-menu">Nível 3</span></a></li>
                                <?php } ?>
                        <?php } ?>
                        <!--END NÍVEL 3-->
                        

                        <?php if($admin == 1) { ?>
                            <li class="sidebar-item"> <a class="sidebar-link waves-effect waves-dark sidebar-link" href="admin_users.php" aria-expanded="false"><i class="fa fa-cogs"></i><span class="hide-menu">Gerenciar Usuários</span></a></li>
                            <li class="sidebar-item"> <a class="sidebar-link waves-effect waves-dark sidebar-link" href="admin_files.php" aria-expanded="false"><i class="fa fa-upload"></i><span class="hide-menu">Upload de Arquivos</span></a></li>
                        <?php }?>

                    </ul>
                </nav>

                







                <!-- End Sidebar navigation -->
            </div>
            <!-- End Sidebar scroll-->
        </aside>
        <!-- ============================================================== -->
        <!-- End Left Sidebar - style you can find in sidebar.scss  -->


        <!-- Modal Meu Perfil -->
                <div class="modal fade none-border" id="add-new-event" data-backdrop="static" data-keyboard="false">
                    <div class="modal-dialog">
                        <div class="modal-content">
                            <div class="modal-header bg-secondary" style="color: #ffffff">
                                <h4 class="modal-title"><strong><i class="ti-user"></i> Meu Perfil</strong></h4>
                                <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                            </div>
                            <div class="modal-body">
                                <form id="form" enctype="multipart/form-data">
                                    <div class="row">
                                        <div class="col-md-12">
                                            <label class="control-label"><b>Nome</b></label>
                                            <input class="form-control form-white"  type="text" name="nome" value="<?php echo $dados['nome']; ?>" required />
                                        </div>
                                        
                                    </div>
                                    <div class="row">
                                        <div class="col-md-6">
                                            <label class="control-label mt-3"><b>Usuário</b></label>
                                            <input class="form-control form-white"  type="text" name="usuario" disabled value="<?php echo $dados['usuario']; ?>" />
                                        </div>
                                        <div class="col-md-6">
                                            <label class="control-label mt-3"><b>Perfil</b></label>
                                            <input class="form-control form-white"  type="text" name="perfil" disabled value="<?php echo $dados['perfil']; ?>" />
                                        </div>
                                    </div>
                                    <div class="row">
                                        <div class="col-md-12">
                                            <label class="control-label mt-3"><b>Avatar</b></label>
                                            <input class="form-control form-white"  type="file" name="foto" />
                                        </div>
                                        
                                    </div>
                                
                                    </div>
                                    <div class="modal-footer">
                                        <button type="submit" class="btn btn-danger waves-effect waves-light save-category" >Salvar</button>
                                        <button type="button" class="btn btn-secondary waves-effect" data-dismiss="modal">Cancelar</button>
                            </form>
                            </div>
                        </div>
                    </div>
                </div>
                <!-- END MODAL -->


                <!-- MODAL ALTERAÇÃO DE SENHA -->
                <div class="modal fade" id="modalSenha" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true" data-backdrop="static" data-keyboard="false">
                  <div class="modal-dialog" role="document"> 
                    <div class="modal-content">
                      <div class="modal-header">
                        <h5 class="modal-title text-success" id="exampleModalLabel"><b>Alterar Senha</b></h5>
                        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                          <span aria-hidden="true">&times;</span>
                        </button>
                      </div>
                      <div class="modal-body">
                        <form id="formSenha">
                            <div class="form-group">
                            <label for="text" class="col-form-label"><b>Senha Atual:</b></label>
                            <input type="password" class="form-control" name="atual" id="atual" required>
                          </div>
                          <div class="row">
                            <div class="col">
                                <label for="text" class="col-form-label"><b>Nova Senha:</b></label>
                              <input type="password" class="form-control" name="nova" id="nova" required>
                            </div>
                            <div class="col">
                                <label for="text" class="col-form-label"><b>Confirmar Senha:</b></label>
                              <input type="password" class="form-control" id="nova2" required>
                            </div>
                          </div>

                      </div>

                      <div class="modal-footer">
                        <p id="retornoSenha" style="margin-right: 130px; display: none"><b></b></p>
                        <button type="submit" class="btn btn-success">Alterar</button>
                        <button type="button" class="btn btn-secondary" data-dismiss="modal">Cancelar</button>
                        </form>
                      </div>
                    </div>
                  </div>
                </div>







            <script type="text/javascript">

                //clique no botão MEU PERFIL
                $('#btnPerfil').on("click", function(e){
                    //abre o modal de editar dados
                    $('#add-new-event').modal();
                });

                //salvar alteração de dados
                $('#form').submit(function(e){
                    e.preventDefault();
                    var form = $('form')[0]; 
                    var formData = new FormData(form);

                    salvarEdicaoUsuario(formData);
                });
                //função chamada acima
                function salvarEdicaoUsuario(dados){
                    $.ajax({
                        type:"POST",
                        data:dados,
                        url:"salvar_usuario_edicao.php",
                        processData: false,  
                        contentType: false


                    }).done(function(data){ //se deu certo
                        $sucesso = $.parseJSON(data)["sucesso"];
                        $mensagem = $.parseJSON(data)["mensagem"];
                        $erro = $.parseJSON(data)["erro"];

                        if($sucesso){
                            alert($mensagem);
                            $('#add-new-event').modal('hide');
                        }
                        if($erro){
                            alert($mensagem);
                        }


                    }).fail(function(){
                        alert('Erro interno. Contate o Administrador!');
                    });
                }//fim função salvar edicao



                //ALTERAÇÃO DE SENHA
                $('#btnSenha').on('click', function(){
                    $('#modalSenha').modal();
                })

                $('#formSenha').submit(function(e){ //submit do formulario
                    e.preventDefault();
                    var form = $(this);
                    var senhaNova = $('#nova').val();
                    var senhaNova2 = $('#nova2').val();
                    //verifica as senhas novas digitadas
                    if(senhaNova == senhaNova2){ //se as novas senhas forem iguais
                        $('#retornoSenha').css('display', 'none');
                        $.ajax({
                            type:'POST',
                            data:form.serialize(),
                            url:'alterar_senha.php'

                        }).done(function(data){
                            $sucesso = $.parseJSON(data)["sucesso"];
                            $mensagem = $.parseJSON(data)["mensagem"];
                            if(!$sucesso){
                                $('#retornoSenha').css('color', 'red');
                                $('#retornoSenha').css('display', 'inline');
                                $('#retornoSenha').css('font-weight', 'bold');
                                $('#retornoSenha').html($mensagem);
                            }
                            else{
                                alert($mensagem);
                                $('#modalSenha').modal('hide');
                            }
                        }).fail(function(){
                            alert('Erro interno. Contate o Administrador.');

                        })//fim ajax
                    }
                    else{ //se as novas senhas forem diferentes
                        $('#retornoSenha').css('color', 'red');
                        $('#retornoSenha').css('display', 'inline');
                        $('#retornoSenha').css('font-weight', 'bold');
                        $('#retornoSenha').html("Novas senhas diferem!");
                    }
                });




            </script>