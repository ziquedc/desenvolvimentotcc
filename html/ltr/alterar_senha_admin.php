<?php
if(!isset($_SESSION)){ //Verificar se a sessão não já está aberta.
    session_start();
	}
  //se a sessão nao for criada no login, será redirecionado de volto para o form de login
  if(!isset ($_SESSION['usuario']) == true){
    unset($_SESSION['usuario']);
    header('location:index.php');
  } 
  //se identificar uma sessão abre a página

include_once('conexao.php');
$id = $_POST['idUser'];
$senha =  md5($_POST['senha1Admin']);

$conn = getConnection();
$retorno = array();

$stm2 = $conn->prepare("
                        update usuarios set senha = ? where id = ?
                        ");
$stm2->bindParam(1,$senha);
$stm2->bindParam(2,$id);

if($stm2->execute()){
    $retorno['sucesso'] = true;
    $retorno['mensagem'] = "Senha alterada com sucesso.";
}else{
    $retorno['sucesso'] = false;
    $retorno['mensagem'] = "Erro ao alterar senha. Tente mais tarde!";
}

echo json_encode($retorno);

?>