<!DOCTYPE html>
<html dir="ltr">

<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <!-- Tell the browser to be responsive to screen width -->
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="description" content="">
    <meta name="author" content="">
    <!-- Favicon icon -->
    <link rel="icon" type="image/png" sizes="16x16" href="../../assets/images/icon.png">
    <title>G-Dev</title>
    <!-- Custom CSS -->
    <link href="../../dist/css/style.min.css" rel="stylesheet">
    
    <!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
    <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
    <!--
        
        <link href="../../assets/libs/bootstrap/dist/css/bootstrap.min.css" rel="stylesheet">
        
        [if lt IE 9]>
    <script src="https://oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js"></script>
    <script src="https://oss.maxcdn.com/libs/respond.js/1.4.2/respond.min.js"></script>
<![endif]-->

    <!--CSS DO SCROLL DO MODAL DE QUESTÕES-->
    <style>
        .scroll{
            width: 470px;
            height:400px;
            overflow:auto;
        }
    </style>
</head>

<body>
    <div class="main-wrapper">
        <!-- ============================================================== -->
        <!-- Preloader - style you can find in spinners.css -->
        <!-- ============================================================== -->
        <div class="preloader">
            <div class="lds-ripple">
                <div class="lds-pos"></div>
                <div class="lds-pos"></div>
            </div>
        </div>
        <!-- ============================================================== -->
        <!-- Preloader - style you can find in spinners.css -->
        <!-- ============================================================== -->
        <!-- ============================================================== -->
        <!-- Login box.scss -->
        <!-- ============================================================== -->
        <div class="auth-wrapper d-flex no-block justify-content-center align-items-center bg-dark">
            <div class="auth-box bg-dark border-top border-secondary">
                <!-- Form de Login -->
                <div id="recoverform"> <!--loginform-->
                    <div class="text-center p-t-20 p-b-20">
                        <span class="db"><img src="../../assets/images/logo-completo.png" alt="logo" /></span>
                    </div>
                    <div class="text-center">
                        <p class="h3" style="color: white; font-size: 35px; margin-bottom: 26px; background-color: #af9f42"><b>Login</b></p>
                    </div>
                    <form class="form-horizontal m-t-20" action="valida_usuario.php" method="POST">
                        <div class="row p-b-30">
                            <div class="col-12">
                                <div class="input-group mb-3">
                                    <div class="input-group-prepend">
                                        <span class="input-group-text bg-success text-white" id="basic-addon1"><i class="ti-user"></i></span>
                                    </div>
                                    <input type="text" class="form-control form-control-lg" placeholder="Usuário" id="login1" name="login1" required>
                                </div>
                                <div class="input-group mb-3">
                                    <div class="input-group-prepend">
                                        <span class="input-group-text bg-warning text-white" id="basic-addon2"><i class="ti-pencil"></i></span>
                                    </div>
                                    <input type="password" class="form-control form-control-lg" placeholder="Senha" id="pass" name="pass" required>
                                </div>
                                
                            </div>
                        </div>
                        <div id="msgRetornoLogin" class="alert " role="alert" style="Display:none;"> </div>
                        <div class="row border-top border-secondary">
                            <div class="col-12">
                                <div class="form-group">
                                <div><img class='mt-3' class='ml-3' id='loading1' src='../../assets/images/loading2.gif' width="33px" style="display:none; float:rigth "></div>
                                    <div class="p-t-20">
                                        <button class="btn btn-info" id="to-recover" type="button">Ir para Cadastro</button>
                                        <input id='btnLogin' class="btn btn-success float-right" type="submit" value="Login" >
                                    </div>
                                </div>
                                
                            </div>
                        </div>
                    </form>
                </div>

                <!--CADASTRO DE USUÁRIO-->
                <div id="loginform"> <!--recoverform-->
                    <div class="text-center p-t-20 p-b-20">
                        <!--LOGO-->
                        <span class="db"><img src="../../assets/images/logo-completo.png" alt="logo" /></span>
                    </div>
                    <div class="text-center">
                        <p class="h3" style="color: white; font-size: 35px; margin-bottom: 26px; background-color: #af9f42"><b>Cadastro</b></p>
                    </div>
                    <div class="row m-t-20">
                        <!-- Form de cadastro -->
                        <form class="col-12" id="formcadastro">
                            <!-- nome -->
                            <div class="input-group mb-3">
                                <div class="input-group-prepend">
                                    <span class="input-group-text bg-success text-white" ><i class="mdi mdi-rename-box"></i></span>
                                </div>
                                <input type="text" class="form-control form-control-lg" placeholder="Nome" id="nome" name="nome" required>
                            </div>
                            <!-- usuario -->
                            <div class="input-group mb-3">
                                <div class="input-group-prepend">
                                    <span class="input-group-text bg-info text-white" ><i class="ti-user"></i></span>
                                </div>
                                <input type="text" class="form-control form-control-lg" placeholder="Usuário" id="usuario" name="usuario" required>
                            </div>
                            <!-- senha -->
                            <div class="input-group mb-3">
                                    <div class="input-group-prepend">
                                        <span class="input-group-text bg-warning text-white"><i class="ti-pencil"></i></span>
                                    </div>
                                    <input type="password" class="form-control form-control-lg" placeholder="Senha" id="senha1" name="senha" required>
                                </div>
                            <!-- confirma senha -->
                            <div class="input-group mb-3">
                                    <div class="input-group-prepend">
                                        <span class="input-group-text bg-danger text-white" ><i class="ti-pencil"></i></span>
                                    </div>
                                    <input type="password" class="form-control form-control-lg" placeholder="Confirmar Senha" id="confsenha" name="confsenha" required>
                                </div>

                                <div id="msgRetorno" class="alert " role="alert" style="Display:none;"> </div>
                            <!-- pwd -->
                            <div class="row m-t-20 p-t-20 border-top border-secondary">
                                <div class="col-12">
                                    <a class="btn btn-success btn-sm" href="#" id="to-login" name="action">Ja possui conta? Vá para Login</a>
                                    <input class="btn  btn-info float-right" type="submit" id="btnavancar" value="Avançar">
                                    
                                </div>
                                <div><img class='ml-3' id='loading2' src='../../assets/images/loading2.gif' width="33px" style="display:none; "></div>
                        
                            </div>
                        </form>
                    </div>
                </div>
            </div>
        </div>
        
        <!-- ============================================================== -->
        <!-- Login box.scss -->
        <!-- ============================================================== -->
        <!-- ============================================================== -->
        <!-- Page wrapper scss in scafholding.scss -->
        <!-- ============================================================== -->
        <!-- ============================================================== -->
        <!-- Page wrapper scss in scafholding.scss -->
        <!-- ============================================================== -->
        <!-- ============================================================== -->
        <!-- Right Sidebar -->
        <!-- ============================================================== -->
        <!-- ============================================================== -->
        <!-- Right Sidebar -->
        <!-- ============================================================== -->


        <!--MODAL 01-->
        <div class="modal fade" id="modal1" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true" data-backdrop="static" data-keyboard="false">
            <div class="modal-dialog" role="document" >
                <div class="modal-content" >
                <div class="modal-header">
                    <h5 class="modal-title" id="exampleModalLabel">Modelagem de Perfil</h5>
                </div>
                <div class="modal-body">
                    <p class="h3"> Para melhor experiência na plataforma, é necessário a modelagem do seu perfil de usuário. Responda o questionário para concluir o cadastro. </p>
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-outline-success" onclick="abrirQuestoes()">Avançar para Questionário</button>
                </div>
                </div>
            </div>
            </div>

        
        <!--MODAL PARA AS QUESTÕES DE MODELAGEM DO PERFIL-->
        <div class="modal fade" id="modalQuestoes" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true" data-backdrop="static" data-keyboard="false">
            <div class="modal-dialog" role="document" >
                <div class="modal-content" >
                <div class="modal-header">
                    <h5 class="modal-title" id="exampleModalLabel">Questionário</h5>
                </div>
                <div class="modal-body">
                    
            
                    <!--FORMULÁRIO COM AS QUESTÕES-->
                    <form id="formQuestoes">
                        <input id="idUsuario" type="text" name="id"  hidden>
                        <div class="scroll">
                            <div class="form-check ">
                                <p class="h5"><b>1) Eu considero difícil criar algo original: </b></p>
                                <input class="form-check-input" type="radio" name="quest1" id="q11"  value="4.4211" checked>
                                <label class="form-check-label" for="q11">
                                    Concordo Totalmente
                                </label>
                            </div>
                            <div class="form-check ">
                                <input class="form-check-input" type="radio" name="quest1" id="q12" value="3.3121">
                                <label class="form-check-label" for="q12">
                                    Concordo
                                </label>
                            </div>
                            <div class="form-check">
                                <input class="form-check-input" type="radio" name="quest1" id="q13" value="2.2541" >
                                <label class="form-check-label" for="q13">
                                    Discordo
                                </label>
                            </div>
                            <div class="form-check">
                                <input class="form-check-input" type="radio" name="quest1" id="q14" value="0.51" >
                                <label class="form-check-label" for="q14">
                                    Discordo Totalmente
                                </label>
                            </div>
                            
                            <!--2-->
                            <div class="form-check mt-3">
                                <p class="h5"><b>2) Comumente, eu sigo as orientações dadas sem questionar: </b></p>
                                <input class="form-check-input" type="radio" name="quest2" id="q21" value="4.4212" checked>
                                <label class="form-check-label" for="q21">
                                Concordo Totalmente
                                </label>
                            </div>
                            <div class="form-check">
                                <input class="form-check-input" type="radio" name="quest2" id="q22" value="3.3122">
                                <label class="form-check-label" for="q22">
                                Concordo
                                </label>
                            </div>
                            <div class="form-check">
                                <input class="form-check-input" type="radio" name="quest2" id="q23" value="2.2542" >
                                <label class="form-check-label" for="q23">
                                    Discordo
                                </label>
                            </div>
                            <div class="form-check">
                                <input class="form-check-input" type="radio" name="quest2" id="q24" value="0.52" >
                                <label class="form-check-label" for="q24">
                                    Discordo Totalmente
                                </label>
                            </div>

                            <!--3-->
                            <div class="form-check mt-3">
                                <p class="h5"><b>3) Em geral, eu aceito as regras estabelecidas: </b></p>
                                <input class="form-check-input" type="radio" name="quest3" id="q31" value="4.4213" checked>
                                <label class="form-check-label" for="q31">
                                Concordo Totalmente
                                </label>
                            </div>
                            <div class="form-check">
                                <input class="form-check-input" type="radio" name="quest3" id="q32" value="3.3123">
                                <label class="form-check-label" for="q32">
                                Concordo
                                </label>
                            </div>
                            <div class="form-check">
                                <input class="form-check-input" type="radio" name="quest3" id="q33" value="2.2543" >
                                <label class="form-check-label" for="q33">
                                    Discordo
                                </label>
                            </div>
                            <div class="form-check">
                                <input class="form-check-input" type="radio" name="quest3" id="q34" value="0.53" >
                                <label class="form-check-label" for="q34">
                                    Discordo Totalmente
                                </label>
                            </div>

                             <!--4-->
                             <div class="form-check mt-3">
                                <p class="h5"><b>4) Eu aprecio experenciar situações novas: </b></p>
                                <input class="form-check-input" type="radio" name="quest4" id="q41" value="4.4214" checked>
                                <label class="form-check-label" for="q41">
                                Concordo Totalmente
                                </label>
                            </div>
                            <div class="form-check">
                                <input class="form-check-input" type="radio" name="quest4" id="q42" value="3.3124">
                                <label class="form-check-label" for="q42">
                                Concordo
                                </label>
                            </div>
                            <div class="form-check">
                                <input class="form-check-input" type="radio" name="quest4" id="q43" value="2.2544" >
                                <label class="form-check-label" for="q43">
                                    Discordo
                                </label>
                            </div>
                            <div class="form-check">
                                <input class="form-check-input" type="radio" name="quest4" id="q44" value="0.54" >
                                <label class="form-check-label" for="q44">
                                    Discordo Totalmente
                                </label>
                            </div>
                            
                            <!--5-->
                            <div class="form-check mt-3">
                                <p class="h5"><b>5) Eu sou capaz de formular respostas originais e criativas, com frequência: </b></p>
                                <input class="form-check-input" type="radio" name="quest5" id="q51" value="4.4215" checked>
                                <label class="form-check-label" for="q51">
                                Concordo Totalmente
                                </label>
                            </div>
                            <div class="form-check">
                                <input class="form-check-input" type="radio" name="quest5" id="q52" value="3.3125">
                                <label class="form-check-label" for="q52">
                                Concordo
                                </label>
                            </div>
                            <div class="form-check">
                                <input class="form-check-input" type="radio" name="quest5" id="q53" value="2.2545" >
                                <label class="form-check-label" for="q53">
                                    Discordo
                                </label>
                            </div>
                            <div class="form-check">
                                <input class="form-check-input" type="radio" name="quest5" id="q54" value="0.55" >
                                <label class="form-check-label" for="q54">
                                    Discordo Totalmente
                                </label>
                            </div>

                            <!--6-->
                            <div class="form-check mt-3">
                                <p class="h5"><b>6) Eu aprecio ousar e tentar criar algo diferente: </b></p>
                                <input class="form-check-input" type="radio" name="quest6" id="q61" value="4.4215" checked>
                                <label class="form-check-label" for="q61">
                                Concordo Totalmente
                                </label>
                            </div>
                            <div class="form-check">
                                <input class="form-check-input" type="radio" name="quest6" id="q62" value="3.3125">
                                <label class="form-check-label" for="q62">
                                Concordo
                                </label>
                            </div>
                            <div class="form-check">
                                <input class="form-check-input" type="radio" name="quest6" id="q63" value="2.2545" >
                                <label class="form-check-label" for="q63">
                                    Discordo
                                </label>
                            </div>
                            <div class="form-check">
                                <input class="form-check-input" type="radio" name="quest6" id="q64" value="0.55" >
                                <label class="form-check-label" for="q64">
                                    Discordo Totalmente
                                </label>
                            </div>


                            <!--7-->
                            <div class="form-check mt-3">
                                <p class="h5"><b>7) Costumo enfatizar o contexto global e não os aspectos específicos das tarefas que realizo: </b></p>
                                <input class="form-check-input" type="radio" name="quest7" id="q71" value="4.4216" checked>
                                <label class="form-check-label" for="q71">
                                Concordo Totalmente
                                </label>
                            </div>
                            <div class="form-check">
                                <input class="form-check-input" type="radio" name="quest7" id="q72" value="3.3126">
                                <label class="form-check-label" for="q72">
                                Concordo
                                </label>
                            </div>
                            <div class="form-check">
                                <input class="form-check-input" type="radio" name="quest7" id="q73" value="2.2546" >
                                <label class="form-check-label" for="q73">
                                    Discordo
                                </label>
                            </div>
                            <div class="form-check">
                                <input class="form-check-input" type="radio" name="quest7" id="q74" value="0.56" >
                                <label class="form-check-label" for="q74">
                                    Discordo Totalmente
                                </label>
                            </div>


                            <!--8-->
                            <div class="form-check mt-3">
                                <p class="h5"><b>8) O contexto global de uma situação é o elemento mais relevante para a tomada de decisões: </b></p>
                                <input class="form-check-input" type="radio" name="quest8" id="q81" value="4.4217" checked>
                                <label class="form-check-label" for="q81">
                                Concordo Totalmente
                                </label>
                            </div>
                            <div class="form-check">
                                <input class="form-check-input" type="radio" name="quest8" id="q82" value="3.3127">
                                <label class="form-check-label" for="q82">
                                Concordo
                                </label>
                            </div>
                            <div class="form-check">
                                <input class="form-check-input" type="radio" name="quest8" id="q83" value="2.2547" >
                                <label class="form-check-label" for="q83">
                                    Discordo
                                </label>
                            </div>
                            <div class="form-check">
                                <input class="form-check-input" type="radio" name="quest8" id="q84" value="0.57" >
                                <label class="form-check-label" for="q84">
                                    Discordo Totalmente
                                </label>
                            </div>

                            <!--9-->
                            <div class="form-check mt-3">
                                <p class="h5"><b>9) Ao ler um texto, eu presto mais atenção na ideia geral do que nos detalhes informativos do mesmo.: </b></p>
                                <input class="form-check-input" type="radio" name="quest9" id="q91" value="4.4218" checked>
                                <label class="form-check-label" for="q91">
                                Concordo Totalmente
                                </label>
                            </div>
                            <div class="form-check">
                                <input class="form-check-input" type="radio" name="quest9" id="q92" value="3.3128">
                                <label class="form-check-label" for="q92">
                                Concordo
                                </label>
                            </div>
                            <div class="form-check">
                                <input class="form-check-input" type="radio" name="quest9" id="q93" value="2.2548" >
                                <label class="form-check-label" for="q93">
                                    Discordo
                                </label>
                            </div>
                            <div class="form-check">
                                <input class="form-check-input" type="radio" name="quest9" id="q94" value="0.58" >
                                <label class="form-check-label" for="q94">
                                    Discordo Totalmente
                                </label>
                            </div>

                            <!--10-->
                            <div class="form-check mt-3">
                                <p class="h5"><b>10) Ao realizar uma tarefa, prefiro usar um processo passo-a-passo, trabalhando com pequenas quantidades de dados de cada vez: </b></p>
                                <input class="form-check-input" type="radio" name="quest10" id="q101" value="4.4219" checked>
                                <label class="form-check-label" for="q101">
                                Concordo Totalmente
                                </label>
                            </div>
                            <div class="form-check">
                                <input class="form-check-input" type="radio" name="quest10" id="q102" value="3.3129">
                                <label class="form-check-label" for="q102">
                                Concordo
                                </label>
                            </div>
                            <div class="form-check">
                                <input class="form-check-input" type="radio" name="quest10" id="q103" value="2.2549" >
                                <label class="form-check-label" for="q103">
                                    Discordo
                                </label>
                            </div>
                            <div class="form-check">
                                <input class="form-check-input" type="radio" name="quest10" id="q104" value="0.59" >
                                <label class="form-check-label" for="q104">
                                    Discordo Totalmente
                                </label>
                            </div>

                            <!--11-->
                            <div class="form-check mt-3">
                                <p class="h5"><b>11) Eu dou mais atenção aos pequenos elementos informativos de um material de estudo ou de trabalho: </b></p>
                                <input class="form-check-input" type="radio" name="quest11" id="q111" value="4.42194" checked>
                                <label class="form-check-label" for="q111">
                                Concordo Totalmente
                                </label>
                            </div>
                            <div class="form-check">
                                <input class="form-check-input" type="radio" name="quest11" id="q112" value="3.31293">
                                <label class="form-check-label" for="q112">
                                Concordo
                                </label>
                            </div>
                            <div class="form-check">
                                <input class="form-check-input" type="radio" name="quest11" id="q113" value="2.25492" >
                                <label class="form-check-label" for="q113">
                                    Discordo
                                </label>
                            </div>
                            <div class="form-check">
                                <input class="form-check-input" type="radio" name="quest11" id="q114" value="0.591" >
                                <label class="form-check-label" for="q114">
                                    Discordo Totalmente
                                </label>
                            </div>

                            <!--12-->
                            <div class="form-check mt-3">
                                <p class="h5"><b>12) Diante de um material escrito, eu dou ênfase a cada tópico separadamente e somente depois busco relações entre as partes: </b></p>
                                <input class="form-check-input" type="radio" name="quest12" id="q121" value="4.421946" checked>
                                <label class="form-check-label" for="q121">
                                Concordo Totalmente
                                </label>
                            </div>
                            <div class="form-check">
                                <input class="form-check-input" type="radio" name="quest12" id="q122" value="3.312936">
                                <label class="form-check-label" for="q122">
                                Concordo
                                </label>
                            </div>
                            <div class="form-check">
                                <input class="form-check-input" type="radio" name="quest12" id="q123" value="2.254926" >
                                <label class="form-check-label" for="q123">
                                    Discordo
                                </label>
                            </div>
                            <div class="form-check">
                                <input class="form-check-input" type="radio" name="quest12" id="q124" value="0.5995" >
                                <label class="form-check-label" for="q124">
                                    Discordo Totalmente
                                </label>
                            </div>

                            <!--13-->
                            <div class="form-check mt-3">
                                <p class="h5"><b>13) Em muitas situações, eu não sou uma pessoa atenta, porque sou apressado: </b></p>
                                <input class="form-check-input" type="radio" name="quest13" id="q131" value="4.421947" checked>
                                <label class="form-check-label" for="q131">
                                Concordo Totalmente
                                </label>
                            </div>
                            <div class="form-check">
                                <input class="form-check-input" type="radio" name="quest13" id="q132" value="3.312937">
                                <label class="form-check-label" for="q132">
                                Concordo
                                </label>
                            </div>
                            <div class="form-check">
                                <input class="form-check-input" type="radio" name="quest13" id="q133" value="2.254927" >
                                <label class="form-check-label" for="q133">
                                    Discordo
                                </label>
                            </div>
                            <div class="form-check">
                                <input class="form-check-input" type="radio" name="quest13" id="q134" value="0.5917" >
                                <label class="form-check-label" for="q134">
                                    Discordo Totalmente
                                </label>
                            </div>

                            <!--14-->
                            <div class="form-check mt-3">
                                <p class="h5"><b>14) Em geral, eu não costumo pensar muito para distribuir o meu tempo: </b></p>
                                <input class="form-check-input" type="radio" name="quest14" id="q141" value="4.421948" checked>
                                <label class="form-check-label" for="q141">
                                Concordo Totalmente
                                </label>
                            </div>
                            <div class="form-check">
                                <input class="form-check-input" type="radio" name="quest14" id="q142" value="3.312938">
                                <label class="form-check-label" for="q142">
                                Concordo
                                </label>
                            </div>
                            <div class="form-check">
                                <input class="form-check-input" type="radio" name="quest14" id="q143" value="2.254928" >
                                <label class="form-check-label" for="q143">
                                    Discordo
                                </label>
                            </div>
                            <div class="form-check">
                                <input class="form-check-input" type="radio" name="quest14" id="q144" value="0.598" >
                                <label class="form-check-label" for="q144">
                                    Discordo Totalmente
                                </label>
                            </div>


                             <!--15-->
                             <div class="form-check mt-3">
                                <p class="h5"><b>15) Em muitas situações, eu dou respostas sem ponderar muito sobre elas: </b></p>
                                <input class="form-check-input" type="radio" name="quest15" id="q151" value="4.421949" checked>
                                <label class="form-check-label" for="q151">
                                Concordo Totalmente
                                </label>
                            </div>
                            <div class="form-check">
                                <input class="form-check-input" type="radio" name="quest15" id="q152" value="3.312939">
                                <label class="form-check-label" for="q152">
                                Concordo
                                </label>
                            </div>
                            <div class="form-check">
                                <input class="form-check-input" type="radio" name="quest15" id="q153" value="2.254929" >
                                <label class="form-check-label" for="q153">
                                    Discordo
                                </label>
                            </div>
                            <div class="form-check">
                                <input class="form-check-input" type="radio" name="quest15" id="q154" value="0.599" >
                                <label class="form-check-label" for="q154">
                                    Discordo Totalmente
                                </label>
                            </div>

                            <!--16-->
                            <div class="form-check mt-3">
                                <p class="h5"><b>16) Eu sou uma pessoa muito atenta e organizada: </b></p>
                                <input class="form-check-input" type="radio" name="quest16" id="q161" value="4.421945" checked>
                                <label class="form-check-label" for="q161">
                                Concordo Totalmente
                                </label>
                            </div>
                            <div class="form-check">
                                <input class="form-check-input" type="radio" name="quest16" id="q162" value="3.312935">
                                <label class="form-check-label" for="q162">
                                Concordo
                                </label>
                            </div>
                            <div class="form-check">
                                <input class="form-check-input" type="radio" name="quest16" id="q163" value="2.254925" >
                                <label class="form-check-label" for="q163">
                                    Discordo
                                </label>
                            </div>
                            <div class="form-check">
                                <input class="form-check-input" type="radio" name="quest16" id="q164" value="0.5915" >
                                <label class="form-check-label" for="q164">
                                    Discordo Totalmente
                                </label>
                            </div>

                            <!--17-->
                            <div class="form-check mt-3">
                                <p class="h5"><b>17) Costumo pensar bastante antes de tomar decisões: </b></p>
                                <input class="form-check-input" type="radio" name="quest17" id="q171" value="4.421947" checked>
                                <label class="form-check-label" for="q171">
                                Concordo Totalmente
                                </label>
                            </div>
                            <div class="form-check">
                                <input class="form-check-input" type="radio" name="quest17" id="q172" value="3.312937">
                                <label class="form-check-label" for="q172">
                                Concordo
                                </label>
                            </div>
                            <div class="form-check">
                                <input class="form-check-input" type="radio" name="quest17" id="q173" value="2.254927" >
                                <label class="form-check-label" for="q173">
                                    Discordo
                                </label>
                            </div>
                            <div class="form-check">
                                <input class="form-check-input" type="radio" name="quest17" id="q174" value="0.5917" >
                                <label class="form-check-label" for="q174">
                                    Discordo Totalmente
                                </label>
                            </div>

                            <!--18-->
                            <div class="form-check mt-3">
                                <p class="h5"><b>18) Eu costumo pensar bem antes de dar uma resposta: </b></p>
                                <input class="form-check-input" type="radio" name="quest18" id="q181" value="4.421948" checked>
                                <label class="form-check-label" for="q181">
                                Concordo Totalmente
                                </label>
                            </div>
                            <div class="form-check">
                                <input class="form-check-input" type="radio" name="quest18" id="q182" value="3.312938">
                                <label class="form-check-label" for="q182">
                                Concordo
                                </label>
                            </div>
                            <div class="form-check">
                                <input class="form-check-input" type="radio" name="quest18" id="q183" value="2.254928" >
                                <label class="form-check-label" for="q183">
                                    Discordo
                                </label>
                            </div>
                            <div class="form-check">
                                <input class="form-check-input" type="radio" name="quest18" id="q184" value="0.5918" >
                                <label class="form-check-label" for="q184">
                                    Discordo Totalmente
                                </label>
                            </div>





                        </div><!--final do scroll-->
                </div>
                <div class="modal-footer">

                                     
                        <button type="submit" class="btn btn-success" >Finalizar</button>
                    </form>
                </div>
                </div>
            </div>
            </div>



        <!-- Modal de apresentação do perfil-->
        <div class="modal fade" id="pergilGerado" tabindex="-1" role="dialog" aria-hidden="true" data-backdrop="static" data-keyboard="false">
        <div class="modal-dialog" role="document">
            <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title" id="exampleModalLabel">Perfil Gerado</h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <div class="modal-body">
                <p class="h3"> Parabéns!!! Seu perfil foi gerado com sucesso e classificado como: <b id="perfil"></b> ! Informações detalhadas sobre seu perfil, estão disponíveis no menu inicial da plataforma.</p>
            </div>
            <div class="modal-footer">
                
               <a href="index.php"> <button type="button" class="btn btn-success">Ir para Login</button> </a>
            </div>
            </div>
        </div>
        </div>







    </div>
    <!-- ============================================================== -->
    <!-- All Required js -->
    <!-- ============================================================== -->
    <script src="../../assets/libs/jquery/dist/jquery.min.js"></script>
    <!-- Bootstrap tether Core JavaScript -->
    <script src="../../assets/libs/popper.js/dist/umd/popper.min.js"></script>
    <script src="../../assets/libs/bootstrap/dist/js/bootstrap.min.js"></script>
    <!-- ============================================================== -->
    <!-- This page plugin js -->
    <!-- ============================================================== -->

    <script>

    $('[data-toggle="tooltip"]').tooltip();
    $(".preloader").fadeOut();
    // ============================================================== 
    // Login and Recover Password 
    // ============================================================== 
    $('#to-recover').on("click", function() {
        
        $("#recoverform").slideUp();
        $("#loginform").fadeIn();

    });
    $('#to-login').click(function(){
        
        $("#loginform").hide();
        $("#recoverform").fadeIn();
    });

    //click botao submit form de login
    $('#btnLogin').on('click', function(){
        $('#loading1').css('display','block');
    })

    //SCRIPS PARA SALVAR USUARIO 

    //CADASTRAR USUÁRIO
    $('#formcadastro').submit(function(e){
        e.preventDefault(); //evita o submit com o enter

        //verifica se as senhas diferem 
        if(  $('#senha1').val() != $('#confsenha').val()  ){ 
            $('#msgRetorno').show();
            $('#msgRetorno').addClass("alert-danger");
            $('#msgRetorno').html("As senhas diferem!");

        }else{ //se forem iguais enviam os dados via ajax
            $('#msgRetorno').hide();
            var formulario = $(this); //pega todo o formulário
            var retorno = salvarUsuario(formulario);
        }//fim else
    });


    //função chamada acima
    function salvarUsuario(dados){
        $('#msgRetorno').hide();
        $('#loading2').css('display','block');
        $.ajax({
            type:"POST",
            data:dados.serialize(),
            url:"salvar_usuario.php",
            async: true

        }).then(sucesso,falha);
        
        function sucesso(data){ // data = dados retornados da outra página
            
            $('#msgRetorno').hide();
            $sucesso = $.parseJSON(data)["sucesso"];
            $mensagem = $.parseJSON(data)["mensagem"];
            
            $('#msgRetorno').show();

            if($sucesso){ //se retornou true
                 $idUsuario = $.parseJSON(data)["id"];
                
                $('#msgRetorno').addClass("alert-success");
                $('#msgRetorno').html($mensagem);
                
                //esvazia os campos
                $('#nome').val("");
                $('#usuario').val("");
                $('#senha1').val("");
                $('#confsenha').val("");
                
                $('#idUsuario').val($idUsuario );

                //chama o modal das perguntas depois de 3 segundos
                setTimeout(function(){ 
                    $('#modal1').modal(); 
                    $('#loading2').css('display','none');
                    }, 3000);
                
            }
            else{ //se retornou false
                $('#loading2').css('display','none');
                $('#msgRetorno').addClass("alert-danger");
                $('#msgRetorno').html($mensagem);
            }
        }
        
        function falha(){
            $('#msgRetorno').addClass("alert-danger");
            $('#msgRetorno').html("Erro interno. Contate o Administrador!");
        }
    }//fim função salvar dados


    //função avança modal
    function abrirQuestoes(){
        $('#modal1').modal('hide');
        $('#modalQuestoes').modal();
    }


    //função finalizar modal de questões
    $('#formQuestoes').submit(function(e){
        e.preventDefault(); 
        
        var formQuest = $(this);
        var ret = salvarQuestoes(formQuest);

    });

   function salvarQuestoes(quest){
        $.ajax({
            type:"POST",
            data:quest.serialize(),
            url:"valida_perfil.php",
            async: true
        }).then(certo, errado);
    };

    function certo(dad){
        $('#modalQuestoes').modal('hide');
        $('#pergilGerado').modal();
        $('#perfil').html(dad);
    }

    function errado(){
        alert('Erro interno');
    }

    </script>


    <!-- Bootstrap tether Core JavaScript -->
    <script src="../../assets/libs/popper.js/dist/umd/popper.min.js"></script>
    <script src="../../assets/libs/bootstrap/dist/js/bootstrap.min.js"></script>
</body>
</html>