<?php  
if(!isset($_SESSION)){ //Verificar se a sessão não já está aberta.
    session_start();
}
//se a sessão nao for criada no login, será redirecionado de volto para o form de login
if(!isset ($_SESSION['usuario']) == true){
    unset($_SESSION['usuario']); //destroi a sessao do usuário
    unset($_SESSION['perfil']); //destroi a sessao do usuário
    unset($_SESSION['nivel']); //destroi a sessao do usuário
    unset($_SESSION['login']); //destroi a sessao do usuário
    unset($_SESSION['admin']); //destroi a sessao do usuário
    header('location:index.php');
} 
//se identificar uma sessão abre a página
//SELECT PARA OBTER OS DADOS SO USUÁRIO
$login = $_SESSION['login'];
include_once('conexao.php');
$conn = getConnection();
$stm2 = $conn->prepare("SELECT *
                        FROM usuarios 
                        INNER JOIN acertos ON acertos.id_usuario = usuarios.id
                        WHERE usuario = ? " );
$stm2->bindParam(1,$login);
$stm2->execute();
$dados2 = $stm2->fetch(PDO::FETCH_ASSOC);
$imagem = $dados2['imagem'];

$nivel_user = $dados2['nivel'];
$acertosNivel1 = $dados2['nivel_1'];
$acertosNivel2 = $dados2['nivel_2'];
$acertosNivel3 = $dados2['nivel_3'];
//verifica se a imagem no banco de dados esta vazia, se estiver vazia seta uma imagem padrão
if(empty($imagem) ){ //se nao tem imagem no bd
    $imagem = "../../assets/images/users/1.jpg";
}
//select no materias para download
$stm3 = $conn->prepare(" SELECT * FROM arquivos ORDER BY nome_arquivo" );
$stm3->execute();

//mensagem de informações sobre o perfil a ser apresentada
$perfil_user = $_SESSION['perfil'];
$msgPerfil = "";
if($perfil_user =='convergente'){
    $msgPerfil ='O usuário que possui estilo de aprendizagem convergente identifica-se com pensamento lógico, com raciocínio. Os indivíduos de pensamento convergente são hábeis em lidar com problemas que requerem uma clara resposta convencional, ou seja, uma solução correta, a partir das informações fornecidas. Preferem problemas formais e tarefas melhor estruturadas, que demandam mais as habilidades lógicas. Inibidos emocionalmente são identificados como mais conformistas, disciplinados e conservadores.';
}
if($perfil_user =='divergente'){
    $msgPerfil ='O usuário que possui estilo de aprendizagem divergente possui criatividade, respostas imaginativas, originais e fluentes. São indivíduos que preferem problemas menos estruturados, que são hábeis em tratar de problemas que demandam a generalização de várias respostas igualmente aceitáveis, onde a ênfase é na quantidade, variedade e originalidade das respostas. Socialmente, são considerados como irritados.';
}
if($perfil_user =='holista'){
    $msgPerfil ='Indivíduos holistas dão maior ênfase no contexto global desde o início de uma tarefa; preferem examinar uma grande quantidade de dados, buscando padrões e relações entre eles. Usam hipóteses mais complexas, às quais combinam diversos dados. ';
}
if($perfil_user =='serialista'){
    $msgPerfil ='Indivíduos serialistas dão maior ênfase em tópicos separados e em sequências lógicas, buscando posteriormente padrões e relações no processo, para confirmar ou não suas hipóteses. Assim, usam hipóteses mais simples e uma abordagem lógico-linear (de uma hipótese para a próxima, passo-a passo).';
}
if($perfil_user =='impulsivo'){
    $msgPerfil ='Indivíduos impulsivos detêm-se pouco em ponderação e organização prévia a uma resposta.';
}
if($perfil_user =='reflexivo'){
    $msgPerfil ='Indivíduos cujos pensamentos são mais organizados, sequenciados, elaborando ponderação prévia a uma resposta são considerados reflexivos.';
}
?>

 <?php include('cabecalho.php'); ?> 

        <div class="page-wrapper">
            <!-- ============================================================== -->
            <!-- BOTÕES GRANDES-->
            <!-- ============================================================== -->
            <div class="page-breadcrumb">
                <div class="row">
                    <div class="col-12 d-flex no-block align-items-center">
                        <h4 class="page-title"></h4>
                        <div class="ml-auto text-right">
                            <nav aria-label="breadcrumb">
                                <ol class="breadcrumb">
                                    <li class="breadcrumb-item"><a href="#">Início</a></li>
                                    
                                </ol>
                            </nav>
                        </div>
                    </div>
                </div>
            </div>
            <!-- ============================================================== -->
            <!-- End Bread crumb and right sidebar toggle -->
            <!-- ============================================================== -->
            <!-- ============================================================== -->
            <!-- Container fluid  -->
            <!-- ============================================================== -->
            <div class="container-fluid">
                <!-- ============================================================== -->
                <!-- Start Page Content -->
                <!-- ============================================================== -->
                <div class="row">
                    <!-- Column -->
                    <div class="col-md-6 col-lg-3">
                        <div class="card card-hover" id="livros">
                            <div class="box bg-cyan text-center">
                                <h1 class="font-light text-white"><i class="fa fa-book"></i></h1>
                                <h6  class="text-white">Livros e Materiais Complementares</h6>
                            </div>
                        </div>
                    </div>
                </div>
                <hr>
                <!--CONTEÚDOS DA PAGINA-->
                <!--LADO ESQUERDO-->
                <div class="row">
                    <div class="col-md-6">
                         <div class="card">
                            <div class="card-body">
                                <h4 class="card-title">Dados do Usuário</h4>
                            </div>
                            <div class="comment-widgets scrollable">
                                <!-- Comment Row -->
                                <div class="d-flex flex-row comment-row m-t-0">
                                    <div class="p-2"><img src="<?php echo $imagem; ?>" alt="user" width="50" class="rounded-circle"></div>
                                    <div class="comment-text w-100">
                                        <h6 class="font-medium"><b><?php echo strtoupper($dados['nome']) ?></b></h6>
                                        <ul class="list-group list-group-flush">
                                          <li class="list-group-item"><b>Perfil: </b><?php echo strtoupper($dados['perfil']) ?>  <i class=" ml-3 text-success fa fa-question-circle fa-2x" aria-hidden="true" data-container="body" data-toggle="popover" data-placement="top" data-content="<?php echo $msgPerfil; ?>"></i></li>
                                          <li class="list-group-item"><b>Nível Atual: </b><?php echo strtoupper($dados['nivel']) ?></li>
                                        </ul>
                                       
                                    </div>
                                    
                                </div>
                                <?php if($nivel_user == 'Completo'){ ?>
                                    <span  class=" btn-block badge badge-warning " ><i class="fa fa-star mr-1"></i><i class="fa fa-star mr-1"></i><i class="fa fa-star mr-3"></i>PARABÉNS!! VOCÊ COMPLETOU TODOS OS NÍVEIS</span>
                                <?php } else {?>
                                    <button id='btn' type="button" class="btn btn-block btn-success" ><i class="fa fa-book mr-3"></i>REALIZAR AVALIAÇÃO PRÓXIMO NÍVEL</button>
                                <?php } ?>
                            </div>
                        </div>
                        
                    </div>

                    <!-- LADO DIREITO-->
                    <div class="col-md-6">  
                        <div class="card">
                            <div class="card-body">
                                <h4 class="card-title m-b-0">Desempenho do Usuário</h4>

                                <!--NÍVEL 1-->
                                <?php   
                                    $porcetagemNivel1 = $acertosNivel1 * 10;
                                    $porcetagemNivel2 = $acertosNivel2 * 10;   
                                    $porcetagemNivel3 = $acertosNivel3 * 10;      
                                ?>
                                    <div class="m-t-20">
                                        <div class="d-flex no-block align-items-center">
                                            <span><b><?php echo $porcetagemNivel1; ?>%</b> de acertos - Nível 1</span>
                                            <div class="ml-auto">
                                                <span>100%</span>
                                            </div>
                                        </div>
                                        <div class="progress">
                                            <div class="progress-bar progress-bar-striped" role="progressbar" style="width: <?php echo $porcetagemNivel1; ?>%" aria-valuenow="10" aria-valuemin="0" aria-valuemax="100"></div>
                                        </div>
                                    </div>
                               
                                <!-- END NÍVEL 1-->
                                <!--NÍVEL 2-->
                                <div>
                                    <div class="d-flex no-block align-items-center m-t-25">
                                        <span><b><?php echo $porcetagemNivel2; ?>%</b> de acertos - Nível 2</span>
                                        <div class="ml-auto">
                                            <span>100%</span>
                                        </div>
                                    </div>
                                    <div class="progress">
                                        <div class="progress-bar progress-bar-striped bg-success" role="progressbar" style="width: <?php echo $porcetagemNivel2; ?>%" aria-valuenow="25" aria-valuemin="0" aria-valuemax="100"></div>
                                    </div>
                                </div>
                                <!--END NÍVEL 2-->


                                <!--NÍVEL 3-->
                                <div>
                                    <div class="d-flex no-block align-items-center m-t-25">
                                        <span><b><?php echo $porcetagemNivel3; ?>%</b> de acertos - Nível 3</span>
                                        <div class="ml-auto">
                                            <span>100%</span>
                                        </div>
                                    </div>
                                    <div class="progress">
                                        <div class="progress-bar progress-bar-striped bg-info" role="progressbar" style="width: <?php echo $porcetagemNivel3; ?>%" aria-valuenow="50" aria-valuemin="0" aria-valuemax="100"></div>
                                    </div>
                                </div>
                                <!--END NÍVEL 3-->
                            </div>
                        </div>
                       
                    </div>
                </div>
                
                
                <!--MODAL DE LIVROS-->
                <div class="modal fade" id="modalLivros" tabindex="-1" role="dialog" aria-labelledby="exampleModalCenterTitle" aria-hidden="true">
                  <div class="modal-dialog modal-dialog-centered" role="document">
                    <div class="modal-content">
                      <div class="modal-header bg-primary text-white" >
                        <h5 class="modal-title" id="exampleModalCenterTitle">Materiais para Download</h5>
                        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                          <span aria-hidden="true">&times;</span>
                        </button>
                      </div>
                      <div class="modal-body text-bold">
                        <div style="width: 100%; height: 400px; overflow: scroll;">  
                            <ul class="list-group list-group-flush">

                            <?php   while($linha = $stm3->fetch(PDO::FETCH_ASSOC)) { ?>
                                <li class="list-group-item">
                                    <a href="<?php echo $linha['diretorio_arquivo'] ?>" download><?php echo $linha['nome_arquivo'] ?></a>
                                </li>
                            <?php  } ?>

                            </ul>
                        </div>    

                      </div>
                      <div class="modal-footer">
                        <button type="button" class="btn btn-secondary" data-dismiss="modal">Fechar</button>
                      </div>
                    </div>
                  </div>
                </div>
                <!--END MODAL DE LIVROS-->

                <!-- MODAL DE PROVAS -->
                <?php include('add_modal_questoes_n1.html') ?>
                <?php include('add_modal_questoes_n2.html') ?>
                <?php include('add_modal_questoes_n3.html') ?>


                <!--MODAL DE CARREGAMENTO COM GIF-->
                <!-- Small modal -->
                <div id='modalGif' class="modal fade bd-example-modal-sm"  tabindex="-1" role="dialog" aria-labelledby="mySmallModalLabel" aria-hidden="true" data-backdrop="static" data-keyboard="false">
                <div class="modal-dialog modal-sm">
                    <div class="modal-content">
                    <img class="img-fluid" alt="Responsive image" src="../../assets/images/loading.gif" width="auto" height="250px">
                    <h5 class='text-center'> Processando dados ... </h5>
                    </div>
                </div>
                </div>

                 <!-- Modal de retorno dos dados-->
                <div class="modal fade" id="modalRetorno" tabindex="-1" role="dialog" aria-hidden="true" data-backdrop="static" data-keyboard="false">
                <div class="modal-dialog" role="document">
                    <div class="modal-content">
                    <div class="modal-header">
                        <h5 class="modal-title" id="exampleModalLabel">Resultado Avaliação</h5>
                        
                        </button>
                    </div>
                    <div class="modal-body">
                        <p id='msgRetorno' class="h3">  </p>
                    </div>
                    <div class="modal-footer">
                        
                    <a href="home.php"> <button type="button" class="btn btn-success">Fechar</button> </a>
                    </div>
                    </div>
                </div>
                </div>








                </div>
                </div>
                </div>




                <script type="text/javascript">
                   //abre modal de livros
                   $('#livros').on('click', function(){
                        $('#modalLivros').modal();
                   }); 

                   //abrir modal de provas
                   $('#btn').on('click', function(){

                    <?php if($_SESSION['nivel'] ==1){?>
                       $('#modalProvas1').modal();
                    <?php }?>
                    <?php if($_SESSION['nivel'] ==2){?>
                       $('#modalProvas2').modal();
                    <?php }?>
                    <?php if($_SESSION['nivel'] ==3){?>
                       $('#modalProvas3').modal();
                    <?php }?>

                   })
                </script>







                <!-- ============================================================== -->
                <!-- End PAge Content -->
                <!-- ============================================================== -->
                <!-- ============================================================== -->
                <!-- Right sidebar -->
                <!-- ============================================================== -->
                <!-- .right-sidebar -->
                <!-- ============================================================== -->
                <!-- End Right sidebar -->
                <!-- ============================================================== -->
            </div>
            <!-- ============================================================== -->
            <!-- End Container fluid  -->
            <!-- ============================================================== -->
            <!-- ============================================================== -->
            <!-- footer -->
            <!-- ============================================================== -->
           
        </div>
        <!-- ============================================================== -->
        <!-- End Page wrapper  -->
        <!-- ============================================================== -->
    </div>
    
    


 <?php include('rodape.php'); ?>   