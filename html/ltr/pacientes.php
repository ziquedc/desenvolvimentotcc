<?php include('cabecalho.php') ?>
<?php include('conexao.php') ?>
<?php 
    $conn = getConnection();
    $stm = $conn->prepare("SELECT * FROM pacientes");
    $stm->execute();
    $i=0;
    //$dados = $stm->fetch(PDO::FETCH_ASSOC);
		

?>



<div class="page-wrapper">
            <!-- ============================================================== -->
            <!-- Bread crumb and right sidebar toggle -->
            <!-- ============================================================== -->
            <div class="page-breadcrumb">
                <div class="row">
                    <div class="col-12 d-flex no-block align-items-center">
                        <h4 class="page-title"></h4>
                        <div class="ml-auto text-right">
                            <nav aria-label="breadcrumb">
                                <ol class="breadcrumb">
                                    <li class="breadcrumb-item"><a href="home.php">Início</a></li>
                                    <li class="breadcrumb-item active" aria-current="page">Dentistas</li>
                                </ol>
                            </nav>
                        </div>
                    </div>
                </div>
            </div>

        <div class="container-fluid">


                <div class="row">
                        <div class="col-md-4">
                            <div class="card">
                                <form class="form-horizontal" action="" method="">
                                    <div class="card-body">
                                        <h4 class="card-title">Cadastrar Paciente</h4>
                                        <div class="form-group row">
                                            <label for="fname" class="col-sm-3 text-right control-label col-form-label">Nome</label>
                                            <div class="col-sm-9">
                                                <input type="text" class="form-control" id="fname" name="nome">
                                            </div>
                                        </div>
                                        <div class="form-group row">
                                            <label for="lname" class="col-sm-3 text-right control-label col-form-label">CPF</label>
                                            <div class="col-sm-9">
                                                <input type="text" class="form-control" id="lname" name="cpf">
                                            </div>
                                        </div>
                                        
                                        <div class="form-group row">
                                            <label for="email1" class="col-sm-3 text-right control-label col-form-label">Telefone</label>
                                            <div class="col-sm-9">
                                                <input type="text" class="form-control" id="email1" name="telefone">
                                            </div>
                                        </div>
                                        <div class="form-group row">
                                            <label for="cono1" class="col-sm-3 text-right control-label col-form-label">Endereço</label>
                                            <div class="col-sm-9">
                                                <input type="text" class="form-control" id="cono1" name="endereco">
                                            </div>
                                        </div>
                                        
                                    </div>
                                    <div class="border-top">
                                        <div class="card-body">
                                            <button type="button" class="btn btn-primary">Salvar</button>
                                        </div>
                                    </div>
                                </form>
                            </div>
                            
                            
    
                        </div>
                        <!---COLUNA DIREITA TABELA-->
                        <div class="col-md-8">
                            
                                <div class="card">
                                        <div class="card-body">
                                            <h5 class="card-title">Pacientes Ativos</h5>
                                            <div class="table-responsive">
                                                <table id="table_dentistas" class="table table-striped table-bordered">
                                                    <thead>
                                                        <tr>
                                                            <th>Name</th>
                                                            <th>Telefone</th>
                                                            <th>CPF</th>
                                                            <th>Editar</th>
                                                            <th>Excluir</th>
                                                        </tr>
                                                    </thead>
                                                    <tbody>
                                                    <?php while($linha = $stm->fetch(PDO::FETCH_ASSOC)){?>
                                                        <tr>
                                                            <td><?php echo $linha['nome']; ?> </td>
                                                            <td><?php echo $linha['telefone'];  ?></td>
                                                            <td><?php echo $linha['cpf'];  ?></td>
                                                            <td><a href="editar_paciente.php?cod=<?php echo $cod;?>"> <button type="button" class="btn btn-primary"><i class="mdi mdi-pencil" aria-hidden="true"></i> </button> </a></td>
                                                            <td><a href="excluir_paciente.php?cod=<?php echo $cod;?>"> <button type="button" class="btn btn-danger"><i class="fa fa-trash" aria-hidden="true"></i> </button> </a></td>

                                                            <?php $i++; }?>
                                                        </tr>
                                                        
                                                       
                                                    </tfoot>
                                                </table>
                                            </div>
        
                                        </div>
                                    </div>
                            
                        </div>
                    </div>
                    
                    <!-- ============================================================== -->
                    <!-- End PAge Content -->
                    <!-- ============================================================== -->
                    <!-- ============================================================== -->
                    <!-- Right sidebar -->
                    <!-- ============================================================== -->
                    <!-- .right-sidebar -->
                    <!-- ============================================================== -->
                    <!-- End Right sidebar -->
                    <!-- ============================================================== -->
                </div>

        </div>






<?php include('rodape.php') ?>