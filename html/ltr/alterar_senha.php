<?php
if(!isset($_SESSION)){ //Verificar se a sessão não já está aberta.
    session_start();
	}

  //se a sessão nao for criada no login, será redirecionado de volto para o form de login
  if(!isset ($_SESSION['usuario']) == true){
    unset($_SESSION['usuario']);
    header('location:index.php');
  } 
  //se identificar uma sessão abre a página

include_once('conexao.php');
$atual = md5($_POST['atual']);
$nova =  md5($_POST['nova']);
$login = $_SESSION['login'];

$conn = getConnection();
$stm = $conn->prepare("
						SELECT * 
						FROM usuarios 
						WHERE usuario = ? 
						");
$stm->bindParam(1,$login);
$stm->execute();
$dados = $stm->fetch(PDO::FETCH_ASSOC);

$retorno = array();

if($atual == $dados['senha']){
	$stm2 = $conn->prepare("
							UPDATE usuarios 
							SET senha = ? 
							WHERE usuario = ? 
							");
	$stm2->bindParam(1,$nova);
	$stm2->bindParam(2,$login);
	if($stm2->execute()){
		$retorno['sucesso'] = true;
    	$retorno['mensagem'] = "Senha alterada com sucesso.";
	}else{
		$retorno['sucesso'] = false;
    	$retorno['mensagem'] = "Erro ao alterar senha. Tente mais tarde!";
	}
	
}else{//se as senhas diferem
	$retorno['sucesso'] = false;
    $retorno['mensagem'] = "Senha atual inválida.";
}

echo json_encode($retorno);

?>