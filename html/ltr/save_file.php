<?php
include_once('conexao.php');


$arquivo = $_FILES['arquivo'];
$nome = $_POST['nome'];

//array de retorno para o ajax
$retorno = array();
$tamanho = 8388608;

$error = array();

$path = $_FILES['arquivo']['name'];
$extenssao = pathinfo($path, PATHINFO_EXTENSION);

// Verifica se o arquivo é uma imagem
if($extenssao != 'pdf'){
    $error[1] = "Isso não é um arquivo PDF.";
    } 


// Verifica se o tamanho da imagem é maior que o tamanho permitido
if($arquivo['size'] > $tamanho) {
   $error[2] = "O arquivo deve ter no máximo ".$tamanho." bytes";
}  

// Se não houver nenhum erro
if (count($error) == 0) {
		
    // Pega extensão da imagem
    preg_match("/\.(pdf){1}$/i", $arquivo["name"], $ext);

    // Gera um nome único para a imagem
    $nome_arquivo = $nome . "." . $ext[1];

    // Caminho de onde ficará a imagem
    $caminho_arquivo = "../../assets/arquivos/livros/" . $nome_arquivo;

    
    // Faz o upload da imagem para seu respectivo caminho
    move_uploaded_file($arquivo["tmp_name"], $caminho_arquivo);

    
   
    
    //SALVAR NO BANCO
	$conn = getConnection();
	$stm = $conn->prepare("INSERT INTO arquivos (nome_arquivo, diretorio_arquivo) VALUES(?,?)");
    $stm->bindParam(1,$nome_arquivo);
	$stm->bindParam(2,$caminho_arquivo);
	

	$retorno = array();

    if($stm->execute()){
        //echo "<script>alert('Cadastrado com sucesso');</script>"; 
        //echo "<script>window.location = 'cpanel_banner.php';</script>";
        $retorno['sucesso'] = true;
        $retorno['mensagem'] = "Arquivo salvo com sucesso";
    }else{
        $retorno['sucesso'] = false;
        $retorno['mensagem'] = "Erro ao salvar arquivo";
    }
}

// Se houver mensagens de erro, exibe-as
if (count($error) != 0) {
    foreach ($error as $erro) {
        $retorno['sucesso'] = false;
        $retorno['mensagem'] = $erro;
        
    }
}


echo json_encode($retorno);









?>
