<?php
include_once('conexao.php');

$id = $_POST['id'];

$conn = getConnection();
$stm = $conn->prepare('SELECT * FROM usuarios WHERE id = ?');
$stm->bindParam(1, $id);
$stm->execute();
$dados = $stm->fetch(PDO::FETCH_ASSOC);
$imagem = $dados['imagem'];

$stm2 = $conn->prepare('DELETE FROM usuarios WHERE id = ?');
$stm2->bindParam(1,$id);
unlink($imagem);
//array de retorno
$retorno = array();

if($stm2->execute()){
	$retorno['sucesso'] = true;
    $retorno['mensagem'] = "Usuário excluído com sucesso";
    echo 'ok';
}else{
	$retorno['sucesso'] = false;
    $retorno['mensagem'] = "Falha ao excluir";
    echo 'falha';
}

echo json_encode($retorno);




?>