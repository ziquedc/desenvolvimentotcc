<?php
//VALIDAÇÃO DA SESSION ABERTA OU NAO
if(!isset($_SESSION)){ //Verificar se a sessão não já está aberta.
    session_start();
}
//se a sessão nao for criada no login, será redirecionado de volto para o form de login
if(!isset ($_SESSION['usuario']) == true){
    unset($_SESSION['usuario']);
    header('location:index.php');
}

##########################################################################################

include_once('conexao.php');
$nome = $_POST['nome'];
//$imagem = $_FILES['foto'];
$login = $_SESSION['login'];


//VERIFICA SE A IMAGEM VEIO VAZIA 
if(!empty($_FILES['foto']['name'])){ //se a imagem nao estiver vazia
    $imagem = $_FILES['foto'];

    //SELECT NA TABELA PARA PEGAR O CAMINHO DA IMAGEM ATUAL E EXCLUIR
    $conn = getConnection();
    $stm2 = $conn->prepare("SELECT * FROM usuarios WHERE usuario = ? ");
    $stm2->bindParam(1,$login);
    $stm2->execute();
    $dados = $stm2->fetch(PDO::FETCH_ASSOC);

    //PROCESSAR IMAGEM
    $largura = 20000;
    $altura = 20000;
    $tamanho = 10485760;


    $error = array();
    // Verifica se o arquivo é uma imagem
    if(!preg_match("/^image\/(pjpeg|jpeg|png|gif|bmp)$/", $imagem["type"])){
        $error[1] = "Isso não é uma imagem.";
        } 
    // Pega as dimensões da imagem
    $dimensoes = getimagesize($imagem["tmp_name"]);

    // Verifica se a largura da imagem é maior que a largura permitida
    if($dimensoes[0] > $largura) {
        $error[2] = "A largura da imagem não deve ultrapassar ".$largura." pixels";
    }

    // Verifica se a altura da imagem é maior que a altura permitida
    if($dimensoes[1] > $altura) {
        $error[3] = "Altura da imagem não deve ultrapassar ".$altura." pixels";
    }

    // Verifica se o tamanho da imagem é maior que o tamanho permitido
    if($imagem["size"] > $tamanho) {
       $error[4] = "A imagem deve ter no máximo ".$tamanho." bytes";
    }  

    // Se não houver nenhum erro
    if (count($error) == 0) {
            
        // Pega extensão da imagem
        preg_match("/\.(gif|bmp|png|jpg|jpeg){1}$/i", $imagem["name"], $ext);

        // Gera um nome único para a imagem
        $nome_imagem = md5(uniqid(time())) . "." . $ext[1];

        // Caminho de onde ficará a imagem
        $caminho_imagem = "../../assets/images/users/" . $nome_imagem;

        //    $caminho_imagem = "
        
        //exclui a imagem atual do diretorio
        unlink($dados['imagem']);
        
        // Faz o upload da imagem para seu respectivo caminho
        move_uploaded_file($imagem["tmp_name"], $caminho_imagem);

        //update na tabela salvando o usuário
        $conn = getConnection();
        $stm = $conn->prepare("UPDATE usuarios SET nome = ?, imagem = ? WHERE usuario = ? ");
        $stm->bindParam(1,$nome);
        $stm->bindParam(2,$caminho_imagem);
        $stm->bindParam(3,$login);
        

        $retorno = array();

        if($stm->execute()){
            //echo "<script>alert('Cadastrado com sucesso');</script>"; 
            //echo "<script>window.location = 'cpanel_banner.php';</script>";
            $retorno['sucesso'] = true;
            $retorno['mensagem'] = "Usuário alterado com sucesso";
        }else{
            $retorno['sucesso'] = false;
            $retorno['mensagem'] = "Erro ao alterar dados";
        }
    }

    // Se houver mensagens de erro, exibe-as
    if (count($error) != 0) {
        foreach ($error as $erro) {
            $retorno['erro'] = true;
            $retorno['mensagem'] = $erro;
            
        }
    }


}
else{//se a imagem vier vazia

    //update na tabela salvando o usuário
    $conn = getConnection();
    $stm = $conn->prepare("UPDATE usuarios SET nome = ?WHERE usuario = ? ");
    $stm->bindParam(1,$nome);
    $stm->bindParam(2,$login);
  

    if($stm->execute()){
        //echo "<script>alert('Cadastrado com sucesso');</script>"; 
        //echo "<script>window.location = 'cpanel_banner.php';</script>";
        $retorno['sucesso'] = true;
        $retorno['mensagem'] = "Usuário alterado com sucesso";
    }else{
        $retorno['sucesso'] = false;
        $retorno['mensagem'] = "Erro ao alterar dados";
    }


}//fim do else




echo json_encode($retorno);



































?>