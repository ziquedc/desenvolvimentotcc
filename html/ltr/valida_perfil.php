<?php
$id = $_POST["id"];

//PEGAR OS DADOS VINDOS DO QUESTIONÁRIO
//convergente
$questao1 = (float) str_replace(",",".", $_POST['quest1']);
$questao2 = (float) str_replace(",",".", $_POST['quest2']);
$questao3 = (float) str_replace(",",".", $_POST['quest3']);
$convergente = $questao1 + $questao2 + $questao3;

//divergente
$questao4 = (float) str_replace(",",".", $_POST['quest4']);
$questao5 = (float) str_replace(",",".", $_POST['quest5']);
$questao6 = (float) str_replace(",",".", $_POST['quest6']);
$divergente = $questao4 + $questao5 + $questao6;

//holista
$questao7 = (float) str_replace(",",".", $_POST['quest7']);
$questao8 = (float) str_replace(",",".", $_POST['quest8']);
$questao9 = (float) str_replace(",",".", $_POST['quest9']);
$holista = $questao7 + $questao8 + $questao9;

//Serialista
$questao10 = (float) str_replace(",",".", $_POST['quest10']);
$questao11 = (float) str_replace(",",".", $_POST['quest11']);
$questao12 = (float) str_replace(",",".", $_POST['quest12']);
$serialista = $questao10 + $questao11 + $questao12;

//Impulsivo
$questao13 = (float) str_replace(",",".", $_POST['quest13']);
$questao14 = (float) str_replace(",",".", $_POST['quest14']);
$questao15 = (float) str_replace(",",".", $_POST['quest15']);
$impulsivo = $questao13 + $questao14 + $questao15;

//Reflexivo
$questao16 = (float) str_replace(",",".", $_POST['quest16']);
$questao17 = (float) str_replace(",",".", $_POST['quest17']);
$questao18 = (float) str_replace(",",".", $_POST['quest18']);
$reflexivo = $questao16 + $questao17 + $questao18;

//gera um array com os perfis e a soma dos pesos de suas questões
$perfis = array(
    'convergente' => $convergente,
    'divergente' => $divergente,
    'holista' => $holista,
    'serialista' => $serialista,
    'impulsivo' => $impulsivo,
    'reflexivo' => $reflexivo,
);

//ordena o array em ordem decrescente
arsort($perfis);
//pega a chave (nome) do maior perfil (no caso, o primeiro, pois esta ordenado em ordem decrescente)
$perfil_gerado =  key($perfis);


//update na tabela usuários com o perfil gerado
include("conexao.php");

//conexao e insert
$conn = getConnection();
$stm = $conn->prepare("UPDATE  usuarios set perfil = ? WHERE id = ?");
$stm->bindParam(1,$perfil_gerado);
$stm->bindParam(2,$id);
$stm->execute();

echo strtoupper($perfil_gerado);

?>