<?php
if(!isset($_SESSION)){ //Verificar se a sessão não já está aberta.
    session_start();
  }
  //se a sessão nao for criada no login, será redirecionado de volto para o form de login
  if(!isset ($_SESSION['usuario']) == true){
    unset($_SESSION['usuario']);
    header('location:index.php');
  } 
  //se identificar uma sessão abre a página
 ?>
<?php include('cabecalho.php') ?>



        <div class="page-wrapper">
            <!-- ============================================================== -->
            <!-- Bread crumb and right sidebar toggle -->
            <!-- ============================================================== -->
            <div class="page-breadcrumb">
                <div class="row">
                    <div class="col-12 d-flex no-block align-items-center">
                        <h4 class="page-title">Linguagem C</h4>
                        <div class="ml-auto text-right">
                            <nav aria-label="breadcrumb">
                                <ol class="breadcrumb">
                                    <li class="breadcrumb-item"><a href="#">Início</a></li>
                                    <li class="breadcrumb-item active" aria-current="page">Nível 2</li>
                                </ol>
                            </nav>
                        </div>
                    </div>
                </div>
            </div>
            <!-- ============================================================== -->
            <!-- End Bread crumb and right sidebar toggle -->
            <!-- ============================================================== -->
            <!-- ============================================================== -->
            <!-- Container fluid  -->
            <!-- ============================================================== -->
            <div class="container-fluid">
                <!-- ============================================================== -->
                <!-- Start Page Content -->
                <!-- ============================================================== -->
                <div class="row">
                    <div class="col-md-6">
                        <div class="card">
                            <div class="card-body">
                                <h4 class="card-title m-b-0">Árvores Binárias</h4>
                            </div>
                            <div class="comment-widgets scrollable">
                                <!-- ARVORES -->
                                <div class="d-flex flex-row comment-row m-t-0">
                                   
                                    <div class="comment-text w-100">
                                        <h5 class="font-medium"><u><b>O Que é uma árvore?</b></u></h5>  
                                        <span class="m-b-15 d-block">
                                        Uma árvore binária é uma estrutura de dados caracterizada por: Ou não tem elemento algum. Ou tem um elemento distinto, 
                                        denominado raiz, 
                                        com dois ponteiros para duas estruturas diferentes, denominadas sub-árvore esquerda e sub-árvore direita.
                                        </span>

                                        <h5 class="font-medium"><u><b>Utilização</b></u></h5>  
                                        <span class="m-b-15 d-block">
                                            É utilizada em casos onde os dados ou objetos possuem relações "hierárquicas" entre si, onde aparece um nodo destaque
                                            (raiz). Os demais nodos estarão organizados de acordo com seus níveis, onde cada nodo terá nodos ascendentes e descendentes.
                                        </span>


                                        
                                    </div>
                                </div>
                                
                                <div class="d-flex flex-row comment-row m-t-0">
                                    <div class="comment-text w-100">
                                    <div class="comment-text w-100">
                                        <iframe src="https://www.youtube.com/embed/PgZflufXGUU" width="595" height="485" frameborder="0" marginwidth="0" marginheight="0" scrolling="no" style="border:1px solid #CCC; border-width:1px; margin-bottom:5px; max-width: 100%;" allowfullscreen> </iframe> <div style="margin-bottom:5px"> <strong> <a href="https://youtu.be/72oa9i7t-CY" title="Variaveis" target="_blank"></a> </strong> Fonte: <strong><a href="https://www.youtube.com/channel/UCI4SwiUc4S2qUjlZhCLY_jA" target="_blank">Rodrigo Guerra - Youtube</a></strong> </div>
                                        
                                    </div><br>
                                    <a href="https://sites.google.com/site/proffdesiqueiraed/aulas/aula-10---arvores" target="_blank"><u><i class="fa fa-search" aria-hidden="true"></i> Link para pesquisa</u></a>
                                    <hr>
                                    </div>
                                </div>
                                <!-- END ARVORES -->

                               <!-- ÁRVORES BINÁRIAS-->
                                <div class="d-flex flex-row comment-row m-t-0">
                                    <div class="comment-text w-100">
                                        <h5 class="font-medium"><u><b></u></b></h5>
                                        <br/>
                                        <img class="img-fluid" alt="Responsive image"  src="../../assets/images/conteudos/arvore-binaria-ex.png" height="80%">
                                        <img class="img-fluid" alt="Responsive image"  src="../../assets/images/conteudos/arvore-binaria-3.png" height="80%">
                                        <img class="img-fluid" alt="Responsive image"  src="../../assets/images/conteudos/arvore-binaria-4.png" height="80%">
                                        <img class="img-fluid" alt="Responsive image"  src="../../assets/images/conteudos/arvore-binaria-5.png" height="80%">
                                        
                                        
                                        <div class="comment-footer">
                                            
                                        </div>
                                    </div>
                                </div>
                                
                                <div class="card" style="margin-top: 15px">
                                    <div class="comment-widgets scrollable">
                                        <div class="d-flex flex-row comment-row m-t-0">
                                        
                                            <div class="comment-text w-100">
                                                

                                            </div>
                                        </div>
                                        
                                        
                                    </div>
                                </div>
                                <!-- END ÁRVORES BINÁRIAS-->
                            </div>
                        </div>
                        
                    </div>
                    <!--FIM COLUNA ESQUERDA-->

                    <!--INICIO COLUNA DIREITA-->
                    <div class="col-md-6">
                     <!--PILHAS -->
                     <div class="card">
                            <div class="card-body">               
                                <div class="d-flex flex-row comment-row m-t-0">
                                </div>
                                <h4 class="card-title m-b-0">Pilhas</h4>
                                <span class="m-b-15 d-block"><br/>
                                Uma pilha é um conjunto ordenado de itens, no qual novos itens podem ser inseridos e a partir do qual podem ser eliminados itens de uma extremidade, chamada topo da pilha. 

                                Também é chamada de lista linear, onde todas as inserções e eliminações são feitas em apenas uma das extremidades, chamada topo.
                                </span>
                                <img class="img-fluid" alt="Responsive image" src="../../assets/images/conteudos/pilha1.png" width="auto" height="222px">
                                <div class="embed-responsive embed-responsive-1by1">
                                        <iframe class="em   bed-responsive-item" src="../../assets/arquivos/conteudos/tutorialpilhas.pdf" allowfullscreen width="500" height="500"></iframe>
                                        <div style="margin-bottom:5px"> <strong> <a  title="Teste de mesa" target="_blank"></a> </strong> Fonte: <strong></strong> </div>
                                </div>
                            </div>
                            
                        </div>
                        <!--END PILHAS -->

                        <!--FILAS -->
                        <div class="card">
                            <div class="card-body">
                                <h4 class="card-title m-b-0">Filas</h4>
                                <br>
                                <span>
                                    <b>O QUE É?</b> <br/>
                                    Estruturas de dados que se comportam
                                como filas tradicionais. A finalidade é
                                registrar a ordem de chegada de
                                componentes. (FORBELLONE;
                                EBERSPACHER, 2005)
                               

                                    <img class="img-fluid" alt="Responsive image" src="../../assets/images/conteudos/fila1.png" width="auto" height="222px">
                                    <br/><br/>
                                    <div class="embed-responsive embed-responsive-4by3">
                                        <iframe class="em   bed-responsive-item" src="../../assets/arquivos/conteudos/filasppt.pdf" allowfullscreen width="500" height="500"></iframe>
                                        <div style="margin-bottom:5px"> <strong> <a  title="Teste de mesa" target="_blank"></a> </strong> Fonte: <strong></strong> </div>
                                    </div><br>
                                    
                                    <i class="fa fa-exclamation-triangle text-success" aria-hidden="true"></i><b>  FIFO</b>(FISRT-IN/FISRT-OUT) - Primeiro a entrar / Primeiro a sair
                                    <br/>
                                    <br/>
                                    <i class="fa fa-exclamation-triangle text-success" aria-hidden="true"></i>  DUAS OPERAÇÔES PARA TRABALHAR COM FILAS:
                                    <ul>
                                        <li> <b>Enqueue -</b> insere elemento; </li>
                                        <li> <b>Dequeue -</b> remove elemento;</li>
                                    </ul>
                                    <br/>
                                    <img class="img-fluid" alt="Responsive image" src="../../assets/images/conteudos/filas2.png" width="auto" height="222px">
                                    <img class="img-fluid" alt="Responsive image" src="../../assets/images/conteudos/fila3.png" width="auto" height="222px">
                                    <img class="img-fluid" alt="Responsive image" src="../../assets/images/conteudos/fila4.png" width="auto" height="222px">
                                    <a href="https://sites.google.com/site/proffdesiqueiraed/aulas/aula-7---filas" target="_blank"><u><i class="fa fa-search" aria-hidden="true"></i> Link para pesquisa</u></a>
                                    <hr>
                                </span>
                            </div>
                        </div>
                        <!--END FILAS -->
                    </div>
                </div>
                
                <!-- ============================================================== -->
                <!-- End PAge Content -->
                <!-- ============================================================== -->
                <!-- ============================================================== -->
                <!-- Right sidebar -->
                <!-- ============================================================== -->
                <!-- .right-sidebar -->
                <!-- ============================================================== -->
                <!-- End Right sidebar -->
                <!-- ============================================================== -->
            </div>
            <!-- ============================================================== -->
            <!-- End Container fluid  -->
            <!-- ============================================================== -->
            <!-- ============================================================== -->
            <!-- footer -->
            <!-- ============================================================== -->
           
            <!-- ============================================================== -->
            <!-- End footer -->
            <!-- ============================================================== -->
        </div>  


        



<?php include('rodape.php') ?>