<?php
session_start(); //inicia a sessão

include_once("conexao.php");
$usuario = $_POST["login1"];
$senha = md5($_POST["pass"]);
//conexao
$conn = getConnection();
$stm = $conn->prepare("SELECT * FROM usuarios WHERE usuario = ? ");
$stm->bindParam(1,$usuario);
$stm->execute();
$linhaAfetadas = $stm->rowCount();

if($linhaAfetadas > 0){ //se o usuário estiver cadastrado no bd
    $dados = $stm->fetch(PDO::FETCH_ASSOC);
    $senhaBanco = $dados['senha'];

    if($senha == $senhaBanco){ //senhas iguais
        $_SESSION['usuario'] = $dados['nome']; //cria uma sessão para o usuário
        $_SESSION['perfil'] = $dados['perfil']; //cria uma sessão para o usuário
        $_SESSION['nivel'] = $dados['nivel']; //cria uma sessão para o usuário
        $_SESSION['login'] = $dados['usuario'];
        $_SESSION['admin'] = $dados['admin'];
        header('location:home.php');

    }else{//senha incorreta
        echo "<script> alert('Senha incorreta!'); </script>";
        echo "<script>window.location = 'index.php';</script>";
    }




}if($linhaAfetadas == 0){ //usuário nao cadastrado
    unset($_SESSION['usuario']); //destroi a sessao do usuário
    unset($_SESSION['perfil']); //destroi a sessao do usuário
    unset($_SESSION['nivel']); //destroi a sessao do usuário
    unset($_SESSION['login']); //destroi a sessao do usuário
    unset($_SESSION['admin']); //destroi a sessao do usuário
    echo "<script> alert('Usuário não cadastrado!'); </script>";
    echo "<script>window.location = 'index.php';</script>";
}



?>