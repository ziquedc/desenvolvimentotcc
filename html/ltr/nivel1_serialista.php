<?php
if(!isset($_SESSION)){ //Verificar se a sessão não já está aberta.
    session_start();
  }
  //se a sessão nao for criada no login, será redirecionado de volto para o form de login
  if(!isset ($_SESSION['usuario']) == true){
    unset($_SESSION['usuario']);
    header('location:index.php');
  } 
  //se identificar uma sessão abre a página
 ?>
<?php include('cabecalho.php') ?>



        <div class="page-wrapper">
            <!-- ============================================================== -->
            <!-- Bread crumb and right sidebar toggle -->
            <!-- ============================================================== -->
            <div class="page-breadcrumb">
                <div class="row">
                    <div class="col-12 d-flex no-block align-items-center">
                        <h4 class="page-title">Linguagem C</h4>
                        <div class="ml-auto text-right">
                            <nav aria-label="breadcrumb">
                                <ol class="breadcrumb">
                                    <li class="breadcrumb-item"><a href="#">Início</a></li>
                                    <li class="breadcrumb-item active" aria-current="page">Nível 1</li>
                                </ol>
                            </nav>
                        </div>
                    </div>
                </div>
            </div>
            <!-- ============================================================== -->
            <!-- End Bread crumb and right sidebar toggle -->
            <!-- ============================================================== -->
            <!-- ============================================================== -->
            <!-- Container fluid  -->
            <!-- ============================================================== -->
            <div class="container-fluid">
                <!-- ============================================================== -->
                <!-- Start Page Content -->
                <!-- ============================================================== -->
                <div class="row">
                    <div class="col-md-6">
                        <div class="card">
                            <div class="card-body">
                                <h4 class="card-title m-b-0">Variáveis</h4>
                            </div>
                            <div class="comment-widgets scrollable">
                                <!-- VARIÁVEIS -->
                                <div class="d-flex flex-row comment-row m-t-0">
                                   
                                    <div class="comment-text w-100">
                                        <h5 class="font-medium"><u><b>O Que é uma variável?</b></u></h5>  
                                        <span class="m-b-15 d-block">
                                        Uma variável simples (ou simplesmente variável) é uma posição de memória cujo conteúdo pode ser
                                        modificado durante a execução de um programa. A referência a uma variável no programa é feita através do
                                        seu <i>identificador</i>, os valores que podem ser nela armazenados dependem do seu tipo de dado 

                                        </span>
                                        
                                    </div>
                                </div>
                                
                                
                                <div class="d-flex flex-row comment-row m-t-0">
                                   <!-- TIPOS DE VARIÁVEIS -->
                                    <div class="comment-text w-100">
                                        <h5 class="font-medium"> <u><b>Tipos de variáveis</b></u> </h5> 
                                        <span class="m-b-15 d-block">
                                        O tipo de dado associado a uma variável é o conjunto dos valores que podem ser nela armazenados. A
                                        linguagem C dispõe dos tipos de dados discriminados na tabela a seguir:
                                        </span>
                                        <table class="table">
                                            <thead>
                                                <tr>
                                                    <th scope="col">Tipo</th>
                                                    <th scope="col">Núnero de Bytes</th>
                                                </tr>
                                            </thead>
                                            <tbody>
                                                <tr>
                                                    <td>char</td>
                                                    <td class="text-primary">1</td>                                                   
                                                </tr>
                                                <tr>
                                                    <td>int</td>
                                                    <td class="text-primary">2</td>                                                   
                                                </tr>
                                                <tr>
                                                    <td>long ou long int</td>
                                                    <td class="text-primary">4</td>                                                   
                                                </tr>
                                                <tr>
                                                    <td>float</td>
                                                    <td class="text-primary">4</td>                                                   
                                                </tr>
                                                <tr>
                                                    <td>double</td>
                                                    <td class="text-primary">8</td>                                                   
                                                </tr>
                                                <tr>
                                                    <td>void</td>
                                                    <td class="text-primary">0</td>                                                   
                                                </tr>
                                                
                                            </tbody>
                                        </table>

                                        <div class="comment-footer">
                                        </div>
                                    </div>
                                    <!-- END TIPOS DE VARIÁVEIS -->
                                </div>
                                <!-- DECLARAÇÃO DE VARIÁVEIS -->
                                <div class="d-flex flex-row comment-row m-t-0">
                                    <div class="comment-text w-100">
                                        <h5 class="font-medium"><u><b>Declaração de variáveis</u></b></h5>
                                        <span class="m-b-15 d-block">
                                        <b>SINTAXE:</b> Tipo de dado Lista de identificadores;<br/>
                                        Exemplo: <i>int Quant;</i><br />
                                        <i>float Num, Soma, Media;</i><br/><br>
                                        <b>CONSTANTES: </b><br>
                                            const Tipo de Dado Identificador = valor;<br>
                                            Exemplo: <i> const float Pi = 3.1416;</i>
                                        </span>
                                       

                                        <div class="comment-footer">
                                         
                                        </div>
                                    </div>
                                </div>
                                <!--END DECLARAÇÃO DE VARIÁVEIS -->
                               <!-- ESTRUTURA DE UM PROGRAMA EM C-->
                                <div class="d-flex flex-row comment-row m-t-0">
                                    <div class="comment-text w-100">
                                        <h5 class="font-medium"><u><b>Estrutura de um Programa em C</u></b></h5>
                                        <img class="img-fluid" alt="Responsive image"  src="../../assets/images/conteudos/hello_world.c.png" height="80%">
                                        <span class="m-b-15 d-block">
                                            <p>Todo programa em C deve conter as blibliotecas a serem utilizadas, declaras nas primeiras
                                                linhas de código e também um função chamada <i>main</i>, cuja tradução é <i>principal.</i>
                                            </p>
                                        </span>
                                    
                                        <div class="comment-footer">
                                         
                                        </div>
                                    </div>
                                </div>
                                <!--END ESTRUTURA DE UM PROGRAMA EM C -->
                               
                            </div>
                        </div>
                          <!-- ENTRADA E SAÍDA DE DADOS -->
                        <div class="accordion" id="accordionExample">
                            <div class="card m-b-0">
                                <div class="card-header" id="headingOne">
                                    <h4 class="card-title m-b-0" style="margin-bottom: 30px">Entrada e Saída de Dados</h4>
                                  <h5 class="mb-0">
                                    <a  data-toggle="collapse" data-target="#collapseOne" aria-expanded="true" aria-controls="collapseOne">
                                        <i class="m-r-5 fa fa-arrow-right" aria-hidden="true"></i>
                                        <span>Função <i>Printf</i></span>
                                    </a>
                                  </h5>
                                </div>
                                <div id="collapseOne" class="collapse show" aria-labelledby="headingOne" data-parent="#accordionExample">
                                  <div class="card-body">
                                    <b>Comando usado para imprimir valores na tela.</b> <br><br>
                                    <img class="img-fluid" alt="Responsive image" src="../../assets/images/conteudos/printf.jpg" width="auto" height="222px">
                                    <br><br>
                                    <b>Comando pode ser usado para imprimir apenas texto puro,
                                       como tambem para apresentar variavéis na tela, usando o %tipo
                                       referente ao tipo da variável em questão.
                                    </b>
                                  </div>
                                </div>
                            </div>
                            <div class="card m-b-0 border-top">
                                <div class="card-header" id="headingTwo">
                                  <h5 class="mb-0">
                                    <a class="collapsed" data-toggle="collapse" data-target="#collapseTwo" aria-expanded="false" aria-controls="collapseTwo">
                                        <i class="m-r-5 fa fa-arrow-right" aria-hidden="true"></i>
                                        <span>Função <i>Scanf</i></span>
                                    </a>
                                  </h5>
                                </div>
                                <div id="collapseTwo" class="collapse" aria-labelledby="headingTwo" data-parent="#accordionExample">
                                  <div class="card-body">
                                   É usada para fazer a leitura de dados formatados via teclado.<br>
                                   <b>SINTAXE:</b><br> 
                                   scanf(“expressão de controle”, lista de argumentos);<br>
                                   EXEMPLO:<br> 
                                   <i>scanf(“%f”, &salario);</i><br><br>
                                   este comando efetua uma leitura do teclado onde é esperada uma variável float (indicada por “%f”). O valor lido será armazenado no endereço da variável salário.

                                    <br><br>
                                    <img class="img-fluid" alt="Responsive image" src="../../assets/images/conteudos/scanf.jpg" width="auto" height="222px">
                                    <br><br>
                                    <b>Comando pode ser usado para imprimir apenas texto puro,
                                       como tambem para apresentar variavéis na tela, usando o %tipo
                                       referente ao tipo da variável em questão.
                                    </b>
                                  </div>
                                </div>
                            </div>
                            <div class="card m-b-0 border-top">
                                <div class="card-header" id="headingThree">
                                  <h5 class="mb-0">
                                    <a class="collapsed" data-toggle="collapse" data-target="#collapseThree" aria-expanded="false" aria-controls="collapseThree">
                                        <i class="m-r-5 fa fa-list" aria-hidden="true"></i>
                                        <span>Exemplo Completo com <i>Printf</i> e <i>Scanf</i></span>
                                    </a>
                                  </h5>
                                </div>
                                <div id="collapseThree" class="collapse" aria-labelledby="headingThree" data-parent="#accordionExample">
                                  <div class="card-body">
                                    <img class="img-fluid" alt="Responsive image" src="../../assets/images/conteudos/exemplo_printf_scanf.png" width="auto" height="222px">
                                  </div>
                                </div>
                            </div>
                        </div>
                        <!--END ENTRADA E SAÍDA DE DADOS -->

                        <!--ESTRUTURAS DE DECISÃO IF E ELSE -->
                        <div class="card" style="margin-top: 15px">
                            <div class="card-body">
                                <h4 class="card-title m-b-0">Estruturas de Decisões <i>IF</i> e <i>ELSE</i></h4>
                            </div>
                            <div class="comment-widgets scrollable">
                                <!-- VARIÁVEIS -->
                                <div class="d-flex flex-row comment-row m-t-0">
                                   
                                    <div class="comment-text w-100">
                                        <div class="card-body">
                                            <img class="img-fluid" alt="Responsive image" src="../../assets/images/conteudos/if_else.jpg" width="auto" height="222px">
                                        </div><br>
                                        <span>
                                            <p>Serve para dizer ao programa qual decisão tomar quando uma certa condição, definida
                                                pelo programador, for verdadeira ou falsa.
                                            </p>
                                        </span>
                                        <img class="img-fluid" alt="Responsive image" src="../../assets/images/conteudos/if_else2.png" width="auto" height="222px"><br><br>
                                        <span class="m-b-15 d-block">
                                        A estrutura <i>if</i> tem como objetivo testar se a condição passada é verdadeira, caso seja
                                        ela entra dentro do bloco <i>if</i>. Não sendo verdadeira a condição, caíra dentro da estrutura <i>else</i>.

                                        </span>
                                    </div>
                                </div>
                                
                                
                            </div>
                        </div>
                        <!--END ESTRUTURAS DE DECISÃO IF E ELSE -->

                        
                        
                    </div>
                    <!--FIM COLUNA ESQUERDA-->

                    <!--INICIO COLUNA DIREITA-->
                    <div class="col-md-6">
                     <!--ESTRUTURAS DE REPETIÇÕES FOR E WHILE -->
                        <div class="card">
                            <div class="card-body">
                                <h4 class="card-title m-b-0">Estruturas de Repetições</h4>
                            </div>
                            <div class="comment-widgets scrollable">
                                
                                <div class="d-flex flex-row comment-row m-t-0">

                                    <div class="comment-text w-100">
                                        <h5 class="font-medium"><u><b>Laço <i>FOR</i></u></b></h5>
                                        <div class="card-body">
                                            <span>
                                                <p>
                                                    SINTAXE: <br>
                                                </p>
                                            </span>
                                            <img class="img-fluid" alt="Responsive image" src="../../assets/images/conteudos/for.png" width="auto" height="222px">
                                        </div><br>
                                        <span class="m-b-15 d-block">
                                            A condição e o incremento do do <i>for</i>, são declaradas no topo, dentro dos parenteses.
                                        </span>
                                        <br>
                                        <p>
                                            MULTIPLAS INSTRUÇÔES: <br>
                                        </p>
                                        <ul>
                                            <li>
                                                Se um laço for deve executar várias instruções a cada iteração, elas precisam estar entre chaves;
                                            </li>
                                            <li>
                                                Sintaxe;
                                            </li>
                                        </ul>
                                        <img class="img-fluid" alt="Responsive image" src="../../assets/images/conteudos/for2.png" width="auto" height="222px"><br><br>

                                        <hr>
                                        <br><br>
                                        <h5 class="font-medium"><u><b>Laço <i>WHILE</i></u></b></h5>
                                        <div class="card-body">
                                            <span>
                                                <p>
                                                    SINTAXE: 
                                                </p>
                                                <p>
                                                    <ul>
                                                        <li>
                                                            Consiste na palavra chave <b><i>while</i></b> seguida de uma expressão de teste entre parênteses;
                                                        </li>
                                                        <li>
                                                            Se a expressão de teste for verdadeira, o corpo do laço é executado de uma vez e a expressão de teste é avaliada novamente;
                                                        </li>
                                                        <li>
                                                            Esse ciclo de teste e execução é repetido até que a expressão de teste torne falsa;
                                                        </li>
                                                        <li>
                                                            Então o laço termina e o controle do programa passa para a linha seguinte;
                                                        </li>
                                                    </ul>
                                                </p>
                                            </span>

                                            <img class="img-fluid" alt="Responsive image" src="../../assets/images/conteudos/while.png" width="auto" height="222px">
                                            <br><br>
                                            <span>
                                                <p>
                                                    <b>ATENÇÂO:</b> Diferentemente do <i>for</i>, a declaração do incremento no <i>while</i>, ocorre dentro do corpo do laço, junto às instruções.
                                                </p>
                                            </span>
                                        </div><br>
                                    </div>


                                </div>
                            </div>
                        </div>
                        <!--END ESTRUTURAS DE REPETIÇÕES FOR E WHILE --> 

                        <!--TESTE DE MESA -->
                        <div class="card">
                            <div class="card-body">
                                <h4 class="card-title m-b-0">Teste de Mesa</h4>
                                <br>
                                <span>
                                    OBJETIVOS:
                                    <ul>
                                        <li>
                                            Aprender a verificar se o programa ou algoritmo leva a um resultado esperado, através de uma simulação de valores;
                                        </li>
                                        <li>
                                            Simula a utilização do algoritmo sem a utilização de um computador, empregando apenas "<i>papel e caneta</i>";
                                        </li>
                                    </ul>

                                    <img class="img-fluid" alt="Responsive image" src="../../assets/images/conteudos/teste_de_mesa.png" width="auto" height="222px">
                                    <img class="img-fluid" alt="Responsive image" src="../../assets/images/conteudos/teste_de_mesa2.png" width="auto" height="222px">
                                    <img class="img-fluid" alt="Responsive image" src="../../assets/images/conteudos/teste_de_mesa3.png" width="auto" height="222px">
                                </span>
                            </div>
                            
                            <!-- exemplo teste de mesa -->
                            <div id="accordian-4">
                                <div class="card m-t-30">
                                    <a class="card-header link" data-toggle="collapse" data-parent="#accordian-4" href="#Toggle-1" aria-expanded="true" aria-controls="Toggle-1">
                                        <i class="fa fa-arrow-right" aria-hidden="true"></i>
                                        <span>Exemplo teste de mesa</span>
                                    </a>
                                    <div id="Toggle-1" class="collapse  multi-collapse">
                                        <div class="card-body widget-content">
                                             <img class="img-fluid" alt="Responsive image" src="../../assets/images/conteudos/teste_de_mesa4.png" width="auto" height="222px">
                                             <img class="img-fluid" alt="Responsive image" src="../../assets/images/conteudos/teste_de_mesa5.png" width="auto" height="222px">
                                        </div>
                                    </div>
                                    
                                    
                                </div>
                            </div>
                            
                            
                        </div>
                        <!--END TESTE DE MESA -->
                    </div>
                </div>
                
                <!-- ============================================================== -->
                <!-- End PAge Content -->
                <!-- ============================================================== -->
                <!-- ============================================================== -->
                <!-- Right sidebar -->
                <!-- ============================================================== -->
                <!-- .right-sidebar -->
                <!-- ============================================================== -->
                <!-- End Right sidebar -->
                <!-- ============================================================== -->
            </div>
            <!-- ============================================================== -->
            <!-- End Container fluid  -->
            <!-- ============================================================== -->
            <!-- ============================================================== -->
            <!-- footer -->
            <!-- ============================================================== -->
           
            <!-- ============================================================== -->
            <!-- End footer -->
            <!-- ============================================================== -->
        </div>  


       




<?php include('rodape.php') ?>